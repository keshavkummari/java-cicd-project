*** Settings ***
Documentation     A test suite with a single test for loading the PRS page. Script still needs to be improved with login details
...
...               GuideLines:
...               1. Test cases are written in a tabular format.
...               2.'#' is used to comment a Line
...               3.Test case consists of multiple sections, such as "Settings","Variables","Keywords","Test Cases".
...               Settings -   used as Global settings such as Loading the library.
...               Variables -  used to declare the variables
...               Keywords -   place for defining the custom keywords
...               Test Cases - test case which is getting executed by the user.
...
...               Change value of variable ${PRS URL} for different environments
...               Change values ${searchname} and ${password} if you want to change the login values
...
Resource          chrome-headless.robot
Test Timeout      2 minutes
Test Setup        Open Chrome Headless
Test Teardown     Close All Browsers

*** Variables ***
${PRS URL}               https://%{PRS_DOMAIN_NAME}/prs_nn/
${HOME URL}              ${PRS URL}index.jsp
${SIGNATURE BOOK URL}    ${PRS URL}searchproxy.jsp?LOGINTYPE=NN
${REQUEST URL}           ${PRS URL}applicationstart.jsp?LOGINTYPE=NN
${LOGIN URL}             ${PRS URL}login.jsp?LOGINTYPE=SEC
${ABOUT PROXY URL}       ${PRS URL}aboutproxies.jsp
${searchname}            d
${password}

*** Keywords ***
Open Browser To PRS Page
    Go To                        ${PRS URL}
    Title Should Be              Nationale Nederlanden intranet
    Page Should Contain          Proxy Registration System - Homepage
    Page Should Contain Image    logo
    Page Should Contain Image    xpath=//*[@id="sub_bg"]/span[4]/img

Open Signature Book Page
    Go To                        ${PRS URL}
    Click Link                   xpath=//*[@id="level1a"]/a
    Location Should Be           ${SIGNATURE BOOK URL}
    Page Should Contain          Signature book
    Page Should Contain Element  xpath=//a[@href="javascript:buttonSubmitSearch('name',document.form.searchname.value)"]

Input Name
    [Arguments]    ${searchname}
    Input Text    searchname    ${searchname}

Submit Name
    Click Element    xpath=//a[@href="javascript:buttonSubmitSearch('name',document.form.searchname.value)"]

Empty Input
    Page Should Contain          Attention: Please enter a search value

Name Input
    Page Should Contain          List of personnel names contains '${searchname}'

Open Request Page
    Go To                        ${PRS URL}
    Click Link                   xpath=//*[@id="level1b"]/a
    Location Should Be           ${REQUEST URL}
    Page Should Contain          Request start page
    Page Should Contain Element  xpath=//a[@href="javascript:buttonSubmitLogin('cds',document.form.copkey.value,document.form.coppw.value)"]
    Page Should Contain Element  xpath=//a[@href="javascript:opopUp('aboutproxies.jsp','pop','380','450','yes')"]

Open Login Page
    Go To                        ${PRS URL}
    Mouse Over                  xpath=//*[@id="level1c"]/a
    Page Should Contain         First controller
    Page Should Contain         Second controller
    Page Should Contain         AA Supervisor
    Page Should Contain         System supervisor
    Click Element               xpath=//*[@id="level2e"]/a/span
    Mouse Over                  xpath=//*[@id="level1c"]/a
    Click Element               xpath=//*[@id="level2b"]/a
    Location Should Be          ${LOGIN URL}
    Page Should Contain         Login as Second controller

Input User ID
    [Arguments]    ${searchname}
    Input Text    userid    ${searchname}

Input Password
    [Arguments]    ${password}
    Input Text    password    ${password}

Submit Unknown System Supervisor Login
    Click Element               xpath=//a[@href="javascript:buttonSubmit('PROCESS')"]/img
    Page Should Contain         Attention: User id unknown

Open About Proxies PopUp
    Go To                        ${PRS URL}
    Click Link                   xpath=//*[@id="level1d"]/a
    Select Window                NEW
    Location Should Be           ${ABOUT PROXY URL}
    Page Should Contain          ABOUT PROXIES
    Close Window

*** Test Cases ***
Valid PRS WebPage
    Open Browser To PRS Page

Valid Signature Book Page
    Open Signature Book Page
    Input Name    ${EMPTY}
    Submit Name
    Empty Input
    Input Name    ${searchname}
    Submit Name
    Name Input

#Valid Request Page
#    Open Request Page

Valid Login Page
    Open Login Page
    Input User ID   ${searchname}
    Input Password    ${password}
    Submit Unknown System Supervisor Login

Valid About Proxies PopUp
    Open About Proxies PopUp
