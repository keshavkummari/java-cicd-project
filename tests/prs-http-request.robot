*** Settings ***
Documentation     Check PRS HTTP response code
Test Timeout      2 minutes
Library           RequestsLibrary

*** Variables ***

*** Test Cases ***
Check PRS HTTP response code
    [Tags]  smoke
    Create Session  prs  https://%{PRS_DOMAIN_NAME}  verify=${CURDIR}${/}insim-root-ca.crt
    ${resp}=  Get Request  prs  /prs_nn/
    Should Be Equal As Strings  ${resp.status_code}  200

