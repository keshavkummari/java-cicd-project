*** Settings ***
Library          Selenium2Library

*** Keywords ***
Open Chrome Headless
    ${c_opts}=             Evaluate      sys.modules['selenium.webdriver'].ChromeOptions()    sys, selenium.webdriver
    Call Method            ${c_opts}     add_argument    headless
    Call Method            ${c_opts}     add_argument    disable-gpu
    Call Method            ${c_opts}     add_argument    disable-software-rasterizer
    Call Method            ${c_opts}     add_argument    disable-shared
    Call Method            ${c_opts}     add_argument    no-sandbox
    Call Method            ${c_opts}     add_argument    no-proxy-server
    Call Method            ${c_opts}     add_argument    window-size\=1024,768
    Create Webdriver       Chrome        crm_alias       chrome_options=${c_opts}
