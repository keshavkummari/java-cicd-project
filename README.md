### Hi there!

Welcome to the example project of the AWS-Team!
In here you will find an example of a project repository that bakes an AMI with a webapp and deploys it.

#### Purpose
The purpose of this example project is to give you, our colleague, a place to refer to for a simple, working example of a pipeline CI/CD solution. If you are not sure where or how to begin with CI/CD-ing your project, we encourage you to clone this project and build on top of it, or change the code as you see fit. As always, if you have any questions, refer to our [OPS-OF-THE-DAY](https://aws-team.docs.aws.insim.biz/aws-platform/) and we will help you!

#### Tools
The following tools will be used:

1. [Gitlab](https://docs.gitlab.com/): VCS (version control system) based on top of [Git](https://git-scm.com/book/en/v2/Getting-Started-Git-Basics).
2. [GitlabCI](https://docs.gitlab.com/ce/ci/): Pipelining solution that comes with Gitlab. We will use this to automate the creation of the artifacts and the deployment of them.
3. [AWS](https://aws.amazon.com/documentation/): AWS is the abbreviation of Amazon Web Services. It is the cloudprovider we use to host all our applications and infrastructure via a 'pay as you use' model.
4. [Terraform](https://www.terraform.io/docs/index.html): Open source counterpart of CloudFormation. Terraform is a powerful 'infrastructure as code' tool. We will use this to create our AWS resources.
5. [Ansible](http://docs.ansible.com/): Ansible is an IT automation engine, that we will use to push configuration to our servers from our code in the repository. Ansible is generally used for cloud provisioning, configuration management, application deployment and sometimes orchestration between multiple parts of an IT setup (think application dependencies, cloud architecture). We will use Ansible exactly for these 4 purposes as well.
6. [Packer](https://www.packer.io/docs/): Open source counterpart of AWS Codebuild. Packer is a simple to use building tool, that is able to create machine images for all kinds of platforms from a source configuration.

#### The setup

From a very high level overview, this is the setup.

1. Gitlab is the system in which we will store our code and configuration files. We will embrace the concept of the monorepo. An interesting discussion regarding the monorepo can be found [here](http://www.drmaciver.com/2016/10/why-you-should-use-a-single-repository-for-all-your-companys-projects/). The monorepo will be the example-project folder, with distinct subfolders for the configuration of the different tools. We divide the folders by tool configuration and we encourage you do the same.

2. Gitlab uses GIT as the underlying version control technique. We will use the concept of [branches](https://git-scm.com/book/en/v2/Git-Branching-Branches-in-a-Nutshell). The idea is that there is a `master` branch. Code committed in the `master` branch will result in a deployment of that code in production via the GitLabCI pipeline. When you do development work, you create a new `branch` and you will split off from the `master` branch code. When committing your code to your `branch` and you push your `branch` to GitLab from your local devstation, the pipeline will deploy that code in the development or staging environment.

2. So, a push of code to this repository, either to `master` or to `branch` will result in triggering of the pipeline. We will build logic into the pipeline so the pipeline knows where to deploy based of the place to where you pushed your code.

3. When the pipeline runs, it will leverage the tools we specified above in a specific order. This is done in the `.gitlab-ci.yml`. We will create an AMI with Ansible and Packer. The AMI will then be deployed with Terraform. Testing will be done in development/staging. A canary test will be done in production.

#### Advanced examples 


##### Blue/Green deployment mechanism for IaaS based applications

The NN AWS team has developed a Blue/Green deployment mechanism for IaaS based applications (appl baked in VM Image).
The application 'app' is a simple Apache configuration serving a webpage on port 80. 

The .gitlab-ci.yml called bluegreen contains all the jobs for this mechanism. For it to work, rename the current .gitlab-ci.yml to something else, and rename the gitlab-ci.yml called bluegreen to gitlab-ci.yml.

The pipeline is context dependendent on the NN AWS SS-DT account, at least for:
- the source AMI being used
- a artifacts bucket, possibly manually created, not encrypted. (already created, it should exist)

The terraform environment is setup as a 'terraform space' accoording to the Space pattern developed by the NN AWS team. 
The resources of this environment can be space instantiated, meaning muliple unique deployments of the same set of resources. 
We usually do this for development purpose, where multiple engineers can work on their own set of resources in their feature branch (where a feature branch strategy is being used in the development workflow)

The tf and pipeline is not able to deploy to multiple accounts (although that can be implemented with small effort)

Some specs:
- based on task decoupling in Concourse CI 
- Terraform environment instantiation based on branch ('Terraform space instancing'), terraform template must be instantiationable via the 'env' variable
- 2 load balancers (Live, Staging)
- Using the terraform space 'app' 

DEMO TEST: while true ; do curl --max-time 5 internal-tf-app-master-live-144919805.eu-west-1.elb.amazonaws.com:80 ; sleep 5 ; done




#### More information

Our documentation can be found [here](https://aws-team.docs.aws.insim.biz/aws-platform/). This can give you an example about how to approach your own project documentation.

Happy coding! -the AWS-Team!
