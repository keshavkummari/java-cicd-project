### Getting started - Git and the project

If you are setting up a new project, and if you want to use Gitlab and GitlabCI, you need to take the following steps in order to start.
1. Create your own project in Gitlab using https://vcs.aws.insim.biz/projects/new. Optionally, you can clone the contents of this repository to your own project to give yourself a headstart. This can be done in multiple ways:
 - Import the URL in the screen: https://vcs.aws.insim.biz/aws-team/example-project.git. Press `Import project from`, then `Git by url`. Add your authentication in the url. As we use MFA, this would be your username and the gitlab token. You can create the gitlab token in the console of Gitlab yourself. Save it away somewhere secure.
 - [Clone](https://git-scm.com/docs/git-clone) the project somewhere on your local machine, then use [push](https://git-scm.com/docs/git-push) to push it to your repository.
 - If you have a current SVN repository, you can migrate following the steps described [here](https://vcs.aws.insim.biz/help/workflow/importing/migrating_from_svn)
2. Now you have your project, request a [runner](https://docs.gitlab.com/runner/) for it at the AWS-Team. You can contact the OPS-OF-THE-DAY for this as described [here](https://aws-team.docs.aws.insim.biz/aws-platform/). You will need the runner to use the GitlabCI.
3. Now you have a project, possibly some content and a runner. You are now all set to go to write your own code.

### Getting started - Developing
You might want to test some of this code first in the AWS cloud. For this you can use a developer machine in AWS. In order to reach this machine using ssh you need a bastion.
1. Request a bastion from the AWS-Team. Provide the AWS-Team with your public key. If you do not have one yet, [generate](https://www.digitalocean.com/community/tutorials/how-to-set-up-ssh-keys--2) it. The AWS-Team will create a bastion for you and also will create a whitelisted security group in your account.
2. Log in in the AWS console. Go to EC2 and spin up an instance. Write down the private IP and make sure the whitelisted security group is attached.
3. Create an .ssh/config file in your home folder of your laptop/workstation. The contents might look similar to this example:

```
# the bastion
Host nn.bastion
ForwardAgent yes
Hostname bastion-aws.aws.insim.biz
User <your corp key>
ControlMaster auto
ControlPersist 5m
IdentityFile ~/.ssh/<private key>

# all your ec2 instances
Host nn.devstationfromyou
Hostname <aws private ip>
User nn-admin
IdentityFile ~/.ssh/<private key> (id_rsa for example)
Host nn.*
ServerAliveInterval 50
ForwardAgent yes
GSSAPIAuthentication no

# routing to the bastion
Host nn.* !nn.bastion
User ec2-user
ProxyCommand ssh nn.bastion -W %h:%p
IdentityFile ~/.ssh/<private key>
ControlMaster auto
ControlPersist 5m
```
