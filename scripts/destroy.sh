#!/bin/bash
export ENVIRONMENT=$1
export VARIABLES_FILE=${CI_PROJECT_DIR}/terraform/main/prs/backend_$ENVIRONMENT.tfvars
export PATH=$PATH:~
export TF_STATE_KEY=${CI_COMMIT_REF_SLUG}/prs.tfstate

echo "env = \"${ENVIRONMENT}\"" | tee -a ${CI_PROJECT_DIR}/terraform/main/prs/prs.tfvars
echo "uuid = \"${UUID}\"" | tee -a ${CI_PROJECT_DIR}/terraform/main/prs/prs.tfvars

cd ${CI_PROJECT_DIR}/terraform/main/prs/
unzip -d . ${CI_PROJECT_DIR}/terraform/provider/aws/terraform-provider-aws_1.7.1_linux_amd64.zip
unzip -d . ${CI_PROJECT_DIR}/terraform/provider/template/terraform-provider-template_1.0.0_linux_amd64.zip
terraform -version
terraform init  -backend=true -backend-config=$VARIABLES_FILE -backend-config="key=${TF_STATE_KEY}"
terraform destroy -force -var-file=${CI_PROJECT_DIR}/terraform/main/prs/prs.tfvars
