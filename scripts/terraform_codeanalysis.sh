#!/bin/bash

if [[ $(terraform fmt terraform/main) ]]; then
  echo "there are files that require formatting with: terraform fmt"
  exit 1
fi
echo "No terraform formatting issues found!"

  #add terraform validate
