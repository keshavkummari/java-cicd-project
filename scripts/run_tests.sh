#!/bin/bash

ENVIRONMENT=$1

pip install --user robotframework
pip install --user robotframework-selenium2library
pip install --user robotframework-requests
export PATH=/home/gitlab-runner/.local/bin:$PATH

if [ "${UUID}" = "master" ]; then
	export PRS_DOMAIN_NAME=prs.${ENVIRONMENT}.keshavkummari.com
else
	export PRS_DOMAIN_NAME=prs-${UUID}.${ENVIRONMENT}.keshavkummari.com
fi

robot -d testresults tests
