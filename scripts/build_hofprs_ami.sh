#!/bin/bash

set -e

#
# Get the sha1sum of a folder
#
# Useful to be able to determine whether any file in the given
# folder has been modified.
#
folder_hash () {
    # Run this in a subshell, so the cd doesn't affect other commands
    (
        cd "$1" >/dev/null || return;
        # Note: Even though sha1sum only works on files, thie find below
        # also explicitly includes folders, so the folder names (which
        # will be in sha1sum's error messages) will also be include in
        # the second sha1sum.
        find . -exec sha1sum {} + 2>&1 | sha1sum | cut -f1 -d' '
    )
}

#
# Finds an AMI ID, given a list of filters
#
# Filters are whitespace separated key=value pairs. Examples:
# - find_ami name=my_ami_name_*
# - find_ami name=my_ami_name_* tag:my-tag=my-value
#
# For all filter options, see:
# https://docs.aws.amazon.com/cli/latest/reference/ec2/describe-images.html#options
#
find_ami () {
        filters=("$@")
        filters=("${filters[@]/=/,Values=}")
        filters=("${filters[@]/#/Name=}")

        aws ec2 describe-images --owners self \
            --region eu-west-1 \
            --filters "${filters[@]}" \
            --query 'Images[*].[ImageId,CreationDate]' --output text \
            | sort -k2 -r | head -n1 | cut -f1
}

BASE_AMI_NAME='rhel_7_base_release_*'
APP_NAME='hofprs-app-*'

BASE_AMI_ID=$(find_ami "name=${BASE_AMI_NAME}")
PACKER_HASH=$(folder_hash ./packer)
ANSIBLE_HASH=$(folder_hash ./ansible)

export BASE_AMI_ID
export PACKER_HASH
export ANSIBLE_HASH

echo "Searching for existing AMI, with:"
echo "- Base AMI: '${BASE_AMI_ID}'"
echo "- Packer:   '${PACKER_HASH}'"
echo "- Ansible:  '${ANSIBLE_HASH}'"

APP_AMI_ID=$(find_ami "name=${APP_NAME}" \
                      "tag:base_ami_id=${BASE_AMI_ID}" \
                      "tag:packer_hash=${PACKER_HASH}" \
                      "tag:ansible_hash=${ANSIBLE_HASH}")

# If AMI is not available, we have to build a new one
if [ -z "$APP_AMI_ID" ]; then
    echo "AMI not found, building a new one."
    packer build -machine-readable "${CI_PROJECT_DIR}/packer/hofprs-app.json" | tee build.log
    APP_AMI_ID=$(grep 'artifact,0,id' build.log | cut -d, -f6 | cut -d: -f2)
else
    echo "Existing AMI found: ${APP_AMI_ID}"
fi

echo "pckr_ami_id = \"${APP_AMI_ID}\"" | tee "${CI_PROJECT_DIR}/terraform/main/prs/terraform.tfvars"
