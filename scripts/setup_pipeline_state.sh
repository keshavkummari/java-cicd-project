#!/bin/sh
export ENVIRONMENT=$1
echo "Checking if the Terraform S3 state bucket is available in $1. This might take up to 100 seconds"

aws s3api wait bucket-exists --bucket prs-tf-state-$ENVIRONMENT --region eu-west-1 || exitCode=$?

if [ "$exitCode" = "255" ]; then
  echo "The Terraform S3 state bucket is unavailable. We will create it"
  cd ${CI_PROJECT_DIR}/terraform/setup/state-storage-bucket

  unzip -d . ${CI_PROJECT_DIR}/terraform/provider/aws/terraform-provider-aws_1.7.1_linux_amd64.zip

  terraform init
  terraform plan -var-file=variables_$ENVIRONMENT.tfvars
  terraform apply -var-file=variables_$ENVIRONMENT.tfvars -auto-approve
fi
