# OpenJDK 7 (`openjdk`)

Source: https://github.com/antarctica/ansible-openjdk-7

Installs OpenJDK

## Overview

* Installs OpenJDK
* By default, sets the default Java installation to OpenJDK


## Variables

* `openjdk_make_default_java_installation`
    * If "true" the openJDK will be made the default JDK using a the alternavives utiliy.
* `java_packet_version`
    * Defines the version of the JDK that will be used
* `java_packet_name`
    * Defines the packet name in the packetmanager, with the java_packet_version-variable in it. e.g. "java-{{ java_packet_version }}-openjdk"

## License

Copyright 2015 NERC BAS. Licensed under the MIT license, see `LICENSE` for details.
