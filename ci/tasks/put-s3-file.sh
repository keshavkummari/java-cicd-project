#!/bin/bash

# assumes AWS cli is installed 
Usage() {
  echo "$0 <bucket> <path to file> <s3 path/to/key>"
}


(( 3 == $# )) || { Usage ; exit 1 ; }

BUCKET=$1
PATH_TO_FILE=$2
S3PATH_TO_KEY=$3


(aws s3 ls ${BUCKET} > /dev/null 2>&1 ) || { echo "Bucket ${BUCKET} does not exist, exiting!" ; exit 1 ; }
[[ -f ${PATH_TO_FILE} ]] || { echo "Path to file does not exist, exiting!" ; exit 1 ; }

echo "Uploading file"
aws s3 cp ${PATH_TO_FILE} s3://${BUCKET}/${S3PATH_TO_KEY} || { echo "error, exiting!" ; exit 1 ; }

