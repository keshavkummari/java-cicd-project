#!/bin/bash


if [[ -z ${ELB} ]] ; then echo "ELB not defined, exiting!" ; exit 1 ; fi
[[ -n $SLEEP ]] || { SLEEP=5 ; }
[[ -n $CHECKS ]] || { CHECKS=5 ; }
if [[ -z ${AWS_ACCOUNT} ]] ; then echo "AWS_ACCOUNT not defined, exiting!" ; exit 1 ; fi


export temp_credentials=$(aws sts assume-role --role-arn arn:aws:iam::${AWS_ACCOUNT}:role/tf-admin --role-session-name check-elb-instance-inservice-session)
export AWS_ACCESS_KEY_ID=$(echo ${temp_credentials} | jq -r '.Credentials.AccessKeyId') AWS_SESSION_TOKEN=$(echo ${temp_credentials} | jq -r '.Credentials.SessionToken') AWS_SECRET_ACCESS_KEY=$(echo ${temp_credentials} | jq -r ' .Credentials.SecretAccessKey') AWS_DEFAULT_REGION=eu-west-1


INSERVICE="False"
echo "Checking if any instance InService for ELB ${ELB} ..."
while [[ ${INSERVICE} == "False" && ${CHECKS} > 0 ]]  ; do
  if (aws elb describe-instance-health --load-balancer-name ${ELB} | grep 'InService')  ; then INSERVICE="True" ; fi
  CHECKS=$((CHECKS-1))
  sleep ${SLEEP}
done
if [[ ${INSERVICE} == "False" ]]; then
  echo "No instances found InService!"
  exit 255
fi
