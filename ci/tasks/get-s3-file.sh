#!/bin/bash
# assumes AWS cli is installed 
Usage() {
  echo "$0 <bucket> <s3 path/to/key> <path to store file> [--tfstate]"
}


(( 3 <= $# ))  && (( $# <= 4 )) || { Usage ; exit 1 ; }

BUCKET=$1
S3PATH_TO_KEY=$2
PATH_TO_STORE_FILE=$3

TFSTATE="false"
[[ -n $4 ]] && [[ $4 == "--tfstate" ]] && TFSTATE="true"

(aws s3 ls ${BUCKET} > /dev/null 2>&1 ) || { echo "Bucket ${BUCKET} does not exist, exiting!" ; exit 1 ; }
[[ -d $(dirname ${PATH_TO_STORE_FILE} ) ]] || { echo "Path to store file does not exist, exiting!" ; exit 1 ; }

if ! (aws s3 ls ${BUCKET}/${S3PATH_TO_KEY} > /dev/null 2>&1 ) ; then
  echo "s3 path/to/key ${S3PATH_TO_KEY} does not exist!"
  if [[ $TFSTATE == "true" ]] ; then
    [[ -z "${TERRAFORM_VERSION}" ]] && { echo "Env Variable TERRAFORM_VERSION not set, exiting!" ; exit 1 ; }
    echo "Creating initial tfstate file"   
    aws s3 cp - s3://${BUCKET}/${S3PATH_TO_KEY} <<EOF
    {
      "version": 3,
      "terraform_version": "${TERRAFORM_VERSION}",
      "lineage": "88e994f7-077a-4948-88b3-cdfcde6d0058",
      "modules": [{
        "path": ["root"],
        "outputs": {},
        "resources": {},
        "depends_on": []
      }]
    }
EOF
    [[ $? == 0 ]] || { echo "error, exiting!" ; exit 1 ; }
  else
    echo "exiting!" ; exit 1
  fi  
fi

echo "Downloading remote file"
aws s3 cp s3://${BUCKET}/${S3PATH_TO_KEY} ${PATH_TO_STORE_FILE} || { echo "error, exiting!" ; exit 1 ; }

