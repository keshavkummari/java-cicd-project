#!/bin/bash


# inputs:
# # Directory with terraform state file (can be empty state file). This file is used to extract the number of desired instances. Bound to concourse resource file name
# - name: state
[[ -d state ]] || { echo "state directory does not exist, exiting!" ; exit 1; }



  # The target account where terraform will be executed. For example, for the bastion this can be the ssdt account number (138060813667) and the ssap account number (106491825524) (mandatory)
[[ -n $AWS_ACCOUNT ]] || { echo 'AWS_ACCOUNT is not set, exiting!' ; exit 1; }
  # Boolean for selecting the blue green pattern, eiter 1 elb or 2 elb's, default is 1 elb
[[ -n $HAS_STAGING_ELB ]] || { HAS_STAGING_ELB="false" ; }
  # Name of the terraform state file
[[ -n $TERRAFORM_STATE_FILE_NAME ]] || { echo 'TERRAFORM_STATE_FILE_NAME is not set, exiting!' ; exit 1; }

[[ -n $SWITCHBACK ]] || { SWITCHBACK="false" ; }

[[ -n $WAIT_ELB_SLEEP ]] || { WAIT_ELB_SLEEP="5" ; }

[[ -n $WAIT_ELB_CHECKS ]] || { WAIT_ELB_CHECKS="48" ; }





check_elb_instance_inservice() {
  check_elb=$1

  echo ""
  SLEEP=${WAIT_ELB_SLEEP}
  CHECKS=${WAIT_ELB_CHECKS}
  INSERVICE="False"
  echo "Checking if any instance InService for ELB ${check_elb} ..."
  while [[ ${INSERVICE} == "False" && ${CHECKS} > 0 ]]  ; do
    if (aws elb describe-instance-health --load-balancer-name ${check_elb} | grep 'InService')  ; then INSERVICE="True" ; fi
    CHECKS=$((CHECKS-1))
    sleep ${SLEEP}
  done
  if [[ ${INSERVICE} == "False" ]]; then
    echo "No instances found InService!"
    exit 255
  fi
}

check_asg_no_instances() {
  check_asg=$1

  echo ""
  NOINSTANCES="False"
  while [[ ${NOINSTANCES} == "False" ]]  ; do
    echo "Waiting for no instances running in ASG ${check_asg} ..."
    if [[ $(aws autoscaling describe-auto-scaling-groups --auto-scaling-group-name ${check_asg} | jq -r '.AutoScalingGroups[0].Instances') == "[]" ]] ; then NOINSTANCES="True" ; fi
    sleep 5
  done
}


switch() {
  target_live=$1
  target_staging=$2
  live_elb=$3
  staging_elb=$4

  echo "Executing Switch"
  echo "target_live: ${target_live}"
  echo "target_staging: ${target_staging}"
  echo "live_elb: ${live_elb}"
  echo "staging_elb: ${staging_elb}"
  echo "HAS_STAGING_ELB: ${HAS_STAGING_ELB}"
  echo ""

  if [[ "$SWITCHBACK" == "true" ]] ; then
    echo "Switchback: first set desired capacity for target_live ASG ${target_live} to the max size"
    target_live_maxsize=$(aws autoscaling describe-auto-scaling-groups --auto-scaling-group-name ${target_live} | jq -r '.AutoScalingGroups[0].MaxSize')
    aws autoscaling set-desired-capacity --auto-scaling-group-name ${target_live} --desired-capacity ${target_live_maxsize}
    # Waiting for the instances to come up ELB healthy can only be done if there is a staging_elb.
    if [[ "${HAS_STAGING_ELB}" == "true" ]] ; then
      check_elb_instance_inservice ${staging_elb}
    fi
  fi

  target_live_asg=$(aws autoscaling describe-auto-scaling-groups --auto-scaling-group-name ${target_live}  | jq -r '.AutoScalingGroups[] | length') || target_live_asg=0
  target_live_elb=$(aws autoscaling describe-auto-scaling-groups --auto-scaling-group-name ${target_live} | jq -r '.AutoScalingGroups[0].LoadBalancerNames[]' | grep "${staging_elb}" ) || target_live_elb="null"
  if [[ $target_live_asg > 0 ]]
  then
    if [ $target_live_elb != "null" ] || [ "${HAS_STAGING_ELB}" = false ]
    then
      # Step 1: First we detach target_live from staging (when connected to the staging elb) and attach it also to live (implies outage on staging, but not on live)
      if [ "${HAS_STAGING_ELB}" = true ] && [ $target_live_elb == "${staging_elb}" ]
      then
        echo "detaching target_live ASG ${target_live} from staging ELB ${staging_elb}..."
        aws autoscaling detach-load-balancers --load-balancer-names ${staging_elb} --auto-scaling-group-name ${target_live}
      fi
      echo "attaching target_live ASG ${target_live} to live ELB ${live_elb}..."
      aws autoscaling attach-load-balancers --load-balancer-names ${live_elb} --auto-scaling-group-name ${target_live}
      echo "Updating tags..."
      aws autoscaling create-or-update-tags --tags "ResourceId=${target_live},ResourceType=auto-scaling-group,Key=status,Value=live,PropagateAtLaunch=true"

    fi
  fi

  target_staging_asg=$(aws autoscaling describe-auto-scaling-groups --auto-scaling-group-name ${target_staging}  | jq -r '.AutoScalingGroups[] | length') || target_staging_asg=0
  target_staging_elb=$(aws autoscaling describe-auto-scaling-groups --auto-scaling-group-name ${target_staging} | jq -r '.AutoScalingGroups[0].LoadBalancerNames[]' | grep "${live_elb}" ) || target_staging_elb="null"
  if [[ $target_staging_asg > 0 ]]
  then
    # Step 2: Next we detach target_staging from live (when connected to the live elb) and attach it to staging (implies outage on staging, but not on live)
    if [ $target_staging_elb != "null" ] && [[ $target_staging_elb == "${live_elb}" ]]
    then
      echo "detaching target_staging ASG ${target_staging} from live ELB ${live_elb}..."
      aws autoscaling detach-load-balancers --load-balancer-names ${live_elb} --auto-scaling-group-name ${target_staging}
    fi
    echo "Updating tags..."
    aws autoscaling create-or-update-tags --tags "ResourceId=${target_staging},ResourceType=auto-scaling-group,Key=status,Value=staging,PropagateAtLaunch=true"
    if [ "${HAS_STAGING_ELB}" = true ] && [ $target_staging_elb != "null" ]
    then
      echo "attaching target_staging ASG ${target_staging} from staging ELB ${staging_elb}..."
      aws autoscaling attach-load-balancers --load-balancer-names ${staging_elb} --auto-scaling-group-name ${target_staging}
    fi
    # Step 3: update the desired capacity for the target staging autoscaling group to 0
    echo "terminating instances in target_staging ASG ${target_staging} "
    aws autoscaling set-desired-capacity --auto-scaling-group-name ${target_staging} --desired-capacity 0
  fi

  if [[ "$SWITCHBACK" == "true" ]] ; then
    # Waiting for the instances to come up ELB healthy after the switch in case there is no staging ELB.
    if [[ "${HAS_STAGING_ELB}" == "false" ]] ; then
      check_asg_no_instances ${target_staging}
      check_elb_instance_inservice ${live_elb}
    fi
  fi
echo "Switch done"
}

### MAIN
echo "Assuming role"
export temp_credentials=$(aws sts assume-role --role-arn arn:aws:iam::${AWS_ACCOUNT}:role/tf-admin --role-session-name bastion-deploy-session)
export AWS_ACCESS_KEY_ID=$(echo ${temp_credentials} | jq -r '.Credentials.AccessKeyId') AWS_SESSION_TOKEN=$(echo ${temp_credentials} | jq -r '.Credentials.SessionToken') AWS_SECRET_ACCESS_KEY=$(echo ${temp_credentials} | jq -r ' .Credentials.SecretAccessKey') AWS_DEFAULT_REGION=eu-west-1

if [[ "$SWITCHBACK" == "true" ]] ; then
  echo "Switchback"
  bluegreen_target_color=$(terraform output -state=state/${TERRAFORM_STATE_FILE_NAME} bluegreen-target-color) || ( echo "tf output bluegreen-target-color not set, exiting!" ; exit 1 )
  if [[ "$bluegreen_target_color" == "blue" ]] ; then
    echo "bluegreen_target_color is blue, lets set target_color to green"
    target_color="green"
  elif [[ "$bluegreen_target_color" == "green" ]] ; then
    echo "bluegreen_target_color is green, lets set target_color to blue"
    target_color="blue"
  else
    echo "tf output bluegreen-target-color is unknown color, exiting!" ; exit 1
  fi
else
  echo "Normal GoLive Switch"
  target_color=$(terraform output -state=state/${TERRAFORM_STATE_FILE_NAME} bluegreen-target-color) || target_color="green"
  echo "target_color is ${target_color}"
fi


echo "Retreiving prefixes for switch execution"
prefixes=$(terraform output -state=state/${TERRAFORM_STATE_FILE_NAME} bluegreen-resources-prefix) || prefixes=""
for item in $prefixes
do
  if [[ -n $item ]] && [[ $item != "" ]]
  then
    prefix=$item
    if [[ "${item: -1}" == "," ]]
    then
      prefix="${item::-1}"
    fi
    echo "terraform module: ${prefix}"
    if [ $target_color == "blue" ]
    then
      target_live="${prefix}blue"
      target_staging="${prefix}green"
    else
      target_live="${prefix}green"
      target_staging="${prefix}blue"
    fi
    live_elb=${prefix}live
    staging_elb=${prefix}staging
    switch $target_live $target_staging $live_elb $staging_elb

  fi
done
