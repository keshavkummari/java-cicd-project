#!/bin/bash

# inputs:
# - name: plan
# - name: state
# - name: git

[[ -d plan ]] || { echo "plan directory does not exist, exiting!" ; exit 1; }
[[ -d state ]] || { echo "state directory does not exist, exiting!" ; exit 1; }
[[ -d git ]] || { echo "git directory does not exist, exiting!" ; exit 1; }


# outputs:
# - name: applied-state
[[ -d applied-state ]] || { echo "applied-state directory does not exist, exiting!" ; exit 1; }

# tf folder, e.g. terraform/app
[[ -n $TERRAFORM_FOLDER ]] || { echo 'TERRAFORM_FOLDER is not set, exiting!' ; exit 1; }
[[ -d $TERRAFORM_FOLDER ]] || { echo 'TERRAFORM_FOLDER does not exist, exiting!' ; exit 1; }

# identifies the functional/logical name of the terraform space, e.g. app
[[ -n $TERRAFORM_SPACE ]] || { echo 'TERRAFORM_SPACE is not set, exiting!' ; exit 1; }
# identifies a unique space instance. Usually the branch name. 
[[ -n $TF_VAR_env ]] || { echo 'TF_VAR_env is not set, exiting!' ; exit 1; }
[[ -n $env ]] || { echo 'env is not set, exiting!' ; exit 1; }

[[ -n $CHECKPOINT_DISABLE ]] || { CHECKPOINT_DISABLE=1 ; }

#  TERRAFORM_BRANCH: master
#  TF_LOG: # TRACE, DEBUG, INFO, WARN, ERROR

#### MAIN 

statename=${TERRAFORM_SPACE}_${env}

terraform apply -input=false -refresh=false \
  -state-out="applied-state/${statename}.tfstate" \
  "plan/${statename}.plan"
