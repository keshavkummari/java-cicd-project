#!/bin/bash

# s3 inputs:
# deploy-ami/
# state/

# within_pipeline outputs:
# vars/
# taint-state/

[[ -d deploy-ami ]] || { echo "deploy-ami directory does not exist, exiting!" ; exit 1; }
[[ -d state ]] || { echo "state directory does not exist, exiting!" ; exit 1; }
[[ -d vars ]] || { echo "vars directory does not exist, exiting!" ; exit 1; }
[[ -d taint-state ]] || { echo "taint-state directory does not exist, exiting!" ; exit 1; }

# The target account where terraform will be executed. For example, for the bastion this can be the ssdt account number (138060813667)
# and the ssap account number (106491825524) (mandatory)
[[ -n $AWS_ACCOUNT ]] || { echo "AWS_ACCOUNT is not set, exiting!" ; exit 1 ; }
# Name of the blue asg. For example for bastion: tf-bastion-blue-aws-{{ branch }}). Note that the example
# uses the aws team as master/template for determining blue or green (mandatory)
[[ -n $BLUE_ASG_TF_NAME ]] || { echo "BLUE_ASG_TF_NAME is not set, exiting!" ; exit 1; }
# Name of the green asg. For example for bastion: tf-bastion-green-aws-{{ branch }}) Note that the example
# uses the aws team as master/template for determining blue or green (mandatory)
[[ -n $GREEN_ASG_TF_NAME ]] || { echo "GREEN_ASG_TF_NAME is not set, exiting!" ; exit 1; }
# The (spacified and jinja-fied) name of the terraform statefile. This task queries the state file for the taint module
[[ -n $TERRAFORM_STATE_FILE_NAME ]] || { echo "TERRAFORM_STATE_FILE_NAME is not set, exiting!" ; exit 1; }




### MAIN 

export temp_credentials=$(aws sts assume-role --role-arn arn:aws:iam::${AWS_ACCOUNT}:role/tf-admin --role-session-name tf-deploy-session)
export AWS_ACCESS_KEY_ID=$(echo ${temp_credentials} | jq -r '.Credentials.AccessKeyId') AWS_SESSION_TOKEN=$(echo ${temp_credentials} | jq -r '.Credentials.SessionToken') AWS_SECRET_ACCESS_KEY=$(echo ${temp_credentials} | jq -r ' .Credentials.SecretAccessKey') AWS_DEFAULT_REGION=eu-west-1

blue_state=$(aws autoscaling describe-tags --filters Name=auto-scaling-group,Values=${BLUE_ASG_TF_NAME}) || blue_state="{\"Tags\": []}"
blue_status=$(echo $blue_state | jq -r '.Tags[] | select(.Key=="status") | .Value')
if [ -z $blue_status ]
then
  blue_status="staging"
fi

green_state=$(aws autoscaling describe-tags --filters Name=auto-scaling-group,Values=${GREEN_ASG_TF_NAME}) || green_state="{\"Tags\": []}"
green_status=$(echo $green_state | jq -r '.Tags[] | select(.Key=="status") | .Value')
if [ -z $green_status ]
then
  green_status="staging"
fi

if [ $blue_status == "live" ]
then
  # Valid case: staging green
  echo "blue is currently the active live instance. We set staging (green)."
  green_status="staging"
  target_color="green"
elif [ $green_status == "live" ]
then
  # Valid case: staging blue
  echo "green is currently the active live instance. We set staging (blue)."
  blue_status="staging"
  target_color="blue"
else
  # Other (illegal) staging cases are handled as both staging green and reverting desired variables back to default settings.
  echo "Both blue and green are staging, (re)setting blue as live asg and staging green."
  target_color="green"
fi

# Either in root (taint-modules output not specified or in modules Lets manipulate the state!
taint_modules=$(terraform output -state=state/${TERRAFORM_STATE_FILE_NAME} taint-modules) || taint_modules="root"

  for item in $taint_modules
  do
    if [[ -n $item ]] && [[ $item != "" ]] && [[ $item != "root" ]]
    then
      module=$item
      if [[ "${item: -1}" == "," ]]
      then
        module="${item::-1}"
      fi
      echo "taint terraform module: ${module}"
      terraform taint -module=${module} -state=state/${TERRAFORM_STATE_FILE_NAME} aws_launch_configuration.${target_color} || echo "Taint Auto Scaling Group Launch Configuration ${target_color} failed for module: ${module}"
    elif [[ $item == "root" ]]
    then
      terraform taint -state=state/${TERRAFORM_STATE_FILE_NAME} aws_launch_configuration.${target_color} || echo "Taint Auto Scaling Group Launch Configuration ${target_color} failed in root space module."
    fi
  done

# Copy updated state to the output directory
cp state/${TERRAFORM_STATE_FILE_NAME} taint-state/${TERRAFORM_STATE_FILE_NAME}

[[ -f vars/bluegreen.tfvars ]] && rm -f vars/bluegreen.tfvars
touch vars/bluegreen.tfvars
echo "ami=\"$(cat deploy-ami/ami)\"" >> vars/bluegreen.tfvars
echo "status_blue=\"${blue_status}\"" >> vars/bluegreen.tfvars
echo "status_green=\"${green_status}\"" >> vars/bluegreen.tfvars
echo "target_color=\"${target_color}\"" >> vars/bluegreen.tfvars


echo "Task output: The following terraform blue green variables are specified: "
cat vars/bluegreen.tfvars
