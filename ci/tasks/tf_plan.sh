#!/bin/bash

# within_pipeline inputs:
# vars/
# existing-state/

# s3 outputs:
# plan/
# state/


[[ -d vars ]] || { echo "vars directory does not exist, exiting!" ; exit 1; }
[[ -d existing-state ]] || { echo "existing-state directory does not exist, exiting!" ; exit 1; }

[[ -d plan ]] || { echo "plan directory does not exist, exiting!" ; exit 1; }
[[ -d state ]] || { echo "state directory does not exist, exiting!" ; exit 1; }


# tf folder, e.g. terraform/app
[[ -n $TERRAFORM_FOLDER ]] || { echo 'TERRAFORM_FOLDER is not set, exiting!' ; exit 1; }
[[ -d $TERRAFORM_FOLDER ]] || { echo 'TERRAFORM_FOLDER does not exist, exiting!' ; exit 1; }

# identifies the functional/logical name of the terraform space, e.g. app
[[ -n $TERRAFORM_SPACE ]] || { echo 'TERRAFORM_SPACE is not set, exiting!' ; exit 1; }
# identifies a unique space instance. Usually the branch name. 
[[ -n $TF_VAR_env ]] || { echo 'TF_VAR_env is not set, exiting!' ; exit 1; }
[[ -n $env ]] || { echo 'env is not set, exiting!' ; exit 1; }

# TF_LOG= # TRACE, DEBUG, INFO, WARN, ERROR

# plan a destroy, lists all resources affected by complete state destroy
[[ -n $TERRAFORM_PLAN_DESTROY ]] || { TERRAFORM_PLAN_DESTROY="false" ; }

[[ -n $CHECKPOINT_DISABLE ]] || { CHECKPOINT_DISABLE=1 ; }

#### MAIN 

VAR_FILES=
for file in vars/*.tfvars; do
  if [[ ! -f "$file" ]]
  then
    continue
  fi
  VAR_FILES="$VAR_FILES -var-file=$file"
done

statename=${TERRAFORM_SPACE}_${env}

terraform get "$TERRAFORM_FOLDER"
terraform plan $VAR_FILES -destroy=${TERRAFORM_PLAN_DESTROY} -refresh=true -input=false \
  -out=plan/${statename}.plan \
  -state=existing-state/${statename}.tfstate "$TERRAFORM_FOLDER"

cp existing-state/*.tfstate state/

