#!/bin/bash
#
# script name: publish_ami_ci

# encryption keys:
# SS DT: arn:aws:kms:eu-west-1:138060813667:key/8fbb94a5-7c29-4cef-9713-ada81733198c
# SS AP: arn:aws:kms:eu-west-1:106491825524:key/b5047c80-b72a-423b-af43-7a21710154df
# NN Bank DT: arn:aws:kms:eu-west-1:942214160060:key/627de455-6a98-4523-8eff-43f8446f75cd
# NN Bank AP: arn:aws:kms:eu-west-1:946637319905:key/d7e1badb-2313-42ad-9204-d77051c1d168


# Usage function
# --propagate-linux is used for propagation of rhel7
Usage () {
          echo "Usage:"
          echo " $0 --propagate-linux <source ami_id> <target_account> <target_account_role> <role_session_name> <encrypted_ami_name> <ami_description> [head_replacement_ami_override]"
          echo " $0 --propagate-centos7 <source ami_id> <target_account> <target_account_role> <role_session_name> <encrypted_ami_name> <ami_description> [head_replacement_ami_override]"
          echo " $0 --propagate-windows <source ami_id> <target_account> <target_account_role> <role_session_name> <encrypted_ami_name> <ami_description> [head_replacement_ami_override]"
}

# Fail the job when one of the commands return exit code <> 0
set -e
# hardcoded vars
export AWS_DEFAULT_REGION="ap-south-1"
# export HTTP_PROXY="http://proxy.dev.aws.insim.biz:8080"
# export HTTPS_PROXY="http://proxy.dev.aws.insim.biz:8080"
# export NO_PROXY="169.254.169.254,s3.amazonaws.com"

# check vars
if [ -z ${HTTP_PROXY} ] ; then echo "VAR HTTP_PROXY is not defined. exiting" ; exit 1 ; fi
if [ -z ${HTTPS_PROXY} ] ; then echo "VAR HTTPS_PROXY is not defined. exiting" ; exit 1 ; fi
if [ -z ${NO_PROXY} ] ; then echo "VAR NO_PROXY is not defined. exiting" ; exit 1 ; fi

case "$1" in
  --propagate-linux)
    if [ -z ${target_encryption_key} ] ; then echo "VAR target_encryption_key is not defined. exiting" ; exit 1 ; fi
    ;;
  --propagate-windows)
    if [ -z ${target_encryption_key} ] ; then echo "VAR target_encryption_key is not defined. exiting" ; exit 1 ; fi
    ;;
  --propagate-centos7)
    if [ -z ${target_encryption_key} ] ; then echo "VAR target_encryption_key is not defined. exiting" ; exit 1 ; fi
    ;;
  *)
    Usage
    exit 1
    ;;
esac


# set parameters vars
case "$1" in
  --propagate-linux)
    source_ami_id=$2
    target_account=$3
    target_account_role=$4
    role_session_name=$5
    encrypted_ami_name=$6
    ami_description=$7
    head_replacement_ami_override=$8
    ;;
  --propagate-windows)
    source_ami_id=$2
    target_account=$3
    target_account_role=$4
    role_session_name=$5
    encrypted_ami_name=$6
    ami_description=$7
    head_replacement_ami_override=$8
    ;;
  --propagate-centos7)
    source_ami_id=$2
    target_account=$3
    target_account_role=$4
    role_session_name=$5
    encrypted_ami_name=$6
    ami_description=$7
    head_replacement_ami_override=$8
    ;;
  *)
    Usage
    exit 1
    ;;
esac

# main functions

Get_snapshot_id_from_ami () {
  local _ami_id=$1
  local _snapshot_id="$(aws ec2 describe-images --image-ids ${_ami_id} | jq -r '.Images[].BlockDeviceMappings[0].Ebs.SnapshotId')"
  echo ${_snapshot_id}
}

S_Copy_encrypt_shapshot_in_source_account () {
  echo "Copying and encrypting snapshot."
  echo "Please wait..."
  encrypted_snapshot_id="$(aws ec2 copy-snapshot --source-region ${AWS_DEFAULT_REGION} --source-snapshot-id ${snapshot_id} --description ${snapshot_id} --destination-region ${AWS_DEFAULT_REGION} --encrypted --kms-key-id ${source_encryption_key} | jq -r '.SnapshotId')"
  if ! [ "$?" = 0 ] ; then echo "Error occured with copy-snapshot, exiting!" ; exit 1 ; fi
  # ec2 wait snapshot-completed will poll every 15 seconds until a successful state has been reached.
  # This will exit with a return code of 255 after 40 failed checks
  # Polling intrval cannot be changed so we have to retry until rc <> 255
  count=0
  aws ec2 wait snapshot-completed --snapshot-ids ${encrypted_snapshot_id}
  rc=$?
  while  [[ ${rc} -eq 255 && ${count} -lt 5 ]];  do
    echo "still waiting on snapshot completion"
    aws ec2 wait snapshot-completed --snapshot-ids ${encrypted_snapshot_id}
    rc=$?
    count=$((count + 1))
  done
  if ! [ "${rc}" = 0 ] ; then echo "Error occured, exiting! $rc" ; exit 1 ; fi
  echo "Copying is done."
  echo "encrypted_snapshot_id: $encrypted_snapshot_id"
}

Get_source_ami_name () {
  source_ami_name="$(aws ec2 describe-images --image-ids ${source_ami_id} | jq -r '.Images[].Name')"
  echo "Name of the source AMI ${source_ami_id} is ${source_ami_name} "
}

Define_encrypted_ami_name () {
  encrypted_ami_name="${source_ami_name}"
  echo "Name of the encrypted ami will be ${encrypted_ami_name}"
}

S_Register_ami_with_encr_snapshot () {
  declare -r devicename="/dev/sda1,Ebs={SnapshotId="$encrypted_snapshot_id",VolumeType=gp2}"
  echo "Registering new AMI using encrypted snapshot ($encrypted_snapshot_id)"
  encrypted_ami_id="$(aws ec2 register-image \
   --name "${encrypted_ami_name}" --description "${encrypted_ami_name}" \
   --root-device-name '/dev/sda1'  \
   --block-device-mappings DeviceName=${devicename} \
   --virtualization-type 'hvm' --architecture 'x86_64' | jq -r '.ImageId')"
  echo "Registered encrypted ami_id : ${encrypted_ami_id}"
  sleep 1
}

S_Tag_ami () {
  echo "Due diligence on tagging.."
  aws ec2 create-tags --resources "${encrypted_ami_id}" "${encrypted_snapshot_id}" \
    --tags  "Key=source,Value=${source_ami_id}" \
            "Key=owner,Value=${owner}"
}

S_Dereg_source_ami () {
  echo "Deregistering source AMI ${source_ami_id} ${source_ami_name}"
  aws ec2 deregister-image --image-id ${source_ami_id}
}

S_Delete_source_ami_snapshot () {
  echo "Deleting source AMI snapshot ${snapshot_id}"
  aws ec2 delete-snapshot --snapshot-id ${snapshot_id}
}

T_Add_share_permissions_to_encr_snapshot () {
  echo "Sharing encrypted snapshot with the target account."
  aws ec2 modify-snapshot-attribute --snapshot-id "$encrypted_snapshot_id" --create-volume-permission "Add=[{UserId=\"$target_account\"}]"
}

T_AssumeRole_copy_encr_snapshot_target_account () {
  echo "Changing (assume role) to target account."
  temp_credentials="$(aws sts assume-role --role-arn ${target_account_role} --role-session-name ${role_session_name})"
  export AWS_ACCESS_KEY_ID="$(echo ${temp_credentials} | jq -r '.Credentials.AccessKeyId')"
  export AWS_SECRET_ACCESS_KEY="$(echo ${temp_credentials} | jq -r ' .Credentials.SecretAccessKey')"
  export AWS_SECURITY_TOKEN="$(echo ${temp_credentials} | jq -r '.Credentials.SessionToken')"
}

###########
# The goal is to reencrypt the volume with the correct kms key and update the vars. This sets encrypted_snapshot_id again.
###########

T_Reencrypt_snapshot_with_propagate_kms () {
  echo "Reencrypting the snapshot with the kms key for ami propagate so the other account iam role can access it"
  echo "We do not need to know the source KMS key, AWS handles this"
  echo "This assumes the role that runs this script has access to this KMS key."
  echo "Old encrypted snapshot id is $encrypted_snapshot_id"
  encrypted_snapshot_id="$(aws ec2 copy-snapshot --source-region ${AWS_DEFAULT_REGION} --source-snapshot-id ${encrypted_snapshot_id} --description ${encrypted_snapshot_id} --destination-region ${AWS_DEFAULT_REGION} --encrypted --kms-key-id ${source_encryption_key}| jq -r '.SnapshotId')"
  aws ec2 wait snapshot-completed --snapshot-ids "$encrypted_snapshot_id"
  echo "New encrypted snapshot id with the ami propagate kms key is $encrypted_snapshot_id"
}

T_Copy_encrypt_snaphot_in_target_account () {
  echo "Copying encrypted snapshot from source account."
  echo "Please wait..."
  target_snapshot_id="$(aws ec2 copy-snapshot --source-region ${AWS_DEFAULT_REGION} --source-snapshot-id ${encrypted_snapshot_id} --description ${encrypted_snapshot_id} --destination-region ${AWS_DEFAULT_REGION} --encrypted --kms-key-id ${target_encryption_key}| jq -r '.SnapshotId')"
  aws ec2 wait snapshot-completed --snapshot-ids "$target_snapshot_id"
  echo "Copying is done."
  echo "Encrypted snapshot id in target account is $target_snapshot_id"
}

T_Create_volume_from_encrypted_snaphot () {
  echo "Creating new volume using target snapshot ($target_snapshot_id) in target account"
  target_volume_id="$(aws ec2 create-volume \
    --snapshot-id ${target_snapshot_id} \
    --availability-zone 'eu-west-1a' \
    --volume-type 'gp2' | jq -r '.VolumeId')"
  aws ec2 wait volume-available --volume-ids "$target_volume_id"
  echo "Created volume vol_id in target account: ${target_volume_id}"
}

# TODO: think of elegant solution for using latest ami (either rhel or windows) in both cloud custodian (disabled encryption controls) and here
# TODO: refactor for --encrypt-only and --encrypt-and-propagate
T_Get_subnet_for_instance () {
  echo "Collecting the subnet_id for the target account"
  target_subnet_id_old=$(aws ec2 describe-subnets --region eu-west-1 --filters Name=tag:Name,Values=tf-platform-trusted-az-a | jq -r '.Subnets[].SubnetId')
# TODO: once all accounts have had their networks spacified, this condition can be removed again and we should only retrieve subnets with the name *-eu-west-1a
  if [ -z ${target_subnet_id_old} ]; then
    target_subnet_id=$(aws ec2 describe-subnets --region eu-west-1 --filters Name=tag:Name,Values=tf-platform-trusted-eu-west-1a | jq -r '.Subnets[].SubnetId')
  else
    target_subnet_id=${target_subnet_id_old}
  fi
  echo "Found subnet in target account (${target_subnet_id})"
}

T_Get_latest_rhel_ami () {
  echo "Collecting latest RHEL AMI ID from the marketplace"
  if [ -z $head_replacement_ami_override ]; then
    marketplace_image_id=$(aws ec2 describe-images --owners 875428837402  | jq -r '.Images | map(select(.Public == true and .Name != null and (.Name | startswith("RHEL-7") and (contains("_Beta") | not) ))) | sort_by(.Name) | reverse | .[0].ImageId')
  else
    marketplace_image_id=${head_replacement_ami_override}
  fi
  echo "Found RHEL AMI ID in the marketplace: (${marketplace_image_id})"
}

T_Get_latest_centos7_ami () {
  echo "Collecting latest CENTOS7 AMI ID from the marketplace"
  if [ -z $head_replacement_ami_override ]; then
    marketplace_image_id=$(aws ec2 describe-images --region eu-west-1 --filters Name=owner-alias,Values=aws-marketplace,Name=product-code,Values=aw0evgkw8e5c1q413zgy5pjce --query 'sort_by(Images, &CreationDate)[-1].[ImageId]' --output text)
  else
    marketplace_image_id=${head_replacement_ami_override}
  fi
  echo "Found CENTOS7 AMI ID in the marketplace: (${marketplace_image_id})"
}

T_Get_latest_windows_ami () {
  echo "Collecting latest Windows Server 2016 AMI ID from the marketplace"
  if [ -z $head_replacement_ami_override ]; then
    marketplace_image_id=$(aws ec2 describe-images --owners amazon | jq -r '.Images | map(select(.Public == true and .Name != null and (.Name | startswith("Windows_Server-2016-English-Full-Base-") and (contains("_Beta") | not) ))) | sort_by(.Name) | reverse | .[0].ImageId')
  else
    marketplace_image_id=${head_replacement_ami_override}
  fi
  echo "Found RHEL AMI ID in the marketplace: (${marketplace_image_id})"
}

T_Create_instance_from_provided_ami () {
  echo "Starting instance ..."
  instance_id=$(aws ec2 run-instances \
   --image-id ${marketplace_image_id} \
   --region ${AWS_DEFAULT_REGION} \
   --subnet-id ${target_subnet_id} \
   --block-device-mappings "[{\"DeviceName\":\"/dev/sda1\",\"Ebs\":{\"DeleteOnTermination\":true}}]" \
   --instance-type t2.medium \
   --placement 'AvailabilityZone=eu-west-1a' \
   --tag-specifications "ResourceType=instance,Tags=[{Key=IACCEPTTHATTHISTAGWILLDESTROYMYINSTANCESANDIACCEPTRESPONSIBILITYFORSETTINGIT,Value=True},{Key=propagation,Value=true},{Key=Name,Value=temporary-propagation-instance}]" | jq -r '.Instances[0].InstanceId')
   aws ec2 wait instance-running --instance-ids "$instance_id"
   echo "Stopping instance $instance_id..."
   aws ec2 stop-instances --force --instance-ids "$instance_id"
   aws ec2 wait instance-stopped --instance-ids "$instance_id"
}

T_Swap_root_volume_on_instance_with_encrypted () {
  defunct_volume_id=$(aws ec2 describe-volumes --filters \
    Name=attachment.instance-id,Values=${instance_id} \
    | jq -r '.Volumes[0].VolumeId')
  echo "Detaching volume: $defunct_volume_id..."
  aws ec2 detach-volume --volume-id "$defunct_volume_id"
  echo "Attaching volume: $target_volume_id..."
  aws ec2 attach-volume --volume-id "$target_volume_id" --instance-id "$instance_id" --device /dev/sda1
}

T_Create_ami_from_instance_in_target_account () {
  echo "Creating image with encrypted volume..."
  target_ami_id="$(aws ec2 create-image \
     --instance-id ${instance_id} \
     --name ${encrypted_ami_name} \
     --description ${ami_description} \
     --block-device-mappings "[{\"DeviceName\":\"/dev/sda1\",\"Ebs\":{\"DeleteOnTermination\":true}}]" \
     | jq -r '.ImageId' )"
  echo "Waiting for image ${target_ami_id} to become available..."
  aws ec2 wait image-available --image-ids $target_ami_id
  echo "Created image $target_ami_id"
}

T_Tag_ami () {
  echo "Due diligence on tagging.."
  aws ec2 create-tags --resources "${target_ami_id}" "${target_snapshot_id}" \
    --tags  "Key=source,Value=${source_ami_id}" \
            "Key=owner,Value=awsteam"
}

T_Clean_up () {
  echo "Deleting defunct volume: ${defunct_volume_id}..."
  aws ec2 wait volume-available --volume-ids "$defunct_volume_id"
  aws ec2 delete-volume --volume-id "$defunct_volume_id"
  echo "Terminating defunct instance ${instance_id}..."
  aws ec2 terminate-instances --instance-ids "$instance_id"
  echo "Deleting defunct snapshot ${target_snapshot_id}..."
  aws ec2 delete-snapshot --snapshot-id "$target_snapshot_id"
}

####### MAIN

case "$1" in
  --propagate-linux)
    encrypted_snapshot_id=$(Get_snapshot_id_from_ami ${source_ami_id}) ; echo "Snapshot id of the AMI ${source_ami_id}  is ${encrypted_snapshot_id}"
    Get_source_ami_name
    T_Reencrypt_snapshot_with_propagate_kms
    T_Add_share_permissions_to_encr_snapshot
    T_AssumeRole_copy_encr_snapshot_target_account
    T_Copy_encrypt_snaphot_in_target_account
    T_Create_volume_from_encrypted_snaphot
    T_Get_subnet_for_instance
    T_Get_latest_rhel_ami
    T_Create_instance_from_provided_ami
    T_Swap_root_volume_on_instance_with_encrypted
    T_Create_ami_from_instance_in_target_account
    T_Tag_ami
    T_Clean_up
    ;;
  --propagate-centos7)
    encrypted_snapshot_id=$(Get_snapshot_id_from_ami ${source_ami_id}) ; echo "Snapshot id of the AMI ${source_ami_id}  is ${encrypted_snapshot_id}"
    Get_source_ami_name
    T_Reencrypt_snapshot_with_propagate_kms
    T_Add_share_permissions_to_encr_snapshot
    T_AssumeRole_copy_encr_snapshot_target_account
    T_Copy_encrypt_snaphot_in_target_account
    T_Create_volume_from_encrypted_snaphot
    T_Get_subnet_for_instance
    T_Get_latest_centos7_ami
    T_Create_instance_from_provided_ami
    T_Swap_root_volume_on_instance_with_encrypted
    T_Create_ami_from_instance_in_target_account
    T_Tag_ami
    T_Clean_up
    ;;
  --propagate-windows)
    encrypted_snapshot_id=$(Get_snapshot_id_from_ami ${source_ami_id}) ; echo "Snapshot id of the AMI ${source_ami_id}  is ${encrypted_snapshot_id}"
    Get_source_ami_name
    T_Reencrypt_snapshot_with_propagate_kms
    T_Add_share_permissions_to_encr_snapshot
    T_AssumeRole_copy_encr_snapshot_target_account
    T_Copy_encrypt_snaphot_in_target_account
    T_Create_volume_from_encrypted_snaphot
    T_Get_subnet_for_instance
    T_Get_latest_windows_ami
    T_Create_instance_from_provided_ami
    T_Swap_root_volume_on_instance_with_encrypted
    T_Create_ami_from_instance_in_target_account
    T_Tag_ami
    T_Clean_up
    ;;
  *)
    Usage
    exit 1
    ;;
esac
echo "${target_ami_id}" > ami
echo "All Done"
