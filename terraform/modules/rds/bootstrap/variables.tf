variable "name" {
  description = "Name to be used as a basename on all the resources identifiers"
  default     = "RDS_BOOSTRAP_EPHEMERAL"
}

variable "subnet_id" {
  description = "Subnet in which the ephemeral instance is created"
  type        = "string"
}

variable "security_group_ids" {
  description = "A list of security groups the ephemeral instance will belong to"
  type        = "list"
}

variable "iam_instance_profile" {
  description = "Instance profile name that will be used by the bootstrap instance"
  type        = "string"
}

variable "rds_endpoint" {
  description = "RDS connection endpoint"
}

variable "rds_port" {
  description = "RDS connection port"
  type        = "string"
  default     = "3306"
}

variable "rds_username" {
  description = "RDS master user name"
  type        = "string"
}

variable "rds_password" {
  description = "RDS master password"
  type        = "string"
}

variable "rds_database_name" {
  description = "RDS database name"
  type        = "string"
}

variable "shell_script" {
  description = "A shell script template that will be run from the ephemeral instanceRDS database name"
  type        = "string"
  default     = "bootstrap.sh.tpl"
}

variable "sql_script" {
  description = "A SQL script that will be run from the ephemeral instance against a MySQL/Aurora RDS DB"
  type        = "string"
  default     = "bootstrap.sql.tpl"
}

variable "instance_type" {
  description = "Ephemeral instance type"
  type        = "string"
  default     = "t2.micro"
}

variable "depends_on" {
  description = "Pseudo variable to introduce dependency on a calculated value."
  type        = "list"
  default     = []
}
