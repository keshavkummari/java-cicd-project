/*
Minimal data to start PRS

System supervisor account:

SYSTEM
welkom01

*/
USE `${DATABASE_NAME}`;

INSERT INTO GENERAL_USER (UGE_CREATEDON, UGE_CREATEDBYID, UGE_CHANGEDON, UGE_CHANGEDBYID, UGE_ID, UGE_USERID, UGE_FIRSTNAMES,UGE_LASTNAME, UGE_PASSWORD, UGE_PASSWDCHANGEDATE, UGE_PASSWDMUSTCHANGE, UGE_TYPE, UGE_LOGINFAILCOUNT, UGE_ACTIVE) VALUES (sysdate(),1,sysdate(),1,1,'SYSTEM','Interne','technische gebruiker','--c=i`[E-&',sysdate(),1,'SYS',1,1);
INSERT INTO GENERAL_USER (UGE_CREATEDON, UGE_CREATEDBYID, UGE_CHANGEDON, UGE_CHANGEDBYID, UGE_ID, UGE_USERID, UGE_FIRSTNAMES,UGE_LASTNAME, UGE_PASSWORD, UGE_PASSWDCHANGEDATE, UGE_PASSWDMUSTCHANGE, UGE_TYPE, UGE_LOGINFAILCOUNT, UGE_ACTIVE) VALUES (sysdate(),1,sysdate(),1,2,'DEF','Anonymous','DEF User','-*7A^gM#fV^9',sysdate(),1,'NN',1,1);
Insert Into LEGAL_ENTITY ( LGE_CREATEDON, LGE_CREATEDBYID, LGE_CHANGEDON, LGE_CHANGEDBYID, LGE_ID, LGE_NAME, LGE_ADMINCENTERID, LGE_ACTIVE ) Values ( Sysdate(), 1, Sysdate(), 1, 'DEF', 'Default entity', Null, 1 );
INSERT INTO SYSTEM_DATA (SYD_LEGALWHOLESALE,SYD_ID,SYD_EMAIL,SYD_PASSWORDDAYS,SYD_CREATEDON,SYD_CREATEDBYID,SYD_CHANGEDON,SYD_CHANGEDBYID)  VALUES ('DEF',1,'system@devoteam.nl',90,sysdate(),1,sysdate(),1);
INSERT INTO STATUS ( STS_CREATEDON,STS_CREATEDBYID,STS_CHANGEDON,STS_CHANGEDBYID,STS_STATUS, STS_NAME ) VALUES (SYSDATE(),1,SYSDATE(),1,0,'Initial entry');
INSERT INTO STATUS ( STS_CREATEDON,STS_CREATEDBYID,STS_CHANGEDON,STS_CHANGEDBYID,STS_STATUS, STS_NAME ) VALUES (SYSDATE(),1,SYSDATE(),1,1,'Checked and printed');
INSERT INTO STATUS ( STS_CREATEDON,STS_CREATEDBYID,STS_CHANGEDON,STS_CHANGEDBYID,STS_STATUS, STS_NAME ) VALUES (SYSDATE(),1,SYSDATE(),1,2,'Looked at by FC');
INSERT INTO STATUS ( STS_CREATEDON,STS_CREATEDBYID,STS_CHANGEDON,STS_CHANGEDBYID,STS_STATUS, STS_NAME ) VALUES (SYSDATE(),1,SYSDATE(),1,3,'Approved by FC');
INSERT INTO STATUS ( STS_CREATEDON,STS_CREATEDBYID,STS_CHANGEDON,STS_CHANGEDBYID,STS_STATUS, STS_NAME ) VALUES (SYSDATE(),1,SYSDATE(),1,4,'Rejected by FC');
INSERT INTO STATUS ( STS_CREATEDON,STS_CREATEDBYID,STS_CHANGEDON,STS_CHANGEDBYID,STS_STATUS, STS_NAME ) VALUES (SYSDATE(),1,SYSDATE(),1,5,'Signature added');
INSERT INTO STATUS ( STS_CREATEDON, STS_CREATEDBYID, STS_CHANGEDON, STS_CHANGEDBYID, STS_STATUS,STS_NAME ) VALUES (SYSDATE(), 1,  SYSDATE(), 1, 6, 'Completely ok by FC');
INSERT INTO STATUS ( STS_CREATEDON,STS_CREATEDBYID,STS_CHANGEDON,STS_CHANGEDBYID,STS_STATUS, STS_NAME ) VALUES (SYSDATE(),1,SYSDATE(),1,7,'Published by SC');
INSERT INTO STATUS ( STS_CREATEDON,STS_CREATEDBYID,STS_CHANGEDON,STS_CHANGEDBYID,STS_STATUS, STS_NAME ) VALUES (SYSDATE(),1,SYSDATE(),1,8,'Fully rejected by SC');
INSERT INTO STATUS ( STS_CREATEDON,STS_CREATEDBYID,STS_CHANGEDON,STS_CHANGEDBYID,STS_STATUS, STS_NAME ) VALUES (SYSDATE(),1,SYSDATE(),1,9,'FC Ok, SC not Ok');
INSERT INTO STATUS ( STS_CREATEDON,STS_CREATEDBYID,STS_CHANGEDON,STS_CHANGEDBYID,STS_STATUS, STS_NAME ) VALUES (SYSDATE(),1,SYSDATE(),1,10,'FC not Ok, SC Ok');
INSERT INTO STATUS ( STS_CREATEDON, STS_CREATEDBYID, STS_CHANGEDON, STS_CHANGEDBYID, STS_STATUS,STS_NAME ) VALUES (SYSDATE(), 1,  SYSDATE(), 1, 11, 'Replaced by a new application');
INSERT INTO STATUS ( STS_CREATEDON, STS_CREATEDBYID, STS_CHANGEDON, STS_CHANGEDBYID, STS_STATUS,STS_NAME ) VALUES ( SYSDATE(), 1,  SYSDATE(), 1, 12, 'Fully rejected by FC');

insert into mgt_struct_version values (SYSDATE(),1,SYSDATE(),1,1,'Default Management Structure',SYSDATE(),SYSDATE(),SYSDATE(),'C');

insert into businessunit_version values  (SYSDATE(),1,SYSDATE(),1,1,1,'Default Executive Centre',null,1 );
insert into businessunit_version values  (SYSDATE(),1,SYSDATE(),1,1,2,'Default Management Centre Division',1,2 );
insert into businessunit_version values  (SYSDATE(),1,SYSDATE(),1,1,3,'Default Organisation Part I',2,3 );
insert into businessunit_version values  (SYSDATE(),1,SYSDATE(),1,1,4,'Default Organisation Part II',3,4 );



Insert into VAR_HTMLTEXT (VHT_PAGENAME,VHT_NUMBER,VHT_HTMLTEXT,VHT_CREATEDON,VHT_CREATEDBYID,VHT_CHANGEDON,VHT_CHANGEDBYID) values ('index.jsp',0,'<table width="500" border="0" cellspacing="1" cellpadding="4" bgcolor="#ffffff">
  <tr>
     <td class="header">
       Proxy Registration System - Homepage
     </td>
  </tr>
  <tr>
    <td class="tableText">
      You are viewing the home page of the Proxy Registration System. The Proxy Registration System, or PRS, is a signaturebook that lists all employees of Nationale Nederlanden who are authorized to sign on behalf of Nationale Nederlanden (proxies). You also can request, change or withdraw your own proxy.
    </td>
  </tr>
  <tr>
    <td class="tableText">
      <br><br>
      <a href="searchproxy.jsp?LOGINTYPE=NN">Click here for the signature book</a>
    </td>
  </tr>
  <tr>
    <td class="tableText">
      <a href="applicationstart.jsp?LOGINTYPE=NN">Click here to request, change or withdraw your own proxy</a>
    </td>
  </tr>
  <tr>
    <td class="tableText">
      <a href="javascript:prs_popla(''aboutproxies.jsp?id=0'',''380'',''500'')">Click here for information about proxies</a>
    </td>
  </tr>
  <tr>
    <td class="tableText">
      <br><br><br><br><br><br><br><br>
      <a href="javascript:prs_popla(''vartextdisplay_pop.jsp?id=0'',''380'',''500'')">Disclaimer</a>
    </td>
  </tr>
</table>',sysdate(),1,sysdate(),1);


Insert into VAR_HTMLTEXT (VHT_PAGENAME,VHT_NUMBER,VHT_HTMLTEXT,VHT_CREATEDON,VHT_CREATEDBYID,VHT_CHANGEDON,VHT_CHANGEDBYID) values ('applicationproxyactions.jsp',0,'<table width="500" border="0" cellspacing="1" cellpadding="4" bgcolor="#ffffff">
<tr><td class="header">
Attention
</td></tr>
<tr><td class="tableText">
The system cannot proces your request, please read the remark carefully.
</td></tr>
</table>
',sysdate(),1,sysdate(),1);
Insert into VAR_HTMLTEXT (VHT_PAGENAME,VHT_NUMBER,VHT_HTMLTEXT,VHT_CREATEDON,VHT_CREATEDBYID,VHT_CHANGEDON,VHT_CHANGEDBYID) values ('applicationproxyactions.jsp',1,'<table width="500" border="0" cellspacing="1" cellpadding="4" bgcolor="#ffffff">
<tr><td class="header">
Remark</td></tr>
<tr><td class="tableText">
A proxy is registered for this legal entity.
To change this proxy, click on return and read the instruction.
<br><br>
If you need an extra proxy for this legal entity please contact us.
</td></tr>
</table><br>',sysdate(),1,sysdate(),1);
Insert into VAR_HTMLTEXT (VHT_PAGENAME,VHT_NUMBER,VHT_HTMLTEXT,VHT_CREATEDON,VHT_CREATEDBYID,VHT_CHANGEDON,VHT_CHANGEDBYID) values ('applicationproxyactions.jsp',2,'<table width="500" border="0" cellspacing="1" cellpadding="4" bgcolor="#ffffff">
<tr><td class="header">
Remark</td></tr>
<tr><td class="tableText">
You already have a request for a new proxy for this legal entity. If you want to correct it, click on ok.
<br><br>
If you need an extra proxy for this legal entity please contact us.
</td></tr>
</table><br>',sysdate(),1,sysdate(),1);
Insert into VAR_HTMLTEXT (VHT_PAGENAME,VHT_NUMBER,VHT_HTMLTEXT,VHT_CREATEDON,VHT_CREATEDBYID,VHT_CHANGEDON,VHT_CHANGEDBYID) values ('applicationproxyactions.jsp',4,'<table width="500" border="0" cellspacing="1" cellpadding="4" bgcolor="#ffffff">
<tr><td class="header">
Remark</td></tr>
<tr><td class="tableText">
You already have a request for a new proxy and for a withdrawal. If you want to correct the request for a new proxy, click on ok.
<br><br>
If you need an extra proxy for this legal entity please contact us.
</td></tr>
</table>',sysdate(),1,sysdate(),1);
Insert into VAR_HTMLTEXT (VHT_PAGENAME,VHT_NUMBER,VHT_HTMLTEXT,VHT_CREATEDON,VHT_CREATEDBYID,VHT_CHANGEDON,VHT_CHANGEDBYID) values ('applicationproxyactions.jsp',5,'<table width="500" border="0" cellspacing="1" cellpadding="4" bgcolor="#ffffff">
<tr><td class="header">
Remark</td></tr>
<tr><td class="tableText">
You already have a request for change for this entity. If you want to correct it, click on ok.
</td></tr>
</table><br>',sysdate(),1,sysdate(),1);
Insert into VAR_HTMLTEXT (VHT_PAGENAME,VHT_NUMBER,VHT_HTMLTEXT,VHT_CREATEDON,VHT_CREATEDBYID,VHT_CHANGEDON,VHT_CHANGEDBYID) values ('applicationproxyactions.jsp',11,'<table width="500" border="0" cellspacing="1" cellpadding="4" bgcolor="#ffffff">
<tr><td class="header">
Remark</td></tr>
<tr><td class="tableText">
Changes for E/F proxies can only be done by creating a request for withdrawal and creating a request for a new proxy. Click on return.</td></tr>
</table><br>',sysdate(),1,sysdate(),1);
Insert into VAR_HTMLTEXT (VHT_PAGENAME,VHT_NUMBER,VHT_HTMLTEXT,VHT_CREATEDON,VHT_CREATEDBYID,VHT_CHANGEDON,VHT_CHANGEDBYID) values ('applicationproxy.jsp',0,'<table width="500" border="0" cellspacing="1" cellpadding="4" bgcolor="#ffffff">
  <tr>
    <td class="header">
      Request for a new proxy
    </td>
  </tr>
  <tr>
    <td class="tableText">
      Please enter the necessary data and click on ok.
      <br><br>
    </td>
  </tr>
</table>',sysdate(),1,sysdate(),1);
Insert into VAR_HTMLTEXT (VHT_PAGENAME,VHT_NUMBER,VHT_HTMLTEXT,VHT_CREATEDON,VHT_CREATEDBYID,VHT_CHANGEDON,VHT_CHANGEDBYID) values ('applicationstnewform.jsp',0,'<table width="500" border="0" cellspacing="1" cellpadding="4" bgcolor="#ffffff">
<tr><td class="header">
Request for a new proxy
</td></tr>
<tr>
  <td class="tableText">
    Please enter the necessary data for a new proxy<br>
    and click on ok.
  </td>
  <td class="prserror">
       <span>
          <img src="images/prs_error_exclamation.gif" width="16" height="16" vspace="0" hspace="0" border="0" alt="" />
          <b>&nbsp;scroll down&nbsp;</b>
          <img src="images/prs_error_exclamation.gif" width="18" height="18" vspace="0" hspace="0" border="0" alt="" />
       </span>
  </td>
</tr>
</table>
<BR>',sysdate(),1,sysdate(),1);
Insert into VAR_HTMLTEXT (VHT_PAGENAME,VHT_NUMBER,VHT_HTMLTEXT,VHT_CREATEDON,VHT_CREATEDBYID,VHT_CHANGEDON,VHT_CHANGEDBYID) values ('applicationdisplay.jsp',0,'<table width="500" border="0" cellspacing="1" cellpadding="4" bgcolor="#ffffff">
  <tr>
     <td colspan="2" class="header">
       Summary of request
     </td>
  </tr>
  <tr>
    <td class="tableText" valign=top">
      Please check the data and click on ok and print.<br>
      If you want to correct the data click on cancel.
    </td>
    <td class="prserror">
       <span>
          <img src="images/prs_error_exclamation.gif" width="16" height="16" vspace="0" hspace="0" border="0" alt="" />
          <b>scroll down</b>
          <img src="images/prs_error_exclamation.gif" width="18" height="18" vspace="0" hspace="0" border="0" alt="" />
       </span>
    </td>
  </tr>
</table>',sysdate(),1,sysdate(),1);
Insert into VAR_HTMLTEXT (VHT_PAGENAME,VHT_NUMBER,VHT_HTMLTEXT,VHT_CREATEDON,VHT_CREATEDBYID,VHT_CHANGEDON,VHT_CHANGEDBYID) values ('applicationproxyactions.jsp',6,'<table width="500" border="0" cellspacing="1" cellpadding="4" bgcolor="#ffffff">
<tr><td class="header">
Remark</td></tr>
<tr><td class="tableText">
You already have a request for withdrawal for this entity. If you want to replace it by a change, click on ok.
</td></tr>
</table><br>',sysdate(),1,sysdate(),1);
Insert into VAR_HTMLTEXT (VHT_PAGENAME,VHT_NUMBER,VHT_HTMLTEXT,VHT_CREATEDON,VHT_CREATEDBYID,VHT_CHANGEDON,VHT_CHANGEDBYID) values ('applicationstchangeform.jsp',0,'<table width="500" border="0" cellspacing="1" cellpadding="4" bgcolor="#ffffff">
<tr><td class="header">
Request for a change of a proxy
</td></tr>
<tr><td class="tableText">
Please enter the necessary data for a change and click on ok.
</td></tr>
</table>
<BR>',sysdate(),1,sysdate(),1);
Insert into VAR_HTMLTEXT (VHT_PAGENAME,VHT_NUMBER,VHT_HTMLTEXT,VHT_CREATEDON,VHT_CREATEDBYID,VHT_CHANGEDON,VHT_CHANGEDBYID) values ('applicationstcancelform.jsp',0,'<table width="500" border="0" cellspacing="1" cellpadding="4" bgcolor="#ffffff">
<tr><td class="header">
Request for a withdrawal of a proxy
</td></tr>
<tr><td class="tableText">
Please enter the necessary data for a withdrawal and click on ok.
</td></tr>
</table>
<BR>',sysdate(),1,sysdate(),1);
Insert into VAR_HTMLTEXT (VHT_PAGENAME,VHT_NUMBER,VHT_HTMLTEXT,VHT_CREATEDON,VHT_CREATEDBYID,VHT_CHANGEDON,VHT_CHANGEDBYID) values ('requesterform.jsp',0,'<table width="500" border="0" cellspacing="1" cellpadding="4" bgcolor="#ffffff">
<tr><td class="header">
Information contact person
</td></tr>
<tr><td class="tableText">
Please enter the necessary data and click on ok.
</td></tr>
</table><BR>',sysdate(),1,sysdate(),1);
Insert into VAR_HTMLTEXT (VHT_PAGENAME,VHT_NUMBER,VHT_HTMLTEXT,VHT_CREATEDON,VHT_CREATEDBYID,VHT_CHANGEDON,VHT_CHANGEDBYID) values ('contactus.jsp',0,'<table width="300" border="0" cellspacing="1" cellpadding="4" bgcolor="#ffffff">
  <tr>
    <td class="tableBdalt">
      If you would like to contact us regarding proxies and procedures please contact for:
    </td>
  </tr>
  <tr>
    <td class="tableBdalt" valign=top">
      ING Nederland (VK)
      <br>
      <a href="mailto:NN@noog.onbekend">INGNederland@nog.onbekend<a>
      <br><br>
  </tr>

  <tr>
    <td class="tableBdalt">
      Staff Departments
      <br>
      <a href="mailto:CLCD.Corporate.Services@mail.ing.nl">CLCD.Corporate.Services@mail.ing.nl<a>
      <br><br>
    </td>
  </tr>

  <tr>
    <td valign="bottom" class="tableBdalt">
      <br><br><br>
      System Supervisor
      <br>
      <a href="mailto:CLCD.Corporate.Services@mail.ing.nl">CLCD.Corporate.Services@mail.ing.nl<a>
      <br><br>
    </td>
  </tr>
</table>

<br><br>',sysdate(),1,sysdate(),1);
Insert into VAR_HTMLTEXT (VHT_PAGENAME,VHT_NUMBER,VHT_HTMLTEXT,VHT_CREATEDON,VHT_CREATEDBYID,VHT_CHANGEDON,VHT_CHANGEDBYID) values ('applicationproxyactions.jsp',9,'<table width="500" border="0" cellspacing="1" cellpadding="4" bgcolor="#ffffff">
<tr><td class="header">
Remark</td></tr>
<tr><td class="tableText">
You already have a request for change for this entity. If you want to replace it with a request for withdrawal, click on ok.
</td></tr>
</table><br>',sysdate(),1,sysdate(),1);
Insert into VAR_HTMLTEXT (VHT_PAGENAME,VHT_NUMBER,VHT_HTMLTEXT,VHT_CREATEDON,VHT_CREATEDBYID,VHT_CHANGEDON,VHT_CHANGEDBYID) values ('applicationproxyactions.jsp',8,'<table width="500" border="0" cellspacing="1" cellpadding="4" bgcolor="#ffffff">
<tr><td class="header">
Remark</td></tr>
<tr><td class="tableText">
You already have a request for a withdrawal. If you want to correct it, click on ok.
</td></tr>
</table><br>',sysdate(),1,sysdate(),1);
Insert into VAR_HTMLTEXT (VHT_PAGENAME,VHT_NUMBER,VHT_HTMLTEXT,VHT_CREATEDON,VHT_CREATEDBYID,VHT_CHANGEDON,VHT_CHANGEDBYID) values ('applicationproxyactions.jsp',7,'<table width="500" border="0" cellspacing="1" cellpadding="4" bgcolor="#ffffff">
<tr><td class="header">
Remark</td></tr>
<tr><td class="tableText">
You already have a request for a new proxy and for a withdrawal. If you want to replace these with a request for change, click on ok.
</td></tr>
</table><br>',sysdate(),1,sysdate(),1);
Insert into VAR_HTMLTEXT (VHT_PAGENAME,VHT_NUMBER,VHT_HTMLTEXT,VHT_CREATEDON,VHT_CREATEDBYID,VHT_CHANGEDON,VHT_CHANGEDBYID) values ('vartextdisplay_pop.jsp',0,'<table width="300">
<tr>
<td width="300">
 <form>
 <br/>
 <div class="header">DISCLAIMER<br />
 <img src="images/hr_lightblue.gif" width="300" height="9" border="0" alt="" />
 </div>

      <table width="300" border="0" cellspacing="1" cellpadding="4" bgcolor="#ffffff">
        <tr>
          <td class="tableBdalt">
             This site contains the digital signature book
             which lists the proxies of Nationale Nederlanden. This digital signature book is linked to the Proxy Registration System (PRS).
             <br><br>
             This book is for use by NN employees and is intended to provide a means of checking the proxies of NN employees. The data contained in this digital signature book may not be used for improper purposes.
             <br><br>
             The Corporate Legal, Compliance & Security Department is the custodian of this digital signature book. The three administrative addresses have devoted great care to the composition of the digital signature book and ensuring its completeness. However, the authorized signatory and the line management of the individual proxies are responsible for ensuring that the particulars of the authorization are correct and up to date. It follows that Corporate Legal, Compliance & Security Department can accept no liability for damage that results from anything in the digital signature book that is not correct.
          </td>
        </tr>
	  </table>',sysdate(),1,sysdate(),1);
Insert into VAR_HTMLTEXT (VHT_PAGENAME,VHT_NUMBER,VHT_HTMLTEXT,VHT_CREATEDON,VHT_CREATEDBYID,VHT_CHANGEDON,VHT_CHANGEDBYID) values ('applicationproxyactions.jsp',12,'<table width="500" border="0" cellspacing="1" cellpadding="4" bgcolor="#ffffff">
<tr><td class="header">
Remark</td></tr>
<tr><td class="tableText">
You already have a request for this legal entity that is being processed. This request has to be processed first before you will be able to enter a new request. For more information please contact us.
</td></tr>
</table><br>',sysdate(),1,sysdate(),1);
Insert into VAR_HTMLTEXT (VHT_PAGENAME,VHT_NUMBER,VHT_HTMLTEXT,VHT_CREATEDON,VHT_CREATEDBYID,VHT_CHANGEDON,VHT_CHANGEDBYID) values ('searchproxy.jsp',0,'<table width="500" border="0" cellspacing="1" cellpadding="4" bgcolor="#ffffff">
<tr><td class="header">
Signature book
</td></tr>
<tr><td class="tableText">
In this signaturebook you can find the proxies and related signatures of NN employees
who are authorized to sign on behalf of ING Bank N.V. and/or Postbank N.V. (authorized
signatories).
</td></tr>
<tr><td class="tableText">
There are several ways to search:
</td></tr>
<tr><td class="tableText">
- by surname or part thereof
<br>
</td></tr>
</table>
',sysdate(),1,sysdate(),1);
Insert into VAR_HTMLTEXT (VHT_PAGENAME,VHT_NUMBER,VHT_HTMLTEXT,VHT_CREATEDON,VHT_CREATEDBYID,VHT_CHANGEDON,VHT_CHANGEDBYID) values ('disclaimer_print.html',0,'This signature book lists the proxies of (in)direct subsidiaries of ING Groep N.V.<br><br>This book is intended to provide a means of checking the proxies of ING employees. The data contained in this signature book may not be used for improper purposes. You agree to hold ING Groep N.V. and its (in)direct subsidiaries harmless from any and all claims and expenses arising from the improper use of any content of this signaturebook.<br><br>The Corporate Legal, Compliance & Security Department of ING Group is the custodian of this signature book. ING Group has devoted great care to the composition of the signature book and ensuring its completeness. However, the authorized signatory and the line management of the individual proxies are responsible for ensuring that the particulars of the authorization are correct and up to date. It follows neither ING Groep N.V. nor (in)direct subsidiaries of can accept any liability for damage that results from anything in the signature book that is not correct.',sysdate(),1,sysdate(),1);
Insert into VAR_HTMLTEXT (VHT_PAGENAME,VHT_NUMBER,VHT_HTMLTEXT,VHT_CREATEDON,VHT_CREATEDBYID,VHT_CHANGEDON,VHT_CHANGEDBYID) values ('disclaimer_cdrom.html',0,'<!-- saved from url=(0022)http://internet.e-mail -->
This CD-ROM contains the digital signature book which lists the proxies of (in)direct subsidiaries of ING Groep N.V.<br><br>This book is intended to provide a means of checking the proxies of ING employees. The data contained in this digital signature book may not be used for improper purposes. You agree to hold ING Groep N.V. and its (in)direct subsidiaries harmless from any and all claims and expenses arising from the improper use of any content of this CD-ROM.<br><br>The Corporate Legal, Compliance & Security Department of ING Group is the custodian of this CD-ROM. ING Group has devoted great care to the composition of the signature book and ensuring its completeness. However, the authorized signatory and the line management of the individual proxies are responsible for ensuring that the particulars of the authorization are correct and up to date. It follows  neither ING Groep N.V. nor (in)direct subsidiaries of can accept any liability for damage that results from anything in the digital signature book that is not correct.',sysdate(),1,sysdate(),1);
Insert into VAR_HTMLTEXT (VHT_PAGENAME,VHT_NUMBER,VHT_HTMLTEXT,VHT_CREATEDON,VHT_CREATEDBYID,VHT_CHANGEDON,VHT_CHANGEDBYID) values ('applicationredirectpdflang.jsp',0,'<table width="500" border="0" cellspacing="1" cellpadding="4" bgcolor="#ffffff">
   <tr>
      <td class="tableBdalt12">Please select the language for the prints you are about to make</td>
   </tr>
</table>',sysdate(),1,sysdate(),1);
Insert into VAR_HTMLTEXT (VHT_PAGENAME,VHT_NUMBER,VHT_HTMLTEXT,VHT_CREATEDON,VHT_CREATEDBYID,VHT_CHANGEDON,VHT_CHANGEDBYID) values ('proxytype_popup.jsp',0,'<table width="300" border="0" cellspacing="1" cellpadding="4" bgcolor="#ffffff">
   <tr>
      <td class="tableBdalt">
A request for the proxy type you selected has to be entered and processed by the administration address mentioned below. Please contact us for further information
	  </td>
   </tr>
</table>',sysdate(),1,sysdate(),1);
Insert into VAR_HTMLTEXT (VHT_PAGENAME,VHT_NUMBER,VHT_HTMLTEXT,VHT_CREATEDON,VHT_CREATEDBYID,VHT_CHANGEDON,VHT_CHANGEDBYID) values ('proxy_scheme_int.jsp',11,'g="0" cellpadding="0">

	<tr><td  class="tableBdAlt" colspan="3"><br><A NAME="type_c1"></A>
	<B>Category C1 attorney:</B> officer having special power to
	represent the company;
	</td></tr>
	<tr><td width="10" class="tableBdalt" valign="top">-</td>
	<td colspan="2" class="tableBdalt" valign="top">
		empowered to perform the legal transactions referred
   		to in block 2, provided that he acts:
	</td></tr>
	<tr><td width="10" class="tableBdalt" valign="top">-</td>
	<td colspan="2" class="tableBdalt" valign="top">
		jointly with a category S, A or C1 attorney;
	</td></tr>
	<tr><td width="10" class="tableBdalt" valign="top">-</td>
	<td colspan="2" class="tableBdalt" valign="top">
		jointly with a category C2, provided the amount
   		involved does not exceed EUR 5,000,000, or
	</td></tr>
	<tr><td width="10" class="tableBdalt" valign="top">-</td>
	<td colspan="2" class="tableBdalt" valign="top">
		jointly with a category D attorney, provided the
   		amount involved does not exceed EUR 1,000,000;
	</td></tr>
	<tr><td width="10" class="tableBdalt" valign="top">-</td>
	<td colspan="2" class="tableBdalt" valign="top">
		empowered to perform the legal transactions referred
	   to in block 3, provided the amount involved does not
	   exceed EUR 1,000,000;
	</td></tr>
	<tr><td  class="tableBdAltIta" colspan="3">
		including, provided that he acts jointly with a category
		S, A, C1 or C2 attorney, as applicable, the power to
		delegate the power of representation that he has with
		that attorney to a third party, provided that the
		substitution occurs in respect of a concrete transaction
		which is specifically and accurately described in writing
		in the authorisation concerning the substitution and is
		intended solely to enable the thi',sysdate(),1,sysdate(),1);
Insert into VAR_HTMLTEXT (VHT_PAGENAME,VHT_NUMBER,VHT_HTMLTEXT,VHT_CREATEDON,VHT_CREATEDBYID,VHT_CHANGEDON,VHT_CHANGEDBYID) values ('proxy_scheme_int.jsp',12,'rd party to appear on
		behalf of the company in order to execute an instrument
		before a civil-law notary in respect of the above-
		mentioned concrete transaction, to which third party
		independent authority may be granted for the company with
		respect to the aforementioned transaction.
	</td></tr>

</table>
</td></tr></table>

<table width="620" border="0" cellspacing="0" cellpadding="0">
<tr>  <td width="20" class="tableBdalt" valign="top">&nbsp;</td> <td width="600" class="tableBdalt">
<!-- end classification and start of the schema -->
<table width="600" border="0" cellspacing="0" cellpadding="0">

	<tr><td  class="tableBdAlt" colspan="3"><br><A NAME="type_c2"></A>
	<B>Category C2 attorney:</B> officer having special power to
	represent the company;
	</td></tr>
<tr><td width="10" class="tableBdalt" valign="top">-</td>
	<td colspan="2" class="tableBdalt" valign="top">
		empowered to perform the legal transactions referred
  		to in block 2:
	</td></tr>
	<tr><td width="10" class="tableBdalt" valign="top">-</td>
	<td colspan="2" class="tableBdalt" valign="top">
		jointly with a category S, A, C1 or C2 attorney,
	  provided the amount involved does not exceed EUR 5,000,000,
	  or
	</td></tr>
	<tr><td width="10" class="tableBdalt" valign="top">-</td>
	<td colspan="2" class="tableBdalt" valign="top">
		jointly with a category D attorney, provided the
  		amount involved does not exceed EUR 1,000,000;
	</td></tr>
	<tr><td width="10" class="tableBdalt" valign="top">-</td>
	<td colspan="2" class="tableBdalt" valign="top">
		empowered to perform the legal transactions referred to
	  in block 3, provided the amount involved does not
	  exceed EUR 1,000,000;
	</td></tr>

	<tr><td  class="tableBdAltIta" colspan="3">
		including',sysdate(),1,sysdate(),1);
Insert into VAR_HTMLTEXT (VHT_PAGENAME,VHT_NUMBER,VHT_HTMLTEXT,VHT_CREATEDON,VHT_CREATEDBYID,VHT_CHANGEDON,VHT_CHANGEDBYID) values ('proxy_scheme_int.jsp',10,'he legal transactions referred
  		to in block 1, provided that he acts jointly with a
  		category S or A attorney;
	</td></tr>
	<tr><td width="10" class="tableBdalt" valign="top">-</td>
	<td colspan="2" class="tableBdalt" valign="top">
		empowered to perform the legal transactions referred
  		to in block 3, provided the amount involved does not
  		exceed EUR 1,000,000;
	</td></tr>
	<tr><td  class="tableBdAltIta" colspan="3">
		including, provided that he acts jointly with a category
		S or A attorney, the power to delegate the power of
		attorney to a third party, provided that the substitution
		occurs only in respect of a concrete transaction which is
		specifically and accurately described in writing in the
		authorisation concerning the substitution, to which third
		party independent authority may be granted for the
		company with respect to the aforementioned transaction.
	</td></tr>

	<tr><td colspan="2" class="tableBdalt" valign="top"><BR>_____________________________</td></tr>
	<tr><td width="10" class="tableBdalt" valign="top">(2)
	</td><td width="590" class="tableBdalt">An amount  specified in euros in these regulations shall  include the corresponding amount in foreign currency, freely convertible into euros at the exchange rate on the date of the relevant offer/transaction.</td></tr>


</table>

<!-- Pagebreak so close and reopen outer table  -->
</td></tr></table>
<PAGE_MARKER_SIGNATUREBOOK>
<PAGE_MARKER_SIGNATUREBOOK>
<div class="prspagebreakafter">&nbsp;</div>

<table width="620" border="0" cellspacing="0" cellpadding="0">
<tr>  <td width="20" class="tableBdalt" valign="top">&nbsp;</td> <td width="600" class="tableBdalt">
<!-- end classification and start of the schema -->
<table width="600" border="0" cellspacin',sysdate(),1,sysdate(),1);
Insert into VAR_HTMLTEXT (VHT_PAGENAME,VHT_NUMBER,VHT_HTMLTEXT,VHT_CREATEDON,VHT_CREATEDBYID,VHT_CHANGEDON,VHT_CHANGEDBYID) values ('applicationstart.jsp',0,'<table width="500" border="0" cellspacing="1" cellpadding="4" bgcolor="#ffffff">
  <tr>
    <td class="tableText">
      In order to obtain the request form for a proxy, enter the necessary data.
      To start please enter your  personnel number and click on go.
    </td>
  </tr>
</table>',sysdate(),1,sysdate(),1);
Insert into VAR_HTMLTEXT (VHT_PAGENAME,VHT_NUMBER,VHT_HTMLTEXT,VHT_CREATEDON,VHT_CREATEDBYID,VHT_CHANGEDON,VHT_CHANGEDBYID) values ('proxyapplicationpersonsel.jsp',0,'<table width="500" border="0" cellspacing="1" cellpadding="4" bgcolor="#ffffff">
  <tr>
     <td colspan="2" class="header">
       Proxies and requests
     </td>
  </tr>
  <tr>
    <td colspan="2" class="tableText">
      List of current proxies and/or requests.
    </td>
  </tr>
  <tr>
    <td class="tableText" valign=top">
      -
    </td>
    <td class="tableText">
      If you want to request a new proxy click on new.
    </td>
  </tr>
  <tr>
    <td class="tableText" valign=top">
      -
    </td>
    <td class="tableText">
      If you want to change or withdraw an existing proxy, select the proxy and click on the specific button.
    </td>
  </tr>
  <tr>
    <td class="tableText" valign=top">
      -
    </td>
    <td class="tableText">
      If you want to correct a request, select the request and click on correction.
      <br><br><br>
    </td>
  </tr>
</table>',sysdate(),1,sysdate(),1);
Insert into VAR_HTMLTEXT (VHT_PAGENAME,VHT_NUMBER,VHT_HTMLTEXT,VHT_CREATEDON,VHT_CREATEDBYID,VHT_CHANGEDON,VHT_CHANGEDBYID) values ('applicationstart.jsp',1,'<table width="500" border="0" cellspacing="1" cellpadding="4" bgcolor="#ffffff">
  <tr>
    <td class="tableText">
      <br><br><br>
      For a change and withdrawal request, you can also search:
    </td>
  </tr>
  <tr>
    <td class="tableText">
      -&nbsp;&nbsp;&nbsp;by the name of the organisational unit or part thereof
      <br>
      -&nbsp;&nbsp;&nbsp;or by using the organisational tree
    </td>
  </tr>
</table>',sysdate(),1,sysdate(),1);
Insert into VAR_HTMLTEXT (VHT_PAGENAME,VHT_NUMBER,VHT_HTMLTEXT,VHT_CREATEDON,VHT_CREATEDBYID,VHT_CHANGEDON,VHT_CHANGEDBYID) values ('applicationdisplay.jsp',1,'<table width="500" border="0" cellspacing="1" cellpadding="4" bgcolor="#ffffff">
  <tr>
     <td colspan="2" class="header">
       Finalized request
     </td>
  </tr>
  <tr>
    <td width ="340" class="tableText" valign=top">
      The request is finalized. Please read the instructions carefully.
      We will register your request, after receiving the signed forms.
      <BR><BR>
      If you click on return you will find your request listed.
    </td>
    <td class="prserror" VALIGN="BOTTOM">
       <span>
          <img src="images/prs_error_exclamation.gif" width="16" height="16" vspace="0" hspace="0" border="0" alt="" />
          <b>&nbsp;scroll down&nbsp;</b>
          <img src="images/prs_error_exclamation.gif" width="18" height="18" vspace="0" hspace="0" border="0" alt="" />
       </span>
    </td>
  </tr>
</table>',sysdate(),1,sysdate(),1);
Insert into VAR_HTMLTEXT (VHT_PAGENAME,VHT_NUMBER,VHT_HTMLTEXT,VHT_CREATEDON,VHT_CREATEDBYID,VHT_CHANGEDON,VHT_CHANGEDBYID) values ('aboutproxies.jsp',0,'<script type="text/javascript" language="javascript">
function prs_popla(url,wid,hgt) {
    prs_secondwindow = window.open (url,"hoplaX", "toolbar=no,width="+wid+",height="+hgt+",directories=no,status=no,scrollbars=yes,resizable=yes,menubar=no,location=no");
    if(!prs_secondwindow.opener) {
          if (!document.layers) {
      prs_secondwindow.opener = window;
        }
    }
    if (!document.layers) {
      eval("try {prs_secondwindow.focus();}catch (e) {alert(''The window was already open and will be re-opened.'');prs_secondwindow.focus();}");
    }
    else {
      prs_secondwindow.focus();
    }
}
</script>
<table width="300" border="0" cellspacing="1" cellpadding="4" bgcolor="#ffffff">
   <tr>
      <td class="tableBdalt">
A Statutory Director or other authorised signatory has the power to represent a company. Such power of representation is granted by or on behalf of the executive board of the company concerned.        ING Group has a vast number of subsidiaries in the Netherlands and abroad. Many of these legal entities have their own specific, business-linked, powers of representation.<br><br>
To view the proxy regulation of a legal entity click on the specific link below.
	  </td>
   </tr>
</table>',sysdate(),1,sysdate(),1);
Insert into VAR_HTMLTEXT (VHT_PAGENAME,VHT_NUMBER,VHT_HTMLTEXT,VHT_CREATEDON,VHT_CREATEDBYID,VHT_CHANGEDON,VHT_CHANGEDBYID) values ('faqpage.jsp',0,'      <table width="300" border="0" cellspacing="1" cellpadding="4" bgcolor="#ffffff">
        <tr>
          <td class="tableHd">
		  	General
		  </td>
        </tr>
        <tr>
          <td class="tableBdalt">
		  	At this moment there is no information available yet.
		  </td>
        </tr>
	  </table>
',sysdate(),1,sysdate(),1);
Insert into VAR_HTMLTEXT (VHT_PAGENAME,VHT_NUMBER,VHT_HTMLTEXT,VHT_CREATEDON,VHT_CREATEDBYID,VHT_CHANGEDON,VHT_CHANGEDBYID) values ('searchproxy.jsp',1,'<table width="500" border="0" cellspacing="1" cellpadding="4" bgcolor="#ffffff">
<tr><td class="tableText">
<br><br>
- by the name of the organisational unit or part thereof
</td></tr>
<tr><td class="tableText">
- using the organisational chart
</td></tr>
</table>
',sysdate(),1,sysdate(),1);
Insert into VAR_HTMLTEXT (VHT_PAGENAME,VHT_NUMBER,VHT_HTMLTEXT,VHT_CREATEDON,VHT_CREATEDBYID,VHT_CHANGEDON,VHT_CHANGEDBYID) values ('proxyapplicationpersonsel.jsp',1,'You already have a request for this legal entity that is being processed. This request has to be processed first before you will be able to correct the request you selected. For more information please contact us.',sysdate(),1,sysdate(),1);
Insert into VAR_HTMLTEXT (VHT_PAGENAME,VHT_NUMBER,VHT_HTMLTEXT,VHT_CREATEDON,VHT_CREATEDBYID,VHT_CHANGEDON,VHT_CHANGEDBYID) values ('proxy_scheme_nn.jsp',11,'g="0" cellpadding="0">

	<tr><td  class="tableBdAlt" colspan="3"><br><A NAME="type_c1"></A>
	<B>Category C1 attorney:</B> officer having special power to
	represent the company;
	</td></tr>
	<tr><td width="10" class="tableBdalt" valign="top">-</td>
	<td colspan="2" class="tableBdalt" valign="top">
		empowered to perform the legal transactions referred
   		to in block 2, provided that he acts:
	</td></tr>
	<tr><td width="10" class="tableBdalt" valign="top">-</td>
	<td colspan="2" class="tableBdalt" valign="top">
		jointly with a category S, A or C1 attorney;
	</td></tr>
	<tr><td width="10" class="tableBdalt" valign="top">-</td>
	<td colspan="2" class="tableBdalt" valign="top">
		jointly with a category C2, provided the amount
   		involved does not exceed EUR 5,000,000, or
	</td></tr>
	<tr><td width="10" class="tableBdalt" valign="top">-</td>
	<td colspan="2" class="tableBdalt" valign="top">
		jointly with a category D attorney, provided the
   		amount involved does not exceed EUR 1,000,000;
	</td></tr>
	<tr><td width="10" class="tableBdalt" valign="top">-</td>
	<td colspan="2" class="tableBdalt" valign="top">
		empowered to perform the legal transactions referred
	   to in block 3, provided the amount involved does not
	   exceed EUR 1,000,000;
	</td></tr>
	<tr><td  class="tableBdAltIta" colspan="3">
		including, provided that he acts jointly with a category
		S, A, C1 or C2 attorney, as applicable, the power to
		delegate the power of representation that he has with
		that attorney to a third party, provided that the
		substitution occurs in respect of a concrete transaction
		which is specifically and accurately described in writing
		in the authorisation concerning the substitution and is
		intended solely to enable the thi',sysdate(),1,sysdate(),1);
Insert into VAR_HTMLTEXT (VHT_PAGENAME,VHT_NUMBER,VHT_HTMLTEXT,VHT_CREATEDON,VHT_CREATEDBYID,VHT_CHANGEDON,VHT_CHANGEDBYID) values ('proxy_scheme_nn.jsp',9,'urs
					    only in respect of a concrete transaction which is
					    specifically and accurately described in writing in the
					    authorisation concerning the substitution;
					</td>
				</tr>

				<tr>
					<td width="10" class="tableBdalt" valign="top">&nbsp;</td>
					<td width="30" class="tableBdalt" valign="top">(ii)</td>
					<td width="560" class="tableBdalt">
						provided that he acts jointly with a category C1 or
					    C2 attorney and provided that the substitution occurs
					    only in respect of a concrete transaction which is
					    specifically and accurately described in writing in the
					    authorisation concerning the substitution, and is
					    intended solely to enable the third party to appear on
					    behalf of the company in order to execute an instrument
					    before a civil-law notary in respect of the above-
					    mentioned transaction;
					</td>
				</tr>
				<tr>
					<td  class="tableBdAltIta" colspan="3">
						to which third party an independent power to represent
						the company may be granted with regard to a transaction
						as referred to under (i) and (ii), respectively.
					</td>
				</tr>
		</table>
		</td></tr>
</table>

<table width="620" border="0" cellspacing="0" cellpadding="0">
<tr>  <td width="20" class="tableBdalt" valign="top">&nbsp;</td>
<td valign="top" width="600" class="tableBdalt">

<table width="600"  border="0" cellspacing="0" cellpadding="0">

	<tr><td  class="tableBdAlt" colspan="3"><br><A NAME="type_b"></A>
	<B>Category B attorney:</B> officer having special power to
	represent the company;
	</td></tr>
	<tr><td width="10" class="tableBdalt" valign="top">-</td>
	<td colspan="2" class="tableBdalt" valign="top">
		empowered to perform t',sysdate(),1,sysdate(),1);
Insert into VAR_HTMLTEXT (VHT_PAGENAME,VHT_NUMBER,VHT_HTMLTEXT,VHT_CREATEDON,VHT_CREATEDBYID,VHT_CHANGEDON,VHT_CHANGEDBYID) values ('proxy_scheme_nn.jsp',31,'ually general managers of
			business units, are authorised by the Executive Board of
			ING Group to grant authorities within their business unit
			as described in the external power-of-attorney
			regulations.
		</td>
	</tr>
	<tr>
		<td colspan="3"  class="tableBd">
			<br><b><i>Application for, changes in or withdrawal of power of attorney</i></b>
		</td>
	</tr>
	<tr>
		<td colspan="3"  class="tableBdAltIta">
			External power of attorney may be applied for on
			<u>www.prs.intranet</u>. An employee''s immediate supervisor
			decides whether the employee is eligible for external
			power of attorney. The designated line manager of the
			relevant business unit approves an application or change
			by signing the grant form. The form is then sent to the
			business unit contact/co-ordinator (see <u>www.prs.intranet</u>)
			for further processing.
		</td>
	</tr>
</table>
</table>',sysdate(),1,sysdate(),1);
Insert into VAR_HTMLTEXT (VHT_PAGENAME,VHT_NUMBER,VHT_HTMLTEXT,VHT_CREATEDON,VHT_CREATEDBYID,VHT_CHANGEDON,VHT_CHANGEDBYID) values ('proxy_scheme_nn.jsp',30,'th a category
			<b>S</b>, <b>A</b> or <b>B attorney</b>, may
			delegate the authority that they have jointly to a third
			party, provided that this authority is limited to a
			narrowly defined transaction. It is then no longer
			necessary for that third party to act with another person
			competent to act.
		</td>
	</tr>
</table>
</table>
<PAGE_MARKER_SIGNATUREBOOK>
<PAGE_MARKER_SIGNATUREBOOK>
<div class="prspagebreakafter">&nbsp;</div>

<table width="620" border="0" cellspacing="0" cellpadding="0">
<tr>  <td width="20" class="tableBdalt" valign="top">&nbsp;</td> <td width="600" class="tableBdalt">
<!-- end classification and start of the schema -->
<table width="600" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td colspan="3"  class="tableBdAltIta">
			<br>Finally, a category <b>C1</b> or <b>C2 attorney</b>, along with a
			category <b>S</b>, <b>A</b>, <b>B</b>, <b>C1</b> or <b>C2 attorney</b>,
			may delegate the authority that they have jointly to a person attached to
			a civil-law notary''s office in connection with a
			transaction that requires a notarial instrument, provided
			that the authority granted to the person is limited to a
			narrowly defined transaction (see the power of
			substitution above).
		</td>
	</tr>
	<tr>
		<td colspan="3"  class="tableBd">
			<br><b>Procedures</b>
		</td>
	</tr>
	<tr>
		<td colspan="3"  class="tableBdAltIta">
			<br>External power of attorney is attached to a position.
			External power of attorney is not granted to an employee
			as a matter of course. External power of attorney may
			only be granted to staff employed by one of ING''s legal
			entities.
		</td>
	</tr>
	<tr>
		<td colspan="3"  class="tableBdAltIta">
			<br>Designated line managers, us',sysdate(),1,sysdate(),1);
Insert into VAR_HTMLTEXT (VHT_PAGENAME,VHT_NUMBER,VHT_HTMLTEXT,VHT_CREATEDON,VHT_CREATEDBYID,VHT_CHANGEDON,VHT_CHANGEDBYID) values ('proxy_scheme_int.jsp',8,'d width="10" class="tableBdalt" valign="top">-</td>
					<td colspan="2" class="tableBdalt" valign="top">
						jointly with a category C1 attorney;
					</td>
				</tr>
				<tr>
					<td width="10" class="tableBdalt" valign="top">-</td>
					<td colspan="2" class="tableBdalt" valign="top">
						jointly with a category C2 attorney, provided the
					  amount involved does not exceed EUR 5,000,000,(2) or
					</td>
				</tr>
				<tr>
					<td width="10" class="tableBdalt" valign="top">-</td>
					<td colspan="2" class="tableBdalt" valign="top">
						jointly with a category D attorney, provided the
					  amount involved does not exceed EUR 1,000,000;
					</td>
				</tr>
				<tr>
					<td width="10" class="tableBdalt" valign="top">-</td>
					<td colspan="2" class="tableBdalt" valign="top">
						empowered to perform the legal transactions referred
					  to in block 3, provided the amount involved does not
					  exceed EUR 1,000,000;
					</td>
				</tr>
		</table>
		</td>
	</tr>
</table>

<table width="620" border="0" cellspacing="0" cellpadding="0">
<tr>  <td width="20" class="tableBdalt" valign="top">&nbsp;</td>
<td valign="top" width="600" class="tableBdalt">

<table width="600"  border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td  class="tableBdAltIta" colspan="3">
					including the power to delegate the power of attorney he
					has jointly with the other attorney to a third party,
					</td>
				</tr>
				<tr>
					<td width="10" class="tableBdalt" valign="top">&nbsp;</td>
					<td width="30" class="tableBdalt" valign="top">(i)</td>
					<td width="560" class="tableBdalt">
						provided that he acts jointly with a category S, A
					    or B attorney and provided that the substitution occ',sysdate(),1,sysdate(),1);
Insert into VAR_HTMLTEXT (VHT_PAGENAME,VHT_NUMBER,VHT_HTMLTEXT,VHT_CREATEDON,VHT_CREATEDBYID,VHT_CHANGEDON,VHT_CHANGEDBYID) values ('proxy_scheme_int.jsp',9,'urs
					    only in respect of a concrete transaction which is
					    specifically and accurately described in writing in the
					    authorisation concerning the substitution;
					</td>
				</tr>

				<tr>
					<td width="10" class="tableBdalt" valign="top">&nbsp;</td>
					<td width="30" class="tableBdalt" valign="top">(ii)</td>
					<td width="560" class="tableBdalt">
						provided that he acts jointly with a category C1 or
					    C2 attorney and provided that the substitution occurs
					    only in respect of a concrete transaction which is
					    specifically and accurately described in writing in the
					    authorisation concerning the substitution, and is
					    intended solely to enable the third party to appear on
					    behalf of the company in order to execute an instrument
					    before a civil-law notary in respect of the above-
					    mentioned transaction;
					</td>
				</tr>
				<tr>
					<td  class="tableBdAltIta" colspan="3">
						to which third party an independent power to represent
						the company may be granted with regard to a transaction
						as referred to under (i) and (ii), respectively.
					</td>
				</tr>
		</table>
		</td></tr>
</table>

<table width="620" border="0" cellspacing="0" cellpadding="0">
<tr>  <td width="20" class="tableBdalt" valign="top">&nbsp;</td>
<td valign="top" width="600" class="tableBdalt">

<table width="600"  border="0" cellspacing="0" cellpadding="0">

	<tr><td  class="tableBdAlt" colspan="3"><br><A NAME="type_b"></A>
	<B>Category B attorney:</B> officer having special power to
	represent the company;
	</td></tr>
	<tr><td width="10" class="tableBdalt" valign="top">-</td>
	<td colspan="2" class="tableBdalt" valign="top">
		empowered to perform t',sysdate(),1,sysdate(),1);
Insert into VAR_HTMLTEXT (VHT_PAGENAME,VHT_NUMBER,VHT_HTMLTEXT,VHT_CREATEDON,VHT_CREATEDBYID,VHT_CHANGEDON,VHT_CHANGEDBYID) values ('proxy_scheme_int.jsp',7,' described above in (iii) above,
       subject to the limitation that the authorisation may be
       delegated only to a third party in order to enable him to
       appear on behalf of the company in order to execute an
       instrument before a civil-law notary in respect of the
       above-mentioned concrete transaction.
</td></tr>
</table>
</td></tr>
</table>
<table width="620" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td width="20" class="tableBdalt" valign="top">&nbsp;</td>
		<td valign="top" width="600" class="tableBdalt">
		<table width="600"  border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td  class="tableBdAlt" colspan="3"><br><A NAME="type_a"></A>
					<B>Category A attorney:</B> officer having general power to
					represent the company:
					</td>
				</tr>
				<tr>
					<td width="10" class="tableBdalt" valign="top">-</td>
					<td colspan="2" class="tableBdalt" valign="top">
						empowered to perform the legal transactions referred
						to in block 1, provided that he acts jointly with a
						category S, A or B attorney;
					</td>
				</tr>
				<tr>
					<td width="10" class="tableBdalt" valign="top">-</td>
					<td colspan="2" class="tableBdalt" valign="top">
						empowered to perform the legal transactions referred
					  to in block 2, provided that he acts:
					</td>
				</tr>
		</table>
		</td>
	</tr>
</table>

<PAGE_MARKER_SIGNATUREBOOK>
<PAGE_MARKER_SIGNATUREBOOK>
<div class="prspagebreakafter">&nbsp;</div>

<table width="620" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td width="20" class="tableBdalt" valign="top">&nbsp;</td>
		<td valign="top" width="600" class="tableBdalt">
		<table width="600"  border="0" cellspacing="0" cellpadding="0">
				<tr>
					<t',sysdate(),1,sysdate(),1);
Insert into VAR_HTMLTEXT (VHT_PAGENAME,VHT_NUMBER,VHT_HTMLTEXT,VHT_CREATEDON,VHT_CREATEDBYID,VHT_CHANGEDON,VHT_CHANGEDBYID) values ('proxy_scheme_int.jsp',5,'" valign="top">&nbsp;</td>
<td valign="top" width="600" class="tableBdalt">

<!-- table for the classification (needs 3 columns) -->
<table width="600"  border="0" cellspacing="0" cellpadding="0">
	<tr>
	<td width="10" class="tableBdalt" valign="top">&nbsp;</td>
	<td width="30" class="tableBdalt" valign="top">&nbsp;</td>
	<td width="560" class="tableBdalt">&nbsp;</td>
	<A NAME="leg_ING"></a><A NAME="leg_PB"></A>
	</tr>
	<tr><td  class="tableBd" colspan="3">
		<B>Classification of attorneys and their authority</B>
	</td></tr>
	<tr><td  class="tableBdAltIta" colspan="3">
		For the purposes of these regulations, notwithstanding
		his authority under the Articles of Association, a member
		of the Executive Board shall be the equivalent of a
		category S attorney.
	</td></tr>
<tr>
<td  class="tableBdAlt" colspan="3"><br><A NAME="type_s"></A>
<B>Category S attorney:</B> officer having general power to
represent the company;
</td>
</tr>
<tr>
<td width="10" class="tableBdalt" valign="top">-</td>
<td colspan="2" class="tableBdalt" valign="top">
	same powers as a category A attorney;
</td>
</tr>
<tr>
<td width="10" class="tableBdalt" valign="top">-</td>
<td colspan="2" class="tableBdalt" valign="top">
	also empowered to act jointly with another category
	S attorney in representing the company in relation to
	third parties in all matters in which the company may be
	validly represented by its executive directors, expressly
	including all acts of disposition, even where there is a
	conflict of interest between the authorised
	representative and the company, other than a conflict
	involving a private interest;
</td>
</tr>
<tr>
<td width="10" class="tableBdalt" valign="top">-</td>
<td colspan="2" class="tableBdalt" valign="top">
also em',sysdate(),1,sysdate(),1);
Insert into VAR_HTMLTEXT (VHT_PAGENAME,VHT_NUMBER,VHT_HTMLTEXT,VHT_CREATEDON,VHT_CREATEDBYID,VHT_CHANGEDON,VHT_CHANGEDBYID) values ('proxy_scheme_int.jsp',6,'powered:
</td></tr>
<tr>
<td width="10" class="tableBdalt" valign="top">&nbsp;</td>
<td width="30" class="tableBdalt" valign="top">(i)</td>
<td width="560" class="tableBdalt">jointly with a category S attorney, to delegate his
       own power of attorney to a third party, subject to the
       restriction in respect of the specific transaction to be
       concluded pursuant to the authorisation or delegated-
       authorisation, that the company should always be
       represented by two persons having the power of attorney;
</td></tr>
<tr>
<td width="10" class="tableBdalt" valign="top">&nbsp;</td>
<td width="30" class="tableBdalt" valign="top">(ii)</td>
<td width="560" class="tableBdalt">jointly with a category S attorney, to represent the
       company in cases in which it acts as executive director
       or authorised representative of other companies;
</td></tr>
<tr>
<td width="10" class="tableBdalt" valign="top">&nbsp;</td>
<td width="30" class="tableBdalt" valign="top">(iii)</td>
<td width="560" class="tableBdalt">jointly with a category S, A or B attorney, to
       delegate his own authorisation together with that
       attorney to a third party, to be vested with independent
       power of attorney of the company in respect of a specific
       transaction, provided that the substitution relates to a
       concrete transaction which is specifically and accurately
       described in writing in the authorisation;
</td></tr>
<tr>
<td width="10" class="tableBdalt" valign="top">&nbsp;</td>
<td width="30" class="tableBdalt" valign="top">(iv)</td>
<td width="560" class="tableBdalt">jointly with a category C1 or C2 power of attorney,
       to delegate as applicable, his own authorisation to a
       third party in the manner',sysdate(),1,sysdate(),1);
Insert into VAR_HTMLTEXT (VHT_PAGENAME,VHT_NUMBER,VHT_HTMLTEXT,VHT_CREATEDON,VHT_CREATEDBYID,VHT_CHANGEDON,VHT_CHANGEDBYID) values ('proxy_scheme_int.jsp',4,'
	</td></tr>
	<tr><td width="10" class="tableBdalt" valign="top">-&nbsp;
	</td><td width="590" class="tableBdalt">agreements for securities services and stockbroking;
	</td></tr>
	<tr><td width="10" class="tableBdalt" valign="top">-&nbsp;
	</td><td width="590" class="tableBdalt">insurance agreements;
	</td></tr>
	<tr><td width="10" class="tableBdalt" valign="top">-&nbsp;
	</td><td width="590" class="tableBdalt">contracts of assignment and contracts for the
	  performance of work.
	</td></tr>
<tr><td colspan="2"  class="tableBd">
<br>Block  3:</td></tr>
	<tr><td width="10" class="tableBdalt" valign="top">-&nbsp;
	</td><td width="590" class="tableBdalt">creation of encumbrances on registered property
	  (e.g. real property, registered ships, aircraft and co-
	  operative ownership of apartment buildings) of clients or
	  third parties for example, mortgages on such property;
	</td></tr>
	<tr><td width="10" class="tableBdalt" valign="top">-&nbsp;
	</td><td width="590" class="tableBdalt">the creation of encumbrances on movable property
	  (e.g. pledge of stock-in-trade) and proprietary rights
	  (e.g. pledge of accounts receivable) of clients or third
	  parties;
	</td></tr>

	<tr><td width="10" class="tableBdalt" valign="top">-&nbsp;
	</td><td width="590" class="tableBdalt">agreements in connection with credit funds, consumer
	  loans, deposits, call loans, money reserve accounts, cash
	  management, current and savings accounts, payment cards,
	  credit cards and agreements for electronic banking,
	  insofar as the monetary amount involved in the contract
	  does not exceed EUR 25,000.
	</td></tr>

</table>
</td></tr>
</table>

<table width="620" border="0" cellspacing="0" cellpadding="0">
<tr>  <td width="20" class="tableBdalt',sysdate(),1,sysdate(),1);
Insert into VAR_HTMLTEXT (VHT_PAGENAME,VHT_NUMBER,VHT_HTMLTEXT,VHT_CREATEDON,VHT_CREATEDBYID,VHT_CHANGEDON,VHT_CHANGEDBYID) values ('proxy_scheme_int.jsp',2,'ta" colspan="3">
provided that ability to delegate power of attorney is
only exercised with due observance of the restrictions
listed below.
</td></tr>

<tr><td colspan="2"  class="tableBd">
<br>Block  2:</td></tr>
	<tr><td width="10" class="tableBdalt" valign="top">-&nbsp;
	</td><td width="590" class="tableBdalt">entering into credit agreements and granting loans,
	  such as overdrafts, advance current account financing of
	  securities, medium-term loans, contingent liability
	  facilities, leasing, funding of commercial transactions;
	</td></tr>
	<tr><td width="10" class="tableBdalt" valign="top">-&nbsp;
	</td><td width="590" class="tableBdalt">agreements and legal transactions connected with
	  seizure and execution against registered property (e.g.
	  real property, registered ships, aircraft and co-
	  operative ownership of apartment buildings) of clients or
	  third parties, including the collection of amounts
	  receivable, recovering collateral, foreclosure and
	  bankruptcy proceedings or the waiver of claims and/or
	  mortgage rights, and including the granting of authority
	  to cancel mortgage entries in public registers on
	  registered property of clients or third parties;
	</td></tr>
	<tr><td width="10" class="tableBdalt" valign="top">-&nbsp;
	</td><td width="590" class="tableBdalt">agreements and legal transactions connected with
	  seizure and execution against movable property (e.g.
	  pledge of stock-in-trade) and proprietary rights (e.g.
	  pledge of accounts receivable) of clients or third
	  parties, including the collection of amounts receivable,
	  foreclosure and bankruptcy proceedings and waiver of
	  claims and/or pledges;
	</td></tr>
	<tr><td width="10" class="tableBdalt" valign="top">-&nbsp;
	</td><td wi',sysdate(),1,sysdate(),1);
Insert into VAR_HTMLTEXT (VHT_PAGENAME,VHT_NUMBER,VHT_HTMLTEXT,VHT_CREATEDON,VHT_CREATEDBYID,VHT_CHANGEDON,VHT_CHANGEDBYID) values ('proxy_scheme_int.jsp',3,'dth="590" class="tableBdalt">agreements of record (in particular, agreements
	  settling a dispute out of court);
	</td></tr>
	<tr><td width="10" class="tableBdalt" valign="top">-&nbsp;
	</td><td width="590" class="tableBdalt">the issuing of guarantees and suretyships and the
	  discharge of clients or third parties from their
	  obligations under guarantees or suretyships;
	</td></tr>
	<tr><td width="10" class="tableBdalt" valign="top">-&nbsp;
	</td><td width="590" class="tableBdalt">the execution of payment orders to domestic or
	  foreign banks;
	</td></tr>
	<tr><td colspan="2" class="tableBdalt" valign="top"><BR>_____________________________</td></tr>
	<tr><td width="10" class="tableBdalt" valign="top">(1)
	</td><td width="590" class="tableBdalt">Reference in these regulations to "agreements" or "transactions" shall include entering into, amending or terminating such agreements as appropriate and the issuing of unilateral declarations if these may give rise to obligations for the company (e.g. binding offer for the provision of credit).</td></tr>


</table>
</td></tr>
</table>
<PAGE_MARKER_SIGNATUREBOOK>
<PAGE_MARKER_SIGNATUREBOOK>
<div class="prspagebreakafter">&nbsp;</div>
<table width="620" border="0" cellspacing="0" cellpadding="0">
<tr>  <td width="20" class="tableBdalt" valign="top">&nbsp;</td>
<td valign="top" width="600" class="tableBdalt">
<table width="600"  border="0" cellspacing="0" cellpadding="0">
	<tr><td width="10" class="tableBdalt" valign="top">-&nbsp;
	</td><td width="590" class="tableBdalt">the acceptance, endorsement and guaranteeing of
	  bills of exchange and instruments payable to bearer or
	  order, the drawing and endorsement of cheques or the
	  issuing, endorsement and guaranteeing of promissory
	  notes;',sysdate(),1,sysdate(),1);
Insert into VAR_HTMLTEXT (VHT_PAGENAME,VHT_NUMBER,VHT_HTMLTEXT,VHT_CREATEDON,VHT_CREATEDBYID,VHT_CHANGEDON,VHT_CHANGEDBYID) values ('proxy_scheme_int.jsp',1,'cipating interest in another enterprise, or
	  expanding, reducing or disposing of the participating
	  interest concerned;
	</td></tr>
	<tr><td width="10" class="tableBdalt" valign="top">-&nbsp;
	</td><td width="590" class="tableBdalt">
	the acquisition or disposal of the company''s
	  registered property (e.g. real property) or the
	  establishment of encumbrances on such property;
	</td></tr>
	<tr><td width="10" class="tableBdalt" valign="top">-&nbsp;
	</td><td width="590" class="tableBdalt">
	participation in securities issues and the provision
	  of related services;
	</td></tr>
	<tr><td width="10" class="tableBdalt" valign="top">-&nbsp;
	</td><td width="590" class="tableBdalt">
	the issuance or confirmation of documentary credits;
	</td></tr>
	<tr><td width="10" class="tableBdalt" valign="top">-&nbsp;
	</td><td width="590" class="tableBdalt">
	the acceptance and termination on behalf of the
	  company of the position of asset manager, administrator,
	  executor, managing director or liquidator of an
	  enterprise, legal entity or person;
	</td></tr>
	<tr><td width="10" class="tableBdalt" valign="top">-&nbsp;
	</td><td width="590" class="tableBdalt">
	the confirmation on behalf of the company of
	  transactions relating to currencies, securities and
	  similar instruments and money and capital market
	  instruments;
	</td></tr>
	<tr><td width="10" class="tableBdalt" valign="top">-&nbsp;
	</td><td width="590" class="tableBdalt">
	entering the company in the Commercial Register;
	</td></tr>
	<tr><td width="10" class="tableBdalt" valign="top">-&nbsp;
	</td><td width="590" class="tableBdalt">
	engaging in complaint and/or arbitration proceedings
	  and litigation or deciding on litigation;
	</td></tr>
<tr><td  class="tableBdAltI',sysdate(),1,sysdate(),1);
Insert into VAR_HTMLTEXT (VHT_PAGENAME,VHT_NUMBER,VHT_HTMLTEXT,VHT_CREATEDON,VHT_CREATEDBYID,VHT_CHANGEDON,VHT_CHANGEDBYID) values ('proxy_scheme_int.jsp',0,'<PAGE_MARKER_SIGNATUREBOOK>
<!-- outer table 620 pixels is recommended for printing -->
<!-- furthermore we cannot have pagebreaks in a table so repeat the outer table at every pagebreak  -->
<!-- Let wel dat voor het signaturebook een speciale pagina marker opgenomen moet worden voor en na iedere pagina 1 -->
<!-- Dus ingeval van 3 paginas zijn er zes markers -->
<!-- Let wel. In het aanmaken van het signaturebook wordt prspagebreak onderdrukt -->

<table width="620" border="0" cellspacing="0" cellpadding="0">
<tr>  <td width="20" class="tableBdalt" valign="top">&nbsp;</td>
<td valign="top" width="600" class="tableBdalt">

<!-- start content in one cel of the outer table -->
<!-- mind that the trick is to have generally a table with 2 or 3 columns summing up to 600 pixels -->
<!-- in the first row the width should always be explicit -->
<table width="600" border="0" cellspacing="0" cellpadding="0">
<tr>
  <td width="10" class="tableBdalt" valign="top">&nbsp;</td>
  <td valign="top" width="590" class="tableBdalt"></td>
</tr>

<tr><td  colspan="2"  align="middle"><div class="header">
REGULATIONS FOR THE POWER TO REPRESENT ING BANK N.V. IN THE NETHERLANDS
<br><img src="images/hr_lightblue.gif" width="600" height="20" border="0" alt="" />
</div>
</td></tr>

<tr><td colspan="2"  class="tableBd">
<br>List of the company''s principal activities (''written
legal transactions'')</td></tr>

<tr><td colspan="2"  class="tableBd">
<br>Block  1:</td></tr>
	<tr><td  class="tableBdAltIta" colspan="3">
	all legal transactions of the company, including all acts
	of disposal; in particular:
	</td></tr>
	<tr><td width="10" class="tableBdalt" valign="top">-&nbsp;</td><td width="590" class="tableBdalt">
	all agreements(1) for the acquisition of a
	  parti',sysdate(),1,sysdate(),1);
Insert into VAR_HTMLTEXT (VHT_PAGENAME,VHT_NUMBER,VHT_HTMLTEXT,VHT_CREATEDON,VHT_CREATEDBYID,VHT_CHANGEDON,VHT_CHANGEDBYID) values ('proxy_scheme_nn.jsp',10,'he legal transactions referred
  		to in block 1, provided that he acts jointly with a
  		category S or A attorney;
	</td></tr>
	<tr><td width="10" class="tableBdalt" valign="top">-</td>
	<td colspan="2" class="tableBdalt" valign="top">
		empowered to perform the legal transactions referred
  		to in block 3, provided the amount involved does not
  		exceed EUR 1,000,000;
	</td></tr>
	<tr><td  class="tableBdAltIta" colspan="3">
		including, provided that he acts jointly with a category
		S or A attorney, the power to delegate the power of
		attorney to a third party, provided that the substitution
		occurs only in respect of a concrete transaction which is
		specifically and accurately described in writing in the
		authorisation concerning the substitution, to which third
		party independent authority may be granted for the
		company with respect to the aforementioned transaction.
	</td></tr>

	<tr><td colspan="2" class="tableBdalt" valign="top"><BR>_____________________________</td></tr>
	<tr><td width="10" class="tableBdalt" valign="top">(2)
	</td><td width="590" class="tableBdalt">An amount  specified in euros in these regulations shall  include the corresponding amount in foreign currency, freely convertible into euros at the exchange rate on the date of the relevant offer/transaction.</td></tr>


</table>

<!-- Pagebreak so close and reopen outer table  -->
</td></tr></table>
<PAGE_MARKER_SIGNATUREBOOK>
<PAGE_MARKER_SIGNATUREBOOK>
<div class="prspagebreakafter">&nbsp;</div>

<table width="620" border="0" cellspacing="0" cellpadding="0">
<tr>  <td width="20" class="tableBdalt" valign="top">&nbsp;</td> <td width="600" class="tableBdalt">
<!-- end classification and start of the schema -->
<table width="600" border="0" cellspacin',sysdate(),1,sysdate(),1);
Insert into VAR_HTMLTEXT (VHT_PAGENAME,VHT_NUMBER,VHT_HTMLTEXT,VHT_CREATEDON,VHT_CREATEDBYID,VHT_CHANGEDON,VHT_CHANGEDBYID) values ('proxy_scheme_nn.jsp',29,'
			may sign alone for work in block 3, up to EUR 1 million.
		</td>
	</tr>
	<tr>
		<td colspan="3"  class="tableBd">
			<br><i>Category E and F power of attorney</i>
		</td>
	</tr>
	<tr>
		<td colspan="3"  class="tableBdAltIta">
			Category E and F powers of attorney of ING Bank N.V.,
			granted to ING Bank N.V. branch employees abroad, are unchanged.
			These powers of attorney have not been filed at the trade register
			of a Dutch Chamber of Commerce.
		</td>
	</tr>
	<tr>
		<td colspan="3"  class="tableBdAltIta">
			<br>Category B or D attorneys may not commit ING jointly with
			an attorney of the same level. In every case, therefore,
			they have to act jointly with an attorney with a ''higher''
			level of authorisation. A category D attorney may not,
			however, sign with a category B attorney.
		</td>
	</tr>	<tr>
		<td colspan="3"  class="tableBd">
			<br><b>Substitution</b>
		</td>
	</tr>
	<tr>
		<td colspan="3"  class="tableBdAltIta">
			The right of substitution is the right to delegate
			authority to another person. The power of substitution
			should be exercised with restraint.
		</td>
	</tr>
	<tr>
		<td colspan="3"  class="tableBdAltIta">
			<br>Generally speaking, a category <b>S attorney</b> is the
			equivalent of a member of the Executive Board for the
			purposes of these regulations. A category S attorney may,
			therefore, delegate his authority to any third party
			provided that he does so together with another category S
			attorney. Such a third party may, however, only commit
			the bank if he acts with another person competent to act
			(dual signatory system) in which case the third party has
			independent freedom of decision. A category <b>S</b>,
			<b>A</b> or <b>B attorney</b>, along wi',sysdate(),1,sysdate(),1);
Insert into VAR_HTMLTEXT (VHT_PAGENAME,VHT_NUMBER,VHT_HTMLTEXT,VHT_CREATEDON,VHT_CREATEDBYID,VHT_CHANGEDON,VHT_CHANGEDBYID) values ('proxy_scheme_nn.jsp',27,'	<br><i>Category B power of attorney</i>
		</td>
	</tr>
	<tr>
		<td colspan="3"  class="tableBdAltIta">
			For <b>external</b> purposes, a category B attorney may sign,
			together with an attorney in category:
		</td>
	</tr>
	<tr>
		<td width="10" class="tableBdalt" valign="top">-&nbsp;</td>
		<td colspan="2" class="tableBdalt">
			S or A, for work in blocks 1, 2 or 3, with no <b>external</b> maximum amount.
		</td>
	</tr>
	<tr>
		<td colspan="3"  class="tableBdAltIta">
			In addition, for <b>external</b> purposes, a category B attorney
			may sign alone for work in block 3, up to EUR 1 million.
		</td>
	</tr>
</table>
		<!-- end content in one cel of the outer table -->
		</td>
	</tr>
</table>
<PAGE_MARKER_SIGNATUREBOOK>
<PAGE_MARKER_SIGNATUREBOOK>
<div class="prspagebreakafter">&nbsp;</div>

<table width="620" border="0" cellspacing="0" cellpadding="0">
<tr>  <td width="20" class="tableBdalt" valign="top">&nbsp;</td> <td width="600" class="tableBdalt">
<!-- end classification and start of the schema -->
<table width="600" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td colspan="3"  class="tableBd">
			<br><i>Category C1 power of attorney</i>
		</td>
	</tr>
	<tr>
		<td colspan="3"  class="tableBdAltIta">
			For <b>external</b> purposes, a category C1 attorney may sign,
			together with an attorney in category:
		</td>
	</tr>
	<tr>
		<td width="10" class="tableBdalt" valign="top">-&nbsp;</td>
		<td colspan="2" class="tableBdalt">
			S, A or C1, for work in blocks 2 or 3, with no
			<b>external</b> maximum amount;
		</td>
	</tr>
	<tr>
		<td width="10" class="tableBdalt" valign="top">-&nbsp;</td>
		<td colspan="2" class="tableBdalt">
			C2, for work in blocks 2 or 3, up to EUR 5 million;
		</td>
	</tr>
	<t',sysdate(),1,sysdate(),1);
Insert into VAR_HTMLTEXT (VHT_PAGENAME,VHT_NUMBER,VHT_HTMLTEXT,VHT_CREATEDON,VHT_CREATEDBYID,VHT_CHANGEDON,VHT_CHANGEDBYID) values ('proxy_scheme_nn.jsp',25,'ts;
		</td>
	</tr>
	<tr>
		<td width="10" class="tableBdalt" valign="top">-&nbsp;</td>
		<td colspan="2" class="tableBdalt">
			granting mortgages and accepting pledges;
		</td>
	</tr>
	<tr>
		<td width="10" class="tableBdalt" valign="top">-&nbsp;</td>
		<td colspan="2" class="tableBdalt">
			securities services;
		</td>
	</tr>
	<tr>
		<td width="10" class="tableBdalt" valign="top">-&nbsp;</td>
		<td colspan="2" class="tableBdalt">
			executing payment orders and other legal transactions in the context of funds transfers.
		</td>
	</tr>
	<tr>
		<td colspan="3"  class="tableBdAltIta">
			<br>Block 3 is also work customarily performed by the branch
			organisation, mainly at branches with a limited number
			staff.
		</td>
	</tr>

	<tr>
		<td colspan="3"  class="tableBd">
			<br><b><i>Types of power of attorney</i></b>
		</td>
	</tr>
	<tr>
		<td colspan="3"  class="tableBdAltIta">
			External power of attorney is designated by the letters
			S, A, B, C1, C2, D, E and F.
		</td>
	</tr>
	<tr>
		<td colspan="3"  class="tableBd">
			<br><i>Category S power of attorney</i>
		</td>
	</tr>
	<tr>
		<td colspan="3"  class="tableBdAltIta">
			For <b>external</b> purposes, a category S attorney may sign,
			together with an attorney in category:
		</td>
	</tr>
	<tr>
		<td width="10" class="tableBdalt" valign="top">-&nbsp;</td>
		<td colspan="2" class="tableBdalt">
			S, A or B, for work in blocks 1, 2 or 3, with no
  			<b>external</b> maximum amount;
		</td>
	</tr>
	<tr>
		<td width="10" class="tableBdalt" valign="top">-&nbsp;</td>
		<td colspan="2" class="tableBdalt">
			C1, for work in blocks 2 or 3, with no <b>external</b>
  			maximum amount;
		</td>
	</tr>
	<tr>
		<td width="10" class="tableBdalt" valign="top">-',sysdate(),1,sysdate(),1);
Insert into VAR_HTMLTEXT (VHT_PAGENAME,VHT_NUMBER,VHT_HTMLTEXT,VHT_CREATEDON,VHT_CREATEDBYID,VHT_CHANGEDON,VHT_CHANGEDBYID) values ('proxy_scheme_nn.jsp',7,' described above in (iii) above,
       subject to the limitation that the authorisation may be
       delegated only to a third party in order to enable him to
       appear on behalf of the company in order to execute an
       instrument before a civil-law notary in respect of the
       above-mentioned concrete transaction.
</td></tr>
</table>
</td></tr>
</table>
<table width="620" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td width="20" class="tableBdalt" valign="top">&nbsp;</td>
		<td valign="top" width="600" class="tableBdalt">
		<table width="600"  border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td  class="tableBdAlt" colspan="3"><br><A NAME="type_a"></A>
					<B>Category A attorney:</B> officer having general power to
					represent the company:
					</td>
				</tr>
				<tr>
					<td width="10" class="tableBdalt" valign="top">-</td>
					<td colspan="2" class="tableBdalt" valign="top">
						empowered to perform the legal transactions referred
						to in block 1, provided that he acts jointly with a
						category S, A or B attorney;
					</td>
				</tr>
				<tr>
					<td width="10" class="tableBdalt" valign="top">-</td>
					<td colspan="2" class="tableBdalt" valign="top">
						empowered to perform the legal transactions referred
					  to in block 2, provided that he acts:
					</td>
				</tr>
		</table>
		</td>
	</tr>
</table>

<PAGE_MARKER_SIGNATUREBOOK>
<PAGE_MARKER_SIGNATUREBOOK>
<div class="prspagebreakafter">&nbsp;</div>

<table width="620" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td width="20" class="tableBdalt" valign="top">&nbsp;</td>
		<td valign="top" width="600" class="tableBdalt">
		<table width="600"  border="0" cellspacing="0" cellpadding="0">
				<tr>
					<t',sysdate(),1,sysdate(),1);
Insert into VAR_HTMLTEXT (VHT_PAGENAME,VHT_NUMBER,VHT_HTMLTEXT,VHT_CREATEDON,VHT_CREATEDBYID,VHT_CHANGEDON,VHT_CHANGEDBYID) values ('proxy_scheme_nn.jsp',8,'d width="10" class="tableBdalt" valign="top">-</td>
					<td colspan="2" class="tableBdalt" valign="top">
						jointly with a category C1 attorney;
					</td>
				</tr>
				<tr>
					<td width="10" class="tableBdalt" valign="top">-</td>
					<td colspan="2" class="tableBdalt" valign="top">
						jointly with a category C2 attorney, provided the
					  amount involved does not exceed EUR 5,000,000,(2) or
					</td>
				</tr>
				<tr>
					<td width="10" class="tableBdalt" valign="top">-</td>
					<td colspan="2" class="tableBdalt" valign="top">
						jointly with a category D attorney, provided the
					  amount involved does not exceed EUR 1,000,000;
					</td>
				</tr>
				<tr>
					<td width="10" class="tableBdalt" valign="top">-</td>
					<td colspan="2" class="tableBdalt" valign="top">
						empowered to perform the legal transactions referred
					  to in block 3, provided the amount involved does not
					  exceed EUR 1,000,000;
					</td>
				</tr>
		</table>
		</td>
	</tr>
</table>

<table width="620" border="0" cellspacing="0" cellpadding="0">
<tr>  <td width="20" class="tableBdalt" valign="top">&nbsp;</td>
<td valign="top" width="600" class="tableBdalt">

<table width="600"  border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td  class="tableBdAltIta" colspan="3">
					including the power to delegate the power of attorney he
					has jointly with the other attorney to a third party,
					</td>
				</tr>
				<tr>
					<td width="10" class="tableBdalt" valign="top">&nbsp;</td>
					<td width="30" class="tableBdalt" valign="top">(i)</td>
					<td width="560" class="tableBdalt">
						provided that he acts jointly with a category S, A
					    or B attorney and provided that the substitution occ',sysdate(),1,sysdate(),1);
Insert into VAR_HTMLTEXT (VHT_PAGENAME,VHT_NUMBER,VHT_HTMLTEXT,VHT_CREATEDON,VHT_CREATEDBYID,VHT_CHANGEDON,VHT_CHANGEDBYID) values ('proxy_scheme_nn.jsp',6,'powered:
</td></tr>
<tr>
<td width="10" class="tableBdalt" valign="top">&nbsp;</td>
<td width="30" class="tableBdalt" valign="top">(i)</td>
<td width="560" class="tableBdalt">jointly with a category S attorney, to delegate his
       own power of attorney to a third party, subject to the
       restriction in respect of the specific transaction to be
       concluded pursuant to the authorisation or delegated-
       authorisation, that the company should always be
       represented by two persons having the power of attorney;
</td></tr>
<tr>
<td width="10" class="tableBdalt" valign="top">&nbsp;</td>
<td width="30" class="tableBdalt" valign="top">(ii)</td>
<td width="560" class="tableBdalt">jointly with a category S attorney, to represent the
       company in cases in which it acts as executive director
       or authorised representative of other companies;
</td></tr>
<tr>
<td width="10" class="tableBdalt" valign="top">&nbsp;</td>
<td width="30" class="tableBdalt" valign="top">(iii)</td>
<td width="560" class="tableBdalt">jointly with a category S, A or B attorney, to
       delegate his own authorisation together with that
       attorney to a third party, to be vested with independent
       power of attorney of the company in respect of a specific
       transaction, provided that the substitution relates to a
       concrete transaction which is specifically and accurately
       described in writing in the authorisation;
</td></tr>
<tr>
<td width="10" class="tableBdalt" valign="top">&nbsp;</td>
<td width="30" class="tableBdalt" valign="top">(iv)</td>
<td width="560" class="tableBdalt">jointly with a category C1 or C2 power of attorney,
       to delegate as applicable, his own authorisation to a
       third party in the manner',sysdate(),1,sysdate(),1);
Insert into VAR_HTMLTEXT (VHT_PAGENAME,VHT_NUMBER,VHT_HTMLTEXT,VHT_CREATEDON,VHT_CREATEDBYID,VHT_CHANGEDON,VHT_CHANGEDBYID) values ('proxy_scheme_nn.jsp',4,'
	</td></tr>
	<tr><td width="10" class="tableBdalt" valign="top">-&nbsp;
	</td><td width="590" class="tableBdalt">agreements for securities services and stockbroking;
	</td></tr>
	<tr><td width="10" class="tableBdalt" valign="top">-&nbsp;
	</td><td width="590" class="tableBdalt">insurance agreements;
	</td></tr>
	<tr><td width="10" class="tableBdalt" valign="top">-&nbsp;
	</td><td width="590" class="tableBdalt">contracts of assignment and contracts for the
	  performance of work.
	</td></tr>
<tr><td colspan="2"  class="tableBd">
<br>Block  3:</td></tr>
	<tr><td width="10" class="tableBdalt" valign="top">-&nbsp;
	</td><td width="590" class="tableBdalt">creation of encumbrances on registered property
	  (e.g. real property, registered ships, aircraft and co-
	  operative ownership of apartment buildings) of clients or
	  third parties for example, mortgages on such property;
	</td></tr>
	<tr><td width="10" class="tableBdalt" valign="top">-&nbsp;
	</td><td width="590" class="tableBdalt">the creation of encumbrances on movable property
	  (e.g. pledge of stock-in-trade) and proprietary rights
	  (e.g. pledge of accounts receivable) of clients or third
	  parties;
	</td></tr>

	<tr><td width="10" class="tableBdalt" valign="top">-&nbsp;
	</td><td width="590" class="tableBdalt">agreements in connection with credit funds, consumer
	  loans, deposits, call loans, money reserve accounts, cash
	  management, current and savings accounts, payment cards,
	  credit cards and agreements for electronic banking,
	  insofar as the monetary amount involved in the contract
	  does not exceed EUR 25,000.
	</td></tr>

</table>
</td></tr>
</table>

<table width="620" border="0" cellspacing="0" cellpadding="0">
<tr>  <td width="20" class="tableBdalt',sysdate(),1,sysdate(),1);
Insert into VAR_HTMLTEXT (VHT_PAGENAME,VHT_NUMBER,VHT_HTMLTEXT,VHT_CREATEDON,VHT_CREATEDBYID,VHT_CHANGEDON,VHT_CHANGEDBYID) values ('proxy_scheme_nn.jsp',5,'" valign="top">&nbsp;</td>
<td valign="top" width="600" class="tableBdalt">

<!-- table for the classification (needs 3 columns) -->
<table width="600"  border="0" cellspacing="0" cellpadding="0">
	<tr>
	<td width="10" class="tableBdalt" valign="top">&nbsp;</td>
	<td width="30" class="tableBdalt" valign="top">&nbsp;</td>
	<td width="560" class="tableBdalt">&nbsp;</td>
	<A NAME="leg_ING"></a><A NAME="leg_PB"></A>
	</tr>
	<tr><td  class="tableBd" colspan="3">
		<B>Classification of attorneys and their authority</B>
	</td></tr>
	<tr><td  class="tableBdAltIta" colspan="3">
		For the purposes of these regulations, notwithstanding
		his authority under the Articles of Association, a member
		of the Executive Board shall be the equivalent of a
		category S attorney.
	</td></tr>
<tr>
<td  class="tableBdAlt" colspan="3"><br><A NAME="type_s"></A>
<B>Category S attorney:</B> officer having general power to
represent the company;
</td>
</tr>
<tr>
<td width="10" class="tableBdalt" valign="top">-</td>
<td colspan="2" class="tableBdalt" valign="top">
	same powers as a category A attorney;
</td>
</tr>
<tr>
<td width="10" class="tableBdalt" valign="top">-</td>
<td colspan="2" class="tableBdalt" valign="top">
	also empowered to act jointly with another category
	S attorney in representing the company in relation to
	third parties in all matters in which the company may be
	validly represented by its executive directors, expressly
	including all acts of disposition, even where there is a
	conflict of interest between the authorised
	representative and the company, other than a conflict
	involving a private interest;
</td>
</tr>
<tr>
<td width="10" class="tableBdalt" valign="top">-</td>
<td colspan="2" class="tableBdalt" valign="top">
also em',sysdate(),1,sysdate(),1);
Insert into VAR_HTMLTEXT (VHT_PAGENAME,VHT_NUMBER,VHT_HTMLTEXT,VHT_CREATEDON,VHT_CREATEDBYID,VHT_CHANGEDON,VHT_CHANGEDBYID) values ('proxy_scheme_nn.jsp',3,'dth="590" class="tableBdalt">agreements of record (in particular, agreements
	  settling a dispute out of court);
	</td></tr>
	<tr><td width="10" class="tableBdalt" valign="top">-&nbsp;
	</td><td width="590" class="tableBdalt">the issuing of guarantees and suretyships and the
	  discharge of clients or third parties from their
	  obligations under guarantees or suretyships;
	</td></tr>
	<tr><td width="10" class="tableBdalt" valign="top">-&nbsp;
	</td><td width="590" class="tableBdalt">the execution of payment orders to domestic or
	  foreign banks;
	</td></tr>
	<tr><td colspan="2" class="tableBdalt" valign="top"><BR>_____________________________</td></tr>
	<tr><td width="10" class="tableBdalt" valign="top">(1)
	</td><td width="590" class="tableBdalt">Reference in these regulations to "agreements" or "transactions" shall include entering into, amending or terminating such agreements as appropriate and the issuing of unilateral declarations if these may give rise to obligations for the company (e.g. binding offer for the provision of credit).</td></tr>


</table>
</td></tr>
</table>
<PAGE_MARKER_SIGNATUREBOOK>
<PAGE_MARKER_SIGNATUREBOOK>
<div class="prspagebreakafter">&nbsp;</div>
<table width="620" border="0" cellspacing="0" cellpadding="0">
<tr>  <td width="20" class="tableBdalt" valign="top">&nbsp;</td>
<td valign="top" width="600" class="tableBdalt">
<table width="600"  border="0" cellspacing="0" cellpadding="0">
	<tr><td width="10" class="tableBdalt" valign="top">-&nbsp;
	</td><td width="590" class="tableBdalt">the acceptance, endorsement and guaranteeing of
	  bills of exchange and instruments payable to bearer or
	  order, the drawing and endorsement of cheques or the
	  issuing, endorsement and guaranteeing of promissory
	  notes;',sysdate(),1,sysdate(),1);
Insert into VAR_HTMLTEXT (VHT_PAGENAME,VHT_NUMBER,VHT_HTMLTEXT,VHT_CREATEDON,VHT_CREATEDBYID,VHT_CHANGEDON,VHT_CHANGEDBYID) values ('proxy_scheme_nn.jsp',1,'cipating interest in another enterprise, or
	  expanding, reducing or disposing of the participating
	  interest concerned;
	</td></tr>
	<tr><td width="10" class="tableBdalt" valign="top">-&nbsp;
	</td><td width="590" class="tableBdalt">
	the acquisition or disposal of the company''s
	  registered property (e.g. real property) or the
	  establishment of encumbrances on such property;
	</td></tr>
	<tr><td width="10" class="tableBdalt" valign="top">-&nbsp;
	</td><td width="590" class="tableBdalt">
	participation in securities issues and the provision
	  of related services;
	</td></tr>
	<tr><td width="10" class="tableBdalt" valign="top">-&nbsp;
	</td><td width="590" class="tableBdalt">
	the issuance or confirmation of documentary credits;
	</td></tr>
	<tr><td width="10" class="tableBdalt" valign="top">-&nbsp;
	</td><td width="590" class="tableBdalt">
	the acceptance and termination on behalf of the
	  company of the position of asset manager, administrator,
	  executor, managing director or liquidator of an
	  enterprise, legal entity or person;
	</td></tr>
	<tr><td width="10" class="tableBdalt" valign="top">-&nbsp;
	</td><td width="590" class="tableBdalt">
	the confirmation on behalf of the company of
	  transactions relating to currencies, securities and
	  similar instruments and money and capital market
	  instruments;
	</td></tr>
	<tr><td width="10" class="tableBdalt" valign="top">-&nbsp;
	</td><td width="590" class="tableBdalt">
	entering the company in the Commercial Register;
	</td></tr>
	<tr><td width="10" class="tableBdalt" valign="top">-&nbsp;
	</td><td width="590" class="tableBdalt">
	engaging in complaint and/or arbitration proceedings
	  and litigation or deciding on litigation;
	</td></tr>
<tr><td  class="tableBdAltI',sysdate(),1,sysdate(),1);
Insert into VAR_HTMLTEXT (VHT_PAGENAME,VHT_NUMBER,VHT_HTMLTEXT,VHT_CREATEDON,VHT_CREATEDBYID,VHT_CHANGEDON,VHT_CHANGEDBYID) values ('proxy_scheme_nn.jsp',0,'<PAGE_MARKER_SIGNATUREBOOK>
<!-- outer table 620 pixels is recommended for printing -->
<!-- furthermore we cannot have pagebreaks in a table so repeat the outer table at every pagebreak  -->
<!-- Let wel dat voor het signaturebook een speciale pagina marker opgenomen moet worden voor en na iedere pagina 1 -->
<!-- Dus ingeval van 3 paginas zijn er zes markers -->
<!-- Let wel. In het aanmaken van het signaturebook wordt prspagebreak onderdrukt -->

<table width="620" border="0" cellspacing="0" cellpadding="0">
<tr>  <td width="20" class="tableBdalt" valign="top">&nbsp;</td>
<td valign="top" width="600" class="tableBdalt">

<!-- start content in one cel of the outer table -->
<!-- mind that the trick is to have generally a table with 2 or 3 columns summing up to 600 pixels -->
<!-- in the first row the width should always be explicit -->
<table width="600" border="0" cellspacing="0" cellpadding="0">
<tr>
  <td width="10" class="tableBdalt" valign="top">&nbsp;</td>
  <td valign="top" width="590" class="tableBdalt"></td>
</tr>

<tr><td  colspan="2"  align="middle"><div class="header">
REGULATIONS FOR THE POWER TO REPRESENT ING BANK N.V. IN THE NETHERLANDS
<br><img src="images/hr_lightblue.gif" width="600" height="20" border="0" alt="" />
</div>
</td></tr>

<tr><td colspan="2"  class="tableBd">
<br>List of the company''s principal activities (''written
legal transactions'')</td></tr>

<tr><td colspan="2"  class="tableBd">
<br>Block  1:</td></tr>
	<tr><td  class="tableBdAltIta" colspan="3">
	all legal transactions of the company, including all acts
	of disposal; in particular:
	</td></tr>
	<tr><td width="10" class="tableBdalt" valign="top">-&nbsp;</td><td width="590" class="tableBdalt">
	all agreements(1) for the acquisition of a
	  parti',sysdate(),1,sysdate(),1);
Insert into VAR_HTMLTEXT (VHT_PAGENAME,VHT_NUMBER,VHT_HTMLTEXT,VHT_CREATEDON,VHT_CREATEDBYID,VHT_CHANGEDON,VHT_CHANGEDBYID) values ('proxy_scheme_nn.jsp',2,'ta" colspan="3">
provided that ability to delegate power of attorney is
only exercised with due observance of the restrictions
listed below.
</td></tr>

<tr><td colspan="2"  class="tableBd">
<br>Block  2:</td></tr>
	<tr><td width="10" class="tableBdalt" valign="top">-&nbsp;
	</td><td width="590" class="tableBdalt">entering into credit agreements and granting loans,
	  such as overdrafts, advance current account financing of
	  securities, medium-term loans, contingent liability
	  facilities, leasing, funding of commercial transactions;
	</td></tr>
	<tr><td width="10" class="tableBdalt" valign="top">-&nbsp;
	</td><td width="590" class="tableBdalt">agreements and legal transactions connected with
	  seizure and execution against registered property (e.g.
	  real property, registered ships, aircraft and co-
	  operative ownership of apartment buildings) of clients or
	  third parties, including the collection of amounts
	  receivable, recovering collateral, foreclosure and
	  bankruptcy proceedings or the waiver of claims and/or
	  mortgage rights, and including the granting of authority
	  to cancel mortgage entries in public registers on
	  registered property of clients or third parties;
	</td></tr>
	<tr><td width="10" class="tableBdalt" valign="top">-&nbsp;
	</td><td width="590" class="tableBdalt">agreements and legal transactions connected with
	  seizure and execution against movable property (e.g.
	  pledge of stock-in-trade) and proprietary rights (e.g.
	  pledge of accounts receivable) of clients or third
	  parties, including the collection of amounts receivable,
	  foreclosure and bankruptcy proceedings and waiver of
	  claims and/or pledges;
	</td></tr>
	<tr><td width="10" class="tableBdalt" valign="top">-&nbsp;
	</td><td wi',sysdate(),1,sysdate(),1);
Insert into VAR_HTMLTEXT (VHT_PAGENAME,VHT_NUMBER,VHT_HTMLTEXT,VHT_CREATEDON,VHT_CREATEDBYID,VHT_CHANGEDON,VHT_CHANGEDBYID) values ('proxy_scheme_nn.jsp',28,'r>
		<td width="10" class="tableBdalt" valign="top">-&nbsp;</td>
		<td colspan="2" class="tableBdalt">
			D, for work in block 2, up to EUR 1 million.
		</td>
	</tr>
	<tr>
		<td colspan="3"  class="tableBdAltIta">
			In addition, for <b>external</b> purposes, a category C1 attorney
			may sign alone for work in block 3, up to EUR 1 million.
		</td>
	</tr>
	<tr>
		<td colspan="3"  class="tableBd">
			<br><i>Category C2 power of attorney</i>
		</td>
	</tr>
	<tr>
		<td colspan="3"  class="tableBdAltIta">
			For <b>external</b> purposes, a category C2 attorney may sign,
			together with an attorney in category:
		</td>
	</tr>
	<tr>
		<td width="10" class="tableBdalt" valign="top">-&nbsp;</td>
		<td colspan="2" class="tableBdalt">
			S, A, C1 or C2 for work in blocks 2 or 3, up to EUR 5 million;
		</td>
	</tr>
	<tr>
		<td width="10" class="tableBdalt" valign="top">-&nbsp;</td>
		<td colspan="2" class="tableBdalt">
			D for work in block 2, up to EUR 1 million.
		</td>
	</tr>
	<tr>
		<td colspan="3"  class="tableBdAltIta">
			In addition, for <b>external</b> purposes, a category C2 attorney
			may sign alone for work in block 3, up to EUR 1 million.
		</td>
	</tr>
	<tr>
		<td colspan="3"  class="tableBd">
			<br><i>Category D power of attorney</i>
		</td>
	</tr>
	<tr>
		<td colspan="3"  class="tableBdAltIta">
			For <b>external</b> purposes, a category D attorney may sign,
			together with an attorney in category:
		</td>
	</tr>
	<tr>
		<td width="10" class="tableBdalt" valign="top">-&nbsp;</td>
		<td colspan="2" class="tableBdalt">
			S, A, C1 or C2 for work in block 2, up to EUR 1 million.
		</td>
	</tr>
	<tr>
		<td colspan="3"  class="tableBdAltIta">
			In addition, for external purposes, a category D attorney',sysdate(),1,sysdate(),1);
Insert into VAR_HTMLTEXT (VHT_PAGENAME,VHT_NUMBER,VHT_HTMLTEXT,VHT_CREATEDON,VHT_CREATEDBYID,VHT_CHANGEDON,VHT_CHANGEDBYID) values ('proxy_scheme_int.jsp',17,'1 million</td><td class="tableBdalt">&nbsp;</td></tr>
					 </table>
				</td>
			</tr>
			<tr>
				<td bgcolor="#ffffff"  class="tableBdalt" valign="top">C2</td>
				<td bgcolor="#ffffff"  class="tableBdalt" valign="top">&nbsp;</td>
				<td bgcolor="#ffffff"  class="tableBdalt" valign="top">
					<table  border="0" cellspacing="0" cellpadding="0">
						<tr><td class="tableBdalt">C2 + S</td><td class="tableBdalt">&nbsp;max. EUR 5 million</td></tr>
						<tr><td class="tableBdalt">C2 + A</td><td class="tableBdalt">&nbsp;max. EUR 5 million</td></tr>
						<tr><td class="tableBdalt">C2 + C1</td><td class="tableBdalt">&nbsp;max. EUR 5 million</td></tr>
						<tr><td class="tableBdalt">C2 + C2</td><td class="tableBdalt">&nbsp;max. EUR 5 million</td></tr>
						<tr><td class="tableBdalt">C2 + D</td><td class="tableBdalt">&nbsp;max. EUR 1 million</td></tr>
					</table>
				</td>
				<td bgcolor="#ffffff"  class="tableBdalt" valign="top">
					 <table border="0" cellspacing="0" cellpadding="0">
						<tr><td class="tableBdalt" >C2 max EUR 1 million</td><td class="tableBdalt">&nbsp;</td></tr>
					 </table>
				</td>
			</tr>
			<tr>
				<td bgcolor="#ffffff"  class="tableBdalt" valign="top">D</td>
				<td bgcolor="#ffffff"  class="tableBdalt" valign="top">&nbsp;</td>
				<td bgcolor="#ffffff"  class="tableBdalt" valign="top">
					<table  border="0" cellspacing="0" cellpadding="0">
						<tr><td class="tableBdalt">D + S</td><td class="tableBdalt">&nbsp;max. EUR 1 million</td></tr>
						<tr><td class="tableBdalt">D + A</td><td class="tableBdalt">&nbsp;max. EUR 1 million</td></tr>
						<tr><td class="tableBdalt">D + C1</td><td class="tableBdalt">&nbsp;max. EUR 1 million</td></tr>
						<tr><td class="tableBdalt">D + C2</td><td class="tableBda',sysdate(),1,sysdate(),1);
Insert into VAR_HTMLTEXT (VHT_PAGENAME,VHT_NUMBER,VHT_HTMLTEXT,VHT_CREATEDON,VHT_CREATEDBYID,VHT_CHANGEDON,VHT_CHANGEDBYID) values ('proxy_scheme_int.jsp',18,'lt">&nbsp;max. EUR 1 million</td></tr>
					</table>
				</td>
				<td bgcolor="#ffffff"  class="tableBdalt" valign="top">
					 <table border="0" cellspacing="0" cellpadding="0">
						<tr><td class="tableBdalt" >D max EUR 1 million</td><td class="tableBdalt">&nbsp;</td></tr>
					 </table>
				</td>
			</tr>
		</table>
		<!-- end matrix -->
		<table width="600" border="0" cellspacing="0" cellpadding="0">
			<tr>
			  <td width="10" class="tableBdalt" valign="top"><B>NB:</B>&nbsp;</td>
			  <td width="590" class="tableBdalt">
				<B> For the purposes of these regulations,
				notwithstanding his authority under the Articles of
				Association, a member of the Executive Board of ING Bank
				N.V. shall be the equivalent of a category S attorney</B>
			  </td>
			</tr>
			<tr>
			  <td width="10" class="tableBdalt" valign="top"><br>&nbsp;</td>
			  <td width="590" class="tableBdalt">&nbsp;</td>
			</tr>
			<tr>
			  <td width="10" class="tableBdalt" valign="top"><br>&nbsp;</td>
			  <td width="590" class="tableBdalt">&nbsp;</td>
			</tr>
		</table>
		<!-- end content in one cel of the outer table -->
		</td>
	</tr>
</table>
<PAGE_MARKER_SIGNATUREBOOK>
<PAGE_MARKER_SIGNATUREBOOK>
<div class="prspagebreakafter">&nbsp;</div>

<table width="620" border="0" cellspacing="0" cellpadding="0">
<tr>  <td width="20" class="tableBdalt" valign="top">&nbsp;</td> <td width="600" class="tableBdalt">
<!-- end classification and start of the schema -->
<table width="600" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td colspan="3"  class="tableBd">
			<b>Notes on the external power-of-attorney regulations for Postbank N.V.</b>
		</td>
	</tr>
	<tr>
		<td colspan="3"  class="tableBd">
			<br><i>For ING employees</i>
		</td>
	</tr>
',sysdate(),1,sysdate(),1);
Insert into VAR_HTMLTEXT (VHT_PAGENAME,VHT_NUMBER,VHT_HTMLTEXT,VHT_CREATEDON,VHT_CREATEDBYID,VHT_CHANGEDON,VHT_CHANGEDBYID) values ('proxy_scheme_int.jsp',19,'
	<tr>
		<td colspan="3"  class="tableBd">
			<br><b>Introduction</b>
		</td>
	</tr>
	<tr>
		<td colspan="3"  class="tableBdAltIta">
			<br>This brochure is for all ING employees who have been
			granted external powers of attorney for ING Bank N.V. In
			their original form, the power-of-attorney regulations
			came into force on 5 September 2000 and this brochure has
			been prepared as a result of changes which took effect on
			1 March 2004. Since that date, ING Bank N.V. has had
			uniform power-of-attorney regulations, and this has
			brought a large number of practical benefits.
		</td>
	</tr>
	<tr>
		<td colspan="3"  class="tableBd">
			<br>This brochure briefly addresses the following subjects:
		</td>
	</tr>
	<tr>
		<td width="10" class="tableBdalt" valign="top">&nbsp;</td>
		<td colspan="2" class="tableBdalt">
			the concept of external versus internal powers of attorney;
		</td>
	</tr>
	<tr>
		<td width="10" class="tableBdalt" valign="top">&nbsp;</td>
		<td colspan="2" class="tableBdalt">
			registration at the Chamber of Commerce;
		</td>
	</tr>
	<tr>
		<td width="10" class="tableBdalt" valign="top">&nbsp;</td>
		<td colspan="2" class="tableBdalt">
			uniform power-of-attorney regulations at Postbank N.V. and ING Bank N.V.;
		</td>
	</tr>
	<tr>
		<td width="10" class="tableBdalt" valign="top">&nbsp;</td>
		<td colspan="2" class="tableBdalt">
			the external power-of-attorney regulations:
		</td>
	</tr>
	<tr>
		<td width="10" class="tableBdalt" valign="top">&nbsp;</td>
		<td width="30" class="tableBdalt" valign="top">-</td>
		<td width="560" class="tableBdalt">
			the external power-of-attorney regulations:
		</td>
	</tr>
	<tr>
		<td width="10" class="tableBdalt" valign="top"> &nbsp;</td>
',sysdate(),1,sysdate(),1);
Insert into VAR_HTMLTEXT (VHT_PAGENAME,VHT_NUMBER,VHT_HTMLTEXT,VHT_CREATEDON,VHT_CREATEDBYID,VHT_CHANGEDON,VHT_CHANGEDBYID) values ('proxy_scheme_int.jsp',20,'
		<td colspan="2" class="tableBdalt">
			the application, change and termination procedure.
		</td>
	</tr>
	<tr>
		<td colspan="3"  class="tableBdAltIta">
			<br>The external power-of-attorney regulations as filed with
			the Chamber of Commerce are presented in the annex.
		</td>
	</tr>
	<tr>
		<td colspan="3"  class="tableBd">
			<br><b>External signatory power</b>
		</td>
	</tr>
	<tr>
		<td colspan="3"  class="tableBdAltIta">
			<br>External signatory powers are powers to legally sign
			documents on behalf of ING Bank N.V. An employee who has
			been granted external powers is authorised to:
		</td>
	</tr>
	<tr>
		<td width="10" class="tableBdalt" valign="top"> &nbsp;</td>
		<td colspan="2" class="tableBdalt">
			commit the bank towards third parties by signing a
			document setting out a legal transaction (almost always)
			together with another ING employee with external
			signatory powers.
		</td>
	</tr>
	<tr>
		<td colspan="3"  class="tableBd">
			<br>Some examples of legal transactions are:
		</td>
	</tr>
	<tr>
		<td width="10" class="tableBdalt" valign="top"> &nbsp;</td>
		<td colspan="2" class="tableBdalt">
			a loan agreement (multilateral legal transaction);
		</td>
	</tr>
	<tr>
		<td width="10" class="tableBdalt" valign="top"> &nbsp;</td>
		<td colspan="2" class="tableBdalt">
			a mortgage offer (unilateral legal transaction).
		</td>
	</tr>
	<tr>
		<td colspan="3"  class="tableBdAltIta">
			<br>The power-of-attorney regulations only apply, therefore,
			to written legal transactions with third parties on
			behalf of ING Bank N.V. The regulations do not apply to
			oral comments, recommendations, commitments, etc. by ING
			employees to third parties.
		</td>
	</tr>
	<tr>
		<td colspan="3"  class',sysdate(),1,sysdate(),1);
Insert into VAR_HTMLTEXT (VHT_PAGENAME,VHT_NUMBER,VHT_HTMLTEXT,VHT_CREATEDON,VHT_CREATEDBYID,VHT_CHANGEDON,VHT_CHANGEDBYID) values ('proxy_scheme_int.jsp',21,'="tableBd">
			<br>Holders of external powers of attorney are listed in the
			trade register of the Chamber of Commerce.
		</td>
	</tr>
	<tr>
		<td colspan="3"  class="tableBd">
			<br><b>Internal signatory powers</b>
		</td>
	</tr>
	<tr>
		<td colspan="3"  class="tableBdAltIta">
			<br>Holders of external powers of attorney are generally also
			subject to internal power-of-attorney regulations. These
			regulations are set out in an internal authority matrix
			at each business unit which specifies, for example, that
			an attorney may only perform legal transactions from the
			office where he/she works and he/she may only perform
			specified legal transactions, for example, with respect
			to securities transactions.
		</td>
	</tr>
</table>
		<!-- end content in one cel of the outer table -->
		</td>
	</tr>
</table>
<PAGE_MARKER_SIGNATUREBOOK>
<PAGE_MARKER_SIGNATUREBOOK>
<div class="prspagebreakafter">&nbsp;</div>

<table width="620" border="0" cellspacing="0" cellpadding="0">
<tr>  <td width="20" class="tableBdalt" valign="top">&nbsp;</td> <td width="600" class="tableBdalt">
<!-- end classification and start of the schema -->
<table width="600" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td colspan="3"  class="tableBdAltIta">
			<br> The internal power-of-attorney regulations only apply
			internally; they do not apply to third parties and are not
			registered with the Chamber of Commerce. The bank cannot
			claim an attorney''s breach of internal powers of attorney
			as a defence against a third party. The internal
			power-of-attorney regulations should not be
			confused with any internal power to approve internal ING
			documents such as expense claims, internal circulars
			etc., as this is completely se',sysdate(),1,sysdate(),1);
Insert into VAR_HTMLTEXT (VHT_PAGENAME,VHT_NUMBER,VHT_HTMLTEXT,VHT_CREATEDON,VHT_CREATEDBYID,VHT_CHANGEDON,VHT_CHANGEDBYID) values ('proxy_scheme_int.jsp',22,'parate from both internal
			and external power-of-attorney regulations.
		</td>
	</tr>
	<tr>
		<td colspan="3"  class="tableBd">
			<br><b>Registration at the Chamber of Commerce</b>
		</td>
	</tr>
	<tr>
		<td colspan="3"  class="tableBdAltIta">
			<br>ING has an extensive and sophisticated network of
			functions, but only a limited number of them bring with
			them an external power of attorney to commit ING Bank
			N.V. in writing. Outsiders must be able to check the
			details of these employees and so the signatures of
			employees with external power of attorney are filed at
			the Chamber of Commerce. The external power of attorney
			regulations are also available at the Chamber of Commerce
			so that third parties can ascertain:
		</td>
	</tr>
	<tr>
		<td width="10" class="tableBdalt" valign="top"> &nbsp;</td>
		<td colspan="2" class="tableBdalt">
			the types of power of attorney recognised at ING;
		</td>
	</tr>
	<tr>
		<td width="10" class="tableBdalt" valign="top"> &nbsp;</td>
		<td colspan="2" class="tableBdalt">
			the permitted combinations of powers of attorney (who may sign with whom);
		</td>
	</tr>
	<tr>
		<td width="10" class="tableBdalt" valign="top"> &nbsp;</td>
		<td colspan="2" class="tableBdalt">
			the transactions that can be signed for;
		</td>
	</tr>
	<tr>
		<td width="10" class="tableBdalt" valign="top"> &nbsp;</td>
		<td colspan="2" class="tableBdalt">
			the maximum amount that an attorney can sign for.
		</td>
	</tr>
	<tr>
		<td colspan="3"  class="tableBdAltIta">
			<br>The bank can in principle declare a legal transaction
			which is signed by an employee who is not authorised to
			do so as void.
		</td>
	</tr>
	<tr>
		<td colspan="3"  class="tableBd">
			<br><b>Uniform exte',sysdate(),1,sysdate(),1);
Insert into VAR_HTMLTEXT (VHT_PAGENAME,VHT_NUMBER,VHT_HTMLTEXT,VHT_CREATEDON,VHT_CREATEDBYID,VHT_CHANGEDON,VHT_CHANGEDBYID) values ('proxy_scheme_int.jsp',23,'rnal power-of-attorney regulations at
			Postbank N.V. and ING Bank N.V.</b>
		</td>
	</tr>
	<tr>
		<td colspan="3"  class="tableBdAltIta">
			<br>The external power-of-attorney regulations of ING Bank
			N.V. and Postbank N.V. are the same. This has been
			decided as private and commercial Postbank products are
			also sold by ING Bank Netherlands. Consequently, staff
			who have powers of attorney for both ING Bank N.V. and
			Postbank N.V. and who conclude, say, loan agreements or
			sell investment products for either welcome the
			standardisation of the powers of attorney regulations. It
			is a practical consideration for them that their powers
			of attorney is the same irrespective of the brand, thus
			avoiding confusion as to which they have to comply with.
			There are also other holders of ING Bank N.V. and/or
			Postbank N.V. powers of attorney at other business units.
		</td>
	</tr>
	<tr>
		<td colspan="3"  class="tableBd">
			<br><b>Notes on external powers of attorney</b>
		</td>
	</tr>
	<tr>
		<td colspan="3"  class="tableBd">
			<br><b><i>The dual signatory system</i></b>
		</td>
	</tr>
	<tr>
		<td colspan="3"  class="tableBdAltIta">
			The dual signatory (joint authorisation) system applies
			across the board and so also applies to the current
			category ''C'' signatories in the ING Bank branch
			organisation. The combination of two attorneys determines
			to what level legal transactions are authorised. Only in
			exceptional cases can a single attorney legally commit
			the bank without a second attorney. The dual signature
			system is in force to ensure segregation of duties and is
			expressed by placing the signatures of both attorneys on
			the document for the third party.
		</td>
	</tr>
</table>',sysdate(),1,sysdate(),1);
Insert into VAR_HTMLTEXT (VHT_PAGENAME,VHT_NUMBER,VHT_HTMLTEXT,VHT_CREATEDON,VHT_CREATEDBYID,VHT_CHANGEDON,VHT_CHANGEDBYID) values ('proxy_scheme_int.jsp',24,'
		<!-- end content in one cel of the outer table -->
		</td>
	</tr>
</table>
<PAGE_MARKER_SIGNATUREBOOK>
<PAGE_MARKER_SIGNATUREBOOK>
<div class="prspagebreakafter">&nbsp;</div>

<table width="620" border="0" cellspacing="0" cellpadding="0">
<tr>  <td width="20" class="tableBdalt" valign="top">&nbsp;</td> <td width="600" class="tableBdalt">
<!-- end classification and start of the schema -->
<table width="600" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td colspan="3"  class="tableBd">
			<br><b><i>Classification of work into blocks</i></b>
		</td>
	</tr>
	<tr>
		<td colspan="3"  class="tableBdAltIta">
			There are three blocks of work for the power-of-attorney
			regulations, blocks 1, 2 or 3.
		</td>
	</tr>
	<tr>
		<td colspan="3"  class="tableBdAltIta">
			<br>Block 1 is work customarily performed by head office,
			such as:
		</td>
	</tr>
	<tr>
		<td width="10" class="tableBdalt" valign="top">-&nbsp;</td>
		<td colspan="2" class="tableBdalt">
			acting as an asset manager;
		</td>
	</tr>
	<tr>
		<td width="10" class="tableBdalt" valign="top">-&nbsp;</td>
		<td colspan="2" class="tableBdalt">
			supervising mergers and acquisitions by ING;
		</td>
	</tr>
	<tr>
		<td width="10" class="tableBdalt" valign="top">-&nbsp;</td>
		<td colspan="2" class="tableBdalt">
			participating in new securities issues.
		</td>
	</tr>
	<tr>
		<td colspan="3"  class="tableBdAltIta">
			<br>Block 2 is work customarily performed by the branch
			organisation. Most of the banking activities relate to
			work on product and customer sales and management
			activities, such as:
		</td>
	</tr>
	<tr>
		<td width="10" class="tableBdalt" valign="top">-&nbsp;</td>
		<td colspan="2" class="tableBdalt">
			concluding credit agreemen',sysdate(),1,sysdate(),1);
Insert into VAR_HTMLTEXT (VHT_PAGENAME,VHT_NUMBER,VHT_HTMLTEXT,VHT_CREATEDON,VHT_CREATEDBYID,VHT_CHANGEDON,VHT_CHANGEDBYID) values ('proxy_scheme_int.jsp',25,'ts;
		</td>
	</tr>
	<tr>
		<td width="10" class="tableBdalt" valign="top">-&nbsp;</td>
		<td colspan="2" class="tableBdalt">
			granting mortgages and accepting pledges;
		</td>
	</tr>
	<tr>
		<td width="10" class="tableBdalt" valign="top">-&nbsp;</td>
		<td colspan="2" class="tableBdalt">
			securities services;
		</td>
	</tr>
	<tr>
		<td width="10" class="tableBdalt" valign="top">-&nbsp;</td>
		<td colspan="2" class="tableBdalt">
			executing payment orders and other legal transactions in the context of funds transfers.
		</td>
	</tr>
	<tr>
		<td colspan="3"  class="tableBdAltIta">
			<br>Block 3 is also work customarily performed by the branch
			organisation, mainly at branches with a limited number
			staff.
		</td>
	</tr>

	<tr>
		<td colspan="3"  class="tableBd">
			<br><b><i>Types of power of attorney</i></b>
		</td>
	</tr>
	<tr>
		<td colspan="3"  class="tableBdAltIta">
			External power of attorney is designated by the letters
			S, A, B, C1, C2, D, E and F.
		</td>
	</tr>
	<tr>
		<td colspan="3"  class="tableBd">
			<br><i>Category S power of attorney</i>
		</td>
	</tr>
	<tr>
		<td colspan="3"  class="tableBdAltIta">
			For <b>external</b> purposes, a category S attorney may sign,
			together with an attorney in category:
		</td>
	</tr>
	<tr>
		<td width="10" class="tableBdalt" valign="top">-&nbsp;</td>
		<td colspan="2" class="tableBdalt">
			S, A or B, for work in blocks 1, 2 or 3, with no
  			<b>external</b> maximum amount;
		</td>
	</tr>
	<tr>
		<td width="10" class="tableBdalt" valign="top">-&nbsp;</td>
		<td colspan="2" class="tableBdalt">
			C1, for work in blocks 2 or 3, with no <b>external</b>
  			maximum amount;
		</td>
	</tr>
	<tr>
		<td width="10" class="tableBdalt" valign="top">-',sysdate(),1,sysdate(),1);
Insert into VAR_HTMLTEXT (VHT_PAGENAME,VHT_NUMBER,VHT_HTMLTEXT,VHT_CREATEDON,VHT_CREATEDBYID,VHT_CHANGEDON,VHT_CHANGEDBYID) values ('proxy_scheme_int.jsp',26,'&nbsp;</td>
		<td colspan="2" class="tableBdalt">
			C2, for work in block 2, up to EUR 5 million;
		</td>
	</tr>
	<tr>
		<td width="10" class="tableBdalt" valign="top">-&nbsp;</td>
		<td colspan="2" class="tableBdalt">
			D, for work in block 2, up to EUR 1 million.
		</td>
	</tr>
	<tr>
		<td colspan="3" class="tableBdalt">
			In addition, for <b>external</b> purposes, a category S power of
			attorney may sign alone for work in block 3, up to EUR 1
			million.
		</td>
	</tr>
	<tr>
		<td colspan="3"  class="tableBd">
			<br><i>Category A power of attorney</i>
		</td>
	</tr>
	<tr>
		<td colspan="3"  class="tableBdAltIta">
			For <b>external</b> purposes, a category A attorney may sign,
			together with an attorney in category:
		</td>
	</tr>
	<tr>
		<td width="10" class="tableBdalt" valign="top">-&nbsp;</td>
		<td colspan="2" class="tableBdalt">
			S, A or B, for work in blocks 1, 2 or 3, with no
			<b>external</b> maximum amount;
		</td>
	</tr>
	<tr>
		<td width="10" class="tableBdalt" valign="top">-&nbsp;</td>
		<td colspan="2" class="tableBdalt">
			C1, for work in blocks 2 or 3, with no <b>external</b>
			maximum amount;
		</td>
	</tr>
	<tr>
		<td width="10" class="tableBdalt" valign="top">-&nbsp;</td>
		<td colspan="2" class="tableBdalt">
			C2, for work in blocks 2 or 3, up to EUR 5 million;
		</td>
	</tr>
	<tr>
		<td width="10" class="tableBdalt" valign="top">-&nbsp;</td>
		<td colspan="2" class="tableBdalt">
			D, for work in block 2, up to EUR 1 million.
		</td>
	</tr>
	<tr>
		<td colspan="3" class="tableBdAltIta">
			In addition, for <b>external</b> purposes, a category A attorney
			may sign alone for work in block 3, up to EUR 1 million.
		</td>
	</tr>
	<tr>
		<td colspan="3"  class="tableBd">
		',sysdate(),1,sysdate(),1);
Insert into VAR_HTMLTEXT (VHT_PAGENAME,VHT_NUMBER,VHT_HTMLTEXT,VHT_CREATEDON,VHT_CREATEDBYID,VHT_CHANGEDON,VHT_CHANGEDBYID) values ('proxy_scheme_int.jsp',27,'	<br><i>Category B power of attorney</i>
		</td>
	</tr>
	<tr>
		<td colspan="3"  class="tableBdAltIta">
			For <b>external</b> purposes, a category B attorney may sign,
			together with an attorney in category:
		</td>
	</tr>
	<tr>
		<td width="10" class="tableBdalt" valign="top">-&nbsp;</td>
		<td colspan="2" class="tableBdalt">
			S or A, for work in blocks 1, 2 or 3, with no <b>external</b> maximum amount.
		</td>
	</tr>
	<tr>
		<td colspan="3"  class="tableBdAltIta">
			In addition, for <b>external</b> purposes, a category B attorney
			may sign alone for work in block 3, up to EUR 1 million.
		</td>
	</tr>
</table>
		<!-- end content in one cel of the outer table -->
		</td>
	</tr>
</table>
<PAGE_MARKER_SIGNATUREBOOK>
<PAGE_MARKER_SIGNATUREBOOK>
<div class="prspagebreakafter">&nbsp;</div>

<table width="620" border="0" cellspacing="0" cellpadding="0">
<tr>  <td width="20" class="tableBdalt" valign="top">&nbsp;</td> <td width="600" class="tableBdalt">
<!-- end classification and start of the schema -->
<table width="600" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td colspan="3"  class="tableBd">
			<br><i>Category C1 power of attorney</i>
		</td>
	</tr>
	<tr>
		<td colspan="3"  class="tableBdAltIta">
			For <b>external</b> purposes, a category C1 attorney may sign,
			together with an attorney in category:
		</td>
	</tr>
	<tr>
		<td width="10" class="tableBdalt" valign="top">-&nbsp;</td>
		<td colspan="2" class="tableBdalt">
			S, A or C1, for work in blocks 2 or 3, with no
			<b>external</b> maximum amount;
		</td>
	</tr>
	<tr>
		<td width="10" class="tableBdalt" valign="top">-&nbsp;</td>
		<td colspan="2" class="tableBdalt">
			C2, for work in blocks 2 or 3, up to EUR 5 million;
		</td>
	</tr>
	<t',sysdate(),1,sysdate(),1);
Insert into VAR_HTMLTEXT (VHT_PAGENAME,VHT_NUMBER,VHT_HTMLTEXT,VHT_CREATEDON,VHT_CREATEDBYID,VHT_CHANGEDON,VHT_CHANGEDBYID) values ('proxy_scheme_int.jsp',28,'r>
		<td width="10" class="tableBdalt" valign="top">-&nbsp;</td>
		<td colspan="2" class="tableBdalt">
			D, for work in block 2, up to EUR 1 million.
		</td>
	</tr>
	<tr>
		<td colspan="3"  class="tableBdAltIta">
			In addition, for <b>external</b> purposes, a category C1 attorney
			may sign alone for work in block 3, up to EUR 1 million.
		</td>
	</tr>
	<tr>
		<td colspan="3"  class="tableBd">
			<br><i>Category C2 power of attorney</i>
		</td>
	</tr>
	<tr>
		<td colspan="3"  class="tableBdAltIta">
			For <b>external</b> purposes, a category C2 attorney may sign,
			together with an attorney in category:
		</td>
	</tr>
	<tr>
		<td width="10" class="tableBdalt" valign="top">-&nbsp;</td>
		<td colspan="2" class="tableBdalt">
			S, A, C1 or C2 for work in blocks 2 or 3, up to EUR 5 million;
		</td>
	</tr>
	<tr>
		<td width="10" class="tableBdalt" valign="top">-&nbsp;</td>
		<td colspan="2" class="tableBdalt">
			D for work in block 2, up to EUR 1 million.
		</td>
	</tr>
	<tr>
		<td colspan="3"  class="tableBdAltIta">
			In addition, for <b>external</b> purposes, a category C2 attorney
			may sign alone for work in block 3, up to EUR 1 million.
		</td>
	</tr>
	<tr>
		<td colspan="3"  class="tableBd">
			<br><i>Category D power of attorney</i>
		</td>
	</tr>
	<tr>
		<td colspan="3"  class="tableBdAltIta">
			For <b>external</b> purposes, a category D attorney may sign,
			together with an attorney in category:
		</td>
	</tr>
	<tr>
		<td width="10" class="tableBdalt" valign="top">-&nbsp;</td>
		<td colspan="2" class="tableBdalt">
			S, A, C1 or C2 for work in block 2, up to EUR 1 million.
		</td>
	</tr>
	<tr>
		<td colspan="3"  class="tableBdAltIta">
			In addition, for external purposes, a category D attorney',sysdate(),1,sysdate(),1);
Insert into VAR_HTMLTEXT (VHT_PAGENAME,VHT_NUMBER,VHT_HTMLTEXT,VHT_CREATEDON,VHT_CREATEDBYID,VHT_CHANGEDON,VHT_CHANGEDBYID) values ('proxy_scheme_int.jsp',29,'
			may sign alone for work in block 3, up to EUR 1 million.
		</td>
	</tr>
	<tr>
		<td colspan="3"  class="tableBd">
			<br><i>Category E and F power of attorney</i>
		</td>
	</tr>
	<tr>
		<td colspan="3"  class="tableBdAltIta">
			Category E and F powers of attorney of ING Bank N.V.,
			granted to ING Bank N.V. branch employees abroad, are unchanged.
			These powers of attorney have not been filed at the trade register
			of a Dutch Chamber of Commerce.
		</td>
	</tr>
	<tr>
		<td colspan="3"  class="tableBdAltIta">
			<br>Category B or D attorneys may not commit ING jointly with
			an attorney of the same level. In every case, therefore,
			they have to act jointly with an attorney with a ''higher''
			level of authorisation. A category D attorney may not,
			however, sign with a category B attorney.
		</td>
	</tr>	<tr>
		<td colspan="3"  class="tableBd">
			<br><b>Substitution</b>
		</td>
	</tr>
	<tr>
		<td colspan="3"  class="tableBdAltIta">
			The right of substitution is the right to delegate
			authority to another person. The power of substitution
			should be exercised with restraint.
		</td>
	</tr>
	<tr>
		<td colspan="3"  class="tableBdAltIta">
			<br>Generally speaking, a category <b>S attorney</b> is the
			equivalent of a member of the Executive Board for the
			purposes of these regulations. A category S attorney may,
			therefore, delegate his authority to any third party
			provided that he does so together with another category S
			attorney. Such a third party may, however, only commit
			the bank if he acts with another person competent to act
			(dual signatory system) in which case the third party has
			independent freedom of decision. A category <b>S</b>,
			<b>A</b> or <b>B attorney</b>, along wi',sysdate(),1,sysdate(),1);
Insert into VAR_HTMLTEXT (VHT_PAGENAME,VHT_NUMBER,VHT_HTMLTEXT,VHT_CREATEDON,VHT_CREATEDBYID,VHT_CHANGEDON,VHT_CHANGEDBYID) values ('proxy_scheme_int.jsp',30,'th a category
			<b>S</b>, <b>A</b> or <b>B attorney</b>, may
			delegate the authority that they have jointly to a third
			party, provided that this authority is limited to a
			narrowly defined transaction. It is then no longer
			necessary for that third party to act with another person
			competent to act.
		</td>
	</tr>
</table>
</table>
<PAGE_MARKER_SIGNATUREBOOK>
<PAGE_MARKER_SIGNATUREBOOK>
<div class="prspagebreakafter">&nbsp;</div>

<table width="620" border="0" cellspacing="0" cellpadding="0">
<tr>  <td width="20" class="tableBdalt" valign="top">&nbsp;</td> <td width="600" class="tableBdalt">
<!-- end classification and start of the schema -->
<table width="600" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td colspan="3"  class="tableBdAltIta">
			<br>Finally, a category <b>C1</b> or <b>C2 attorney</b>, along with a
			category <b>S</b>, <b>A</b>, <b>B</b>, <b>C1</b> or <b>C2 attorney</b>,
			may delegate the authority that they have jointly to a person attached to
			a civil-law notary''s office in connection with a
			transaction that requires a notarial instrument, provided
			that the authority granted to the person is limited to a
			narrowly defined transaction (see the power of
			substitution above).
		</td>
	</tr>
	<tr>
		<td colspan="3"  class="tableBd">
			<br><b>Procedures</b>
		</td>
	</tr>
	<tr>
		<td colspan="3"  class="tableBdAltIta">
			<br>External power of attorney is attached to a position.
			External power of attorney is not granted to an employee
			as a matter of course. External power of attorney may
			only be granted to staff employed by one of ING''s legal
			entities.
		</td>
	</tr>
	<tr>
		<td colspan="3"  class="tableBdAltIta">
			<br>Designated line managers, us',sysdate(),1,sysdate(),1);
Insert into VAR_HTMLTEXT (VHT_PAGENAME,VHT_NUMBER,VHT_HTMLTEXT,VHT_CREATEDON,VHT_CREATEDBYID,VHT_CHANGEDON,VHT_CHANGEDBYID) values ('proxy_scheme_int.jsp',31,'ually general managers of
			business units, are authorised by the Executive Board of
			ING Group to grant authorities within their business unit
			as described in the external power-of-attorney
			regulations.
		</td>
	</tr>
	<tr>
		<td colspan="3"  class="tableBd">
			<br><b><i>Application for, changes in or withdrawal of power of attorney</i></b>
		</td>
	</tr>
	<tr>
		<td colspan="3"  class="tableBdAltIta">
			External power of attorney may be applied for on
			<u>www.prs.intranet</u>. An employee''s immediate supervisor
			decides whether the employee is eligible for external
			power of attorney. The designated line manager of the
			relevant business unit approves an application or change
			by signing the grant form. The form is then sent to the
			business unit contact/co-ordinator (see <u>www.prs.intranet</u>)
			for further processing.
		</td>
	</tr>
</table>
</table>',sysdate(),1,sysdate(),1);
Insert into VAR_HTMLTEXT (VHT_PAGENAME,VHT_NUMBER,VHT_HTMLTEXT,VHT_CREATEDON,VHT_CREATEDBYID,VHT_CHANGEDON,VHT_CHANGEDBYID) values ('proxy_scheme_nn.jsp',22,'parate from both internal
			and external power-of-attorney regulations.
		</td>
	</tr>
	<tr>
		<td colspan="3"  class="tableBd">
			<br><b>Registration at the Chamber of Commerce</b>
		</td>
	</tr>
	<tr>
		<td colspan="3"  class="tableBdAltIta">
			<br>ING has an extensive and sophisticated network of
			functions, but only a limited number of them bring with
			them an external power of attorney to commit ING Bank
			N.V. in writing. Outsiders must be able to check the
			details of these employees and so the signatures of
			employees with external power of attorney are filed at
			the Chamber of Commerce. The external power of attorney
			regulations are also available at the Chamber of Commerce
			so that third parties can ascertain:
		</td>
	</tr>
	<tr>
		<td width="10" class="tableBdalt" valign="top"> &nbsp;</td>
		<td colspan="2" class="tableBdalt">
			the types of power of attorney recognised at ING;
		</td>
	</tr>
	<tr>
		<td width="10" class="tableBdalt" valign="top"> &nbsp;</td>
		<td colspan="2" class="tableBdalt">
			the permitted combinations of powers of attorney (who may sign with whom);
		</td>
	</tr>
	<tr>
		<td width="10" class="tableBdalt" valign="top"> &nbsp;</td>
		<td colspan="2" class="tableBdalt">
			the transactions that can be signed for;
		</td>
	</tr>
	<tr>
		<td width="10" class="tableBdalt" valign="top"> &nbsp;</td>
		<td colspan="2" class="tableBdalt">
			the maximum amount that an attorney can sign for.
		</td>
	</tr>
	<tr>
		<td colspan="3"  class="tableBdAltIta">
			<br>The bank can in principle declare a legal transaction
			which is signed by an employee who is not authorised to
			do so as void.
		</td>
	</tr>
	<tr>
		<td colspan="3"  class="tableBd">
			<br><b>Uniform exte',sysdate(),1,sysdate(),1);
Insert into VAR_HTMLTEXT (VHT_PAGENAME,VHT_NUMBER,VHT_HTMLTEXT,VHT_CREATEDON,VHT_CREATEDBYID,VHT_CHANGEDON,VHT_CHANGEDBYID) values ('proxy_scheme_nn.jsp',23,'rnal power-of-attorney regulations at
			Postbank N.V. and ING Bank N.V.</b>
		</td>
	</tr>
	<tr>
		<td colspan="3"  class="tableBdAltIta">
			<br>The external power-of-attorney regulations of ING Bank
			N.V. and Postbank N.V. are the same. This has been
			decided as private and commercial Postbank products are
			also sold by ING Bank Netherlands. Consequently, staff
			who have powers of attorney for both ING Bank N.V. and
			Postbank N.V. and who conclude, say, loan agreements or
			sell investment products for either welcome the
			standardisation of the powers of attorney regulations. It
			is a practical consideration for them that their powers
			of attorney is the same irrespective of the brand, thus
			avoiding confusion as to which they have to comply with.
			There are also other holders of ING Bank N.V. and/or
			Postbank N.V. powers of attorney at other business units.
		</td>
	</tr>
	<tr>
		<td colspan="3"  class="tableBd">
			<br><b>Notes on external powers of attorney</b>
		</td>
	</tr>
	<tr>
		<td colspan="3"  class="tableBd">
			<br><b><i>The dual signatory system</i></b>
		</td>
	</tr>
	<tr>
		<td colspan="3"  class="tableBdAltIta">
			The dual signatory (joint authorisation) system applies
			across the board and so also applies to the current
			category ''C'' signatories in the ING Bank branch
			organisation. The combination of two attorneys determines
			to what level legal transactions are authorised. Only in
			exceptional cases can a single attorney legally commit
			the bank without a second attorney. The dual signature
			system is in force to ensure segregation of duties and is
			expressed by placing the signatures of both attorneys on
			the document for the third party.
		</td>
	</tr>
</table>',sysdate(),1,sysdate(),1);
Insert into VAR_HTMLTEXT (VHT_PAGENAME,VHT_NUMBER,VHT_HTMLTEXT,VHT_CREATEDON,VHT_CREATEDBYID,VHT_CHANGEDON,VHT_CHANGEDBYID) values ('proxy_scheme_nn.jsp',21,'="tableBd">
			<br>Holders of external powers of attorney are listed in the
			trade register of the Chamber of Commerce.
		</td>
	</tr>
	<tr>
		<td colspan="3"  class="tableBd">
			<br><b>Internal signatory powers</b>
		</td>
	</tr>
	<tr>
		<td colspan="3"  class="tableBdAltIta">
			<br>Holders of external powers of attorney are generally also
			subject to internal power-of-attorney regulations. These
			regulations are set out in an internal authority matrix
			at each business unit which specifies, for example, that
			an attorney may only perform legal transactions from the
			office where he/she works and he/she may only perform
			specified legal transactions, for example, with respect
			to securities transactions.
		</td>
	</tr>
</table>
		<!-- end content in one cel of the outer table -->
		</td>
	</tr>
</table>
<PAGE_MARKER_SIGNATUREBOOK>
<PAGE_MARKER_SIGNATUREBOOK>
<div class="prspagebreakafter">&nbsp;</div>

<table width="620" border="0" cellspacing="0" cellpadding="0">
<tr>  <td width="20" class="tableBdalt" valign="top">&nbsp;</td> <td width="600" class="tableBdalt">
<!-- end classification and start of the schema -->
<table width="600" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td colspan="3"  class="tableBdAltIta">
			<br> The internal power-of-attorney regulations only apply
			internally; they do not apply to third parties and are not
			registered with the Chamber of Commerce. The bank cannot
			claim an attorney''s breach of internal powers of attorney
			as a defence against a third party. The internal
			power-of-attorney regulations should not be
			confused with any internal power to approve internal ING
			documents such as expense claims, internal circulars
			etc., as this is completely se',sysdate(),1,sysdate(),1);
Insert into VAR_HTMLTEXT (VHT_PAGENAME,VHT_NUMBER,VHT_HTMLTEXT,VHT_CREATEDON,VHT_CREATEDBYID,VHT_CHANGEDON,VHT_CHANGEDBYID) values ('proxy_scheme_nn.jsp',19,'
	<tr>
		<td colspan="3"  class="tableBd">
			<br><b>Introduction</b>
		</td>
	</tr>
	<tr>
		<td colspan="3"  class="tableBdAltIta">
			<br>This brochure is for all ING employees who have been
			granted external powers of attorney for ING Bank N.V. In
			their original form, the power-of-attorney regulations
			came into force on 5 September 2000 and this brochure has
			been prepared as a result of changes which took effect on
			1 March 2004. Since that date, ING Bank N.V. has had
			uniform power-of-attorney regulations, and this has
			brought a large number of practical benefits.
		</td>
	</tr>
	<tr>
		<td colspan="3"  class="tableBd">
			<br>This brochure briefly addresses the following subjects:
		</td>
	</tr>
	<tr>
		<td width="10" class="tableBdalt" valign="top"> &nbsp;</td>
		<td colspan="2" class="tableBdalt">
			the concept of external versus internal powers of attorney;
		</td>
	</tr>
	<tr>
		<td width="10" class="tableBdalt" valign="top"> &nbsp;</td>
		<td colspan="2" class="tableBdalt">
			registration at the Chamber of Commerce;
		</td>
	</tr>
	<tr>
		<td width="10" class="tableBdalt" valign="top"> &nbsp;</td>
		<td colspan="2" class="tableBdalt">
			uniform power-of-attorney regulations at Postbank N.V. and ING Bank N.V.;
		</td>
	</tr>
	<tr>
		<td width="10" class="tableBdalt" valign="top"> &nbsp;</td>
		<td colspan="2" class="tableBdalt">
			the external power-of-attorney regulations:
		</td>
	</tr>
	<tr>
		<td width="10" class="tableBdalt" valign="top">&nbsp;</td>
		<td width="30" class="tableBdalt" valign="top">-</td>
		<td width="560" class="tableBdalt">
			the external power-of-attorney regulations:
		</td>
	</tr>
	<tr>
		<td width="10" class="tableBdalt" valign="top"> &nbsp;</td>
',sysdate(),1,sysdate(),1);
Insert into VAR_HTMLTEXT (VHT_PAGENAME,VHT_NUMBER,VHT_HTMLTEXT,VHT_CREATEDON,VHT_CREATEDBYID,VHT_CHANGEDON,VHT_CHANGEDBYID) values ('proxy_scheme_nn.jsp',26,'&nbsp;</td>
		<td colspan="2" class="tableBdalt">
			C2, for work in block 2, up to EUR 5 million;
		</td>
	</tr>
	<tr>
		<td width="10" class="tableBdalt" valign="top">-&nbsp;</td>
		<td colspan="2" class="tableBdalt">
			D, for work in block 2, up to EUR 1 million.
		</td>
	</tr>
	<tr>
		<td colspan="3" class="tableBdalt">
			In addition, for <b>external</b> purposes, a category S power of
			attorney may sign alone for work in block 3, up to EUR 1
			million.
		</td>
	</tr>
	<tr>
		<td colspan="3"  class="tableBd">
			<br><i>Category A power of attorney</i>
		</td>
	</tr>
	<tr>
		<td colspan="3"  class="tableBdAltIta">
			For <b>external</b> purposes, a category A attorney may sign,
			together with an attorney in category:
		</td>
	</tr>
	<tr>
		<td width="10" class="tableBdalt" valign="top">-&nbsp;</td>
		<td colspan="2" class="tableBdalt">
			S, A or B, for work in blocks 1, 2 or 3, with no
			<b>external</b> maximum amount;
		</td>
	</tr>
	<tr>
		<td width="10" class="tableBdalt" valign="top">-&nbsp;</td>
		<td colspan="2" class="tableBdalt">
			C1, for work in blocks 2 or 3, with no <b>external</b>
			maximum amount;
		</td>
	</tr>
	<tr>
		<td width="10" class="tableBdalt" valign="top">-&nbsp;</td>
		<td colspan="2" class="tableBdalt">
			C2, for work in blocks 2 or 3, up to EUR 5 million;
		</td>
	</tr>
	<tr>
		<td width="10" class="tableBdalt" valign="top">-&nbsp;</td>
		<td colspan="2" class="tableBdalt">
			D, for work in block 2, up to EUR 1 million.
		</td>
	</tr>
	<tr>
		<td colspan="3" class="tableBdAltIta">
			In addition, for <b>external</b> purposes, a category A attorney
			may sign alone for work in block 3, up to EUR 1 million.
		</td>
	</tr>
	<tr>
		<td colspan="3"  class="tableBd">
		',sysdate(),1,sysdate(),1);
Insert into VAR_HTMLTEXT (VHT_PAGENAME,VHT_NUMBER,VHT_HTMLTEXT,VHT_CREATEDON,VHT_CREATEDBYID,VHT_CHANGEDON,VHT_CHANGEDBYID) values ('proxy_scheme_nn.jsp',24,'
		<!-- end content in one cel of the outer table -->
		</td>
	</tr>
</table>
<PAGE_MARKER_SIGNATUREBOOK>
<PAGE_MARKER_SIGNATUREBOOK>
<div class="prspagebreakafter">&nbsp;</div>

<table width="620" border="0" cellspacing="0" cellpadding="0">
<tr>  <td width="20" class="tableBdalt" valign="top">&nbsp;</td> <td width="600" class="tableBdalt">
<!-- end classification and start of the schema -->
<table width="600" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td colspan="3"  class="tableBd">
			<br><b><i>Classification of work into blocks</i></b>
		</td>
	</tr>
	<tr>
		<td colspan="3"  class="tableBdAltIta">
			There are three blocks of work for the power-of-attorney
			regulations, blocks 1, 2 or 3.
		</td>
	</tr>
	<tr>
		<td colspan="3"  class="tableBdAltIta">
			<br>Block 1 is work customarily performed by head office,
			such as:
		</td>
	</tr>
	<tr>
		<td width="10" class="tableBdalt" valign="top">-&nbsp;</td>
		<td colspan="2" class="tableBdalt">
			acting as an asset manager;
		</td>
	</tr>
	<tr>
		<td width="10" class="tableBdalt" valign="top">-&nbsp;</td>
		<td colspan="2" class="tableBdalt">
			supervising mergers and acquisitions by ING;
		</td>
	</tr>
	<tr>
		<td width="10" class="tableBdalt" valign="top">-&nbsp;</td>
		<td colspan="2" class="tableBdalt">
			participating in new securities issues.
		</td>
	</tr>
	<tr>
		<td colspan="3"  class="tableBdAltIta">
			<br>Block 2 is work customarily performed by the branch
			organisation. Most of the banking activities relate to
			work on product and customer sales and management
			activities, such as:
		</td>
	</tr>
	<tr>
		<td width="10" class="tableBdalt" valign="top">-&nbsp;</td>
		<td colspan="2" class="tableBdalt">
			concluding credit agreemen',sysdate(),1,sysdate(),1);
Insert into VAR_HTMLTEXT (VHT_PAGENAME,VHT_NUMBER,VHT_HTMLTEXT,VHT_CREATEDON,VHT_CREATEDBYID,VHT_CHANGEDON,VHT_CHANGEDBYID) values ('proxy_scheme_int.jsp',13,', provided that he acts jointly with a category
		S, A, C1 or C2 attorney, as applicable, the power to
		delegate the power of representation that he has with
		that attorney to a third party, provided that the
		substitution occurs in respect of a concrete transaction
		which is specifically and accurately described in writing
		in the authorisation concerning the substitution and is
		intended solely to enable the third party to appear on
		behalf of the company in order to execute an instrument
		before a civil-law notary in respect of the above-
		mentioned concrete transaction, to which third party
		independent authority may be granted for the company with
		respect to the aforementioned transaction.
	</td></tr>

	<tr><td  class="tableBdAlt" colspan="3"><br><A NAME="type_d"></A>
	<B>Category D attorney:</B> officer having special power to
	represent the company;
	</td></tr>
	<tr><td width="10" class="tableBdalt" valign="top">-</td>
	<td colspan="2" class="tableBdalt" valign="top">
		empowered to perform the legal transactions referred
	  to in block 2, provided that he acts jointly with a
	  category S, A, C1 or C2 attorney, provided the amount
	  involved does not exceed EUR 1,000,000;
	</td></tr>
	<tr><td width="10" class="tableBdalt" valign="top">-</td>
	<td colspan="2" class="tableBdalt" valign="top">
		empowered to perform the legal transactions referred
	  to in block 3, provided the amount involved does not
	  exceed EUR 1,000,000.
	</td></tr>
</table>

<!-- Pagebreak so close and reopen outer table  -->
</td></tr></table>
<PAGE_MARKER_SIGNATUREBOOK>
<PAGE_MARKER_SIGNATUREBOOK>
<div class="prspagebreakafter">&nbsp;</div>

<table width="620" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td width="20" class="ta',sysdate(),1,sysdate(),1);
Insert into VAR_HTMLTEXT (VHT_PAGENAME,VHT_NUMBER,VHT_HTMLTEXT,VHT_CREATEDON,VHT_CREATEDBYID,VHT_CHANGEDON,VHT_CHANGEDBYID) values ('proxy_scheme_int.jsp',14,'bleBdalt" valign="top">&nbsp;</td>
		<td width="600" class="tableBdalt">
		<!-- end classification and start of the schema -->
		<table width="600" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td width="600" class="tableBdalt" align="middle">&nbsp;</div><br><br><div class="header">
				matrix for standardised power-of-attorney regulations at</div>
				</td>
			</tr>
			<tr>
				<td  width="600" class="tableBdalt" align="middle"><div class="header">
				ING Bank N.V.</div><br><br>
				</td>
			</tr>
		</table>
		<!-- begin matrix -->
		<table width="600" border="0" cellspacing="1" cellpadding="4" bgcolor="#000000">
			<tr>
				<td width="100" class="tableHd" bgcolor="#c2dbf8" >Category</td>
				<td width="100" class="tableHd" bgcolor="#c2dbf8">Block 1</td>
				<td width="200" class="tableHd" bgcolor="#c2dbf8">Block 2</td>
				<td width="200" class="tableHd" bgcolor="#c2dbf8">Block 3</td>
			</tr>
			<tr>
				<td bgcolor="#ffffff"  class="tableBdalt" valign="top">S</td>
				<td bgcolor="#ffffff"  class="tableBdalt" valign="top">
					<table  border="0" cellspacing="0" cellpadding="0">
		      			<tr><td class="tableBdalt">S + S</td></tr>
		      			<tr><td class="tableBdalt">S + A</td></tr>
		      			<tr><td class="tableBdalt">S + B</td></tr>
		  			</table>
				</td>
				<td bgcolor="#ffffff"  class="tableBdalt" valign="top">
					 <table border="0" cellspacing="0" cellpadding="0">
						<tr><td class="tableBdalt" >S + S</td><td class="tableBdalt">&nbsp;</td></tr>
						<tr><td class="tableBdalt">S + A</td><td class="tableBdalt">&nbsp;</td></tr>
						<tr><td class="tableBdalt">S + B</td><td class="tableBdalt">&nbsp;</td></tr>
						<tr><td class="tableBdalt">S + C1</td><td class="tableBdalt">&nbsp;</td></tr>
						<tr><',sysdate(),1,sysdate(),1);
Insert into VAR_HTMLTEXT (VHT_PAGENAME,VHT_NUMBER,VHT_HTMLTEXT,VHT_CREATEDON,VHT_CREATEDBYID,VHT_CHANGEDON,VHT_CHANGEDBYID) values ('proxy_scheme_int.jsp',15,'td class="tableBdalt">S + C2</td><td class="tableBdalt">&nbsp;max. EUR 5 million</td></tr>
						<tr><td class="tableBdalt">S + D</td><td class="tableBdalt">&nbsp;max. EUR 1 million</td></tr>
					 </table>
				</td>
				<td bgcolor="#ffffff"  class="tableBdalt" valign="top">
					 <table border="0" cellspacing="0" cellpadding="0">
						<tr><td class="tableBdalt" >S max EUR 1 million</td><td class="tableBdalt">&nbsp;</td></tr>
					 </table>
				</td>
			</tr>
			<tr>
				<td bgcolor="#ffffff"  class="tableBdalt" valign="top">A</td>
				<td bgcolor="#ffffff"  class="tableBdalt" valign="top">
					<table  border="0" cellspacing="0" cellpadding="0">
					<tr><td class="tableBdalt">A + S</td></tr>
					<tr><td class="tableBdalt">A + A</td></tr>
					<tr><td class="tableBdalt">A + B</td></tr>
					</table>
				</td>
				<td bgcolor="#ffffff"  class="tableBdalt" valign="top">
					<table  border="0" cellspacing="0" cellpadding="0">
					<tr><td class="tableBdalt">A + S</td><td class="tableBdalt">&nbsp;</td></tr>
					<tr><td class="tableBdalt">A + A</td><td class="tableBdalt">&nbsp;</td></tr>
					<tr><td class="tableBdalt">A + B</td><td class="tableBdalt">&nbsp;</td></tr>
					<tr><td class="tableBdalt">A + C1</td><td class="tableBdalt">&nbsp;</td></tr>
					<tr><td class="tableBdalt">A + C2</td><td class="tableBdalt">&nbsp;max. EUR 5 million</td></tr>
					<tr><td class="tableBdalt">A + D</td><td class="tableBdalt">&nbsp;max. EUR 1 million</td></tr>
					</table>
				</td>
				<td bgcolor="#ffffff"  class="tableBdalt" valign="top">
					 <table border="0" cellspacing="0" cellpadding="0">
						<tr><td class="tableBdalt" >A max EUR 1 million</td><td class="tableBdalt">&nbsp;</td></tr>
					 </table>
				</td>
			</tr>
			<tr>
				<t',sysdate(),1,sysdate(),1);
Insert into VAR_HTMLTEXT (VHT_PAGENAME,VHT_NUMBER,VHT_HTMLTEXT,VHT_CREATEDON,VHT_CREATEDBYID,VHT_CHANGEDON,VHT_CHANGEDBYID) values ('proxy_scheme_int.jsp',16,'d bgcolor="#ffffff"  class="tableBdalt" valign="top">B</td>
				<td bgcolor="#ffffff"  class="tableBdalt" valign="top">
					<table  border="0" cellspacing="0" cellpadding="0">
					<tr><td class="tableBdalt">B + S</td></tr>
					<tr><td class="tableBdalt">B + A</td></tr>
					</table>
				</td>
				<td bgcolor="#ffffff"  class="tableBdalt" valign="top">
					<table  border="0" cellspacing="0" cellpadding="0">
					<tr><td class="tableBdalt">B + S</td><td class="tableBdalt">&nbsp;</td></tr>
					<tr><td class="tableBdalt">B + A</td><td class="tableBdalt">&nbsp;</td></tr>
					</table>
				</td>
				<td bgcolor="#ffffff"  class="tableBdalt" valign="top">
					 <table border="0" cellspacing="0" cellpadding="0">
						<tr><td class="tableBdalt" >B max EUR 1 million</td><td class="tableBdalt">&nbsp;</td></tr>
					 </table>
				</td>
			</tr>
			<tr>
				<td bgcolor="#ffffff"  class="tableBdalt" valign="top">C1</td>
				<td bgcolor="#ffffff"  class="tableBdalt" valign="top">&nbsp;</td>
				<td bgcolor="#ffffff"  class="tableBdalt" valign="top">
					<table  border="0" cellspacing="0" cellpadding="0">
						<tr><td class="tableBdalt">C1 + S</td><td class="tableBdalt">&nbsp;</td></tr>
						<tr><td class="tableBdalt">C1 + A</td><td class="tableBdalt">&nbsp;</td></tr>
						<tr><td class="tableBdalt">C1 + C1</td><td class="tableBdalt">&nbsp;</td></tr>
						<tr><td class="tableBdalt">C1 + C2</td><td class="tableBdalt">&nbsp;max. EUR 5 million</td></tr>
						<tr><td class="tableBdalt">C1 + D</td><td class="tableBdalt">&nbsp;max. EUR 1 million</td></tr>
					</table>
				</td>
				<td bgcolor="#ffffff"  class="tableBdalt" valign="top">
					 <table border="0" cellspacing="0" cellpadding="0">
						<tr><td class="tableBdalt" >C1 max EUR ',sysdate(),1,sysdate(),1);
Insert into VAR_HTMLTEXT (VHT_PAGENAME,VHT_NUMBER,VHT_HTMLTEXT,VHT_CREATEDON,VHT_CREATEDBYID,VHT_CHANGEDON,VHT_CHANGEDBYID) values ('proxy_scheme_nn.jsp',20,'
		<td colspan="2" class="tableBdalt">
			the application, change and termination procedure.
		</td>
	</tr>
	<tr>
		<td colspan="3"  class="tableBdAltIta">
			<br>The external power-of-attorney regulations as filed with
			the Chamber of Commerce are presented in the annex.
		</td>
	</tr>
	<tr>
		<td colspan="3"  class="tableBd">
			<br><b>External signatory power</b>
		</td>
	</tr>
	<tr>
		<td colspan="3"  class="tableBdAltIta">
			<br>External signatory powers are powers to legally sign
			documents on behalf of ING Bank N.V. An employee who has
			been granted external powers is authorised to:
		</td>
	</tr>
	<tr>
		<td width="10" class="tableBdalt" valign="top"> &nbsp;</td>
		<td colspan="2" class="tableBdalt">
			commit the bank towards third parties by signing a
			document setting out a legal transaction (almost always)
			together with another ING employee with external
			signatory powers.
		</td>
	</tr>
	<tr>
		<td colspan="3"  class="tableBd">
			<br>Some examples of legal transactions are:
		</td>
	</tr>
	<tr>
		<td width="10" class="tableBdalt" valign="top"> &nbsp;</td>
		<td colspan="2" class="tableBdalt">
			a loan agreement (multilateral legal transaction);
		</td>
	</tr>
	<tr>
		<td width="10" class="tableBdalt" valign="top"> &nbsp;</td>
		<td colspan="2" class="tableBdalt">
			a mortgage offer (unilateral legal transaction).
		</td>
	</tr>
	<tr>
		<td colspan="3"  class="tableBdAltIta">
			<br>The power-of-attorney regulations only apply, therefore,
			to written legal transactions with third parties on
			behalf of ING Bank N.V. The regulations do not apply to
			oral comments, recommendations, commitments, etc. by ING
			employees to third parties.
		</td>
	</tr>
	<tr>
		<td colspan="3"  class',sysdate(),1,sysdate(),1);
Insert into VAR_HTMLTEXT (VHT_PAGENAME,VHT_NUMBER,VHT_HTMLTEXT,VHT_CREATEDON,VHT_CREATEDBYID,VHT_CHANGEDON,VHT_CHANGEDBYID) values ('proxy_scheme_nn.jsp',18,'lt">&nbsp;max. EUR 1 million</td></tr>
					</table>
				</td>
				<td bgcolor="#ffffff"  class="tableBdalt" valign="top">
					 <table border="0" cellspacing="0" cellpadding="0">
						<tr><td class="tableBdalt" >D max EUR 1 million</td><td class="tableBdalt">&nbsp;</td></tr>
					 </table>
				</td>
			</tr>
		</table>
		<!-- end matrix -->
		<table width="600" border="0" cellspacing="0" cellpadding="0">
			<tr>
			  <td width="10" class="tableBdalt" valign="top"><B>NB:</B>&nbsp;</td>
			  <td width="590" class="tableBdalt">
				<B> For the purposes of these regulations,
				notwithstanding his authority under the Articles of
				Association, a member of the Executive Board of ING Bank
				N.V. shall be the equivalent of a category S attorney</B>
			  </td>
			</tr>
			<tr>
			  <td width="10" class="tableBdalt" valign="top"><br>&nbsp;</td>
			  <td width="590" class="tableBdalt">&nbsp;</td>
			</tr>
			<tr>
			  <td width="10" class="tableBdalt" valign="top"><br>&nbsp;</td>
			  <td width="590" class="tableBdalt">&nbsp;</td>
			</tr>
		</table>
		<!-- end content in one cel of the outer table -->
		</td>
	</tr>
</table>
<PAGE_MARKER_SIGNATUREBOOK>
<PAGE_MARKER_SIGNATUREBOOK>
<div class="prspagebreakafter">&nbsp;</div>

<table width="620" border="0" cellspacing="0" cellpadding="0">
<tr>  <td width="20" class="tableBdalt" valign="top">&nbsp;</td> <td width="600" class="tableBdalt">
<!-- end classification and start of the schema -->
<table width="600" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td colspan="3"  class="tableBd">
			<b>Notes on the external power-of-attorney regulations for Postbank N.V.</b>
		</td>
	</tr>
	<tr>
		<td colspan="3"  class="tableBd">
			<br><i>For ING employees</i>
		</td>
	</tr>
',sysdate(),1,sysdate(),1);
Insert into VAR_HTMLTEXT (VHT_PAGENAME,VHT_NUMBER,VHT_HTMLTEXT,VHT_CREATEDON,VHT_CREATEDBYID,VHT_CHANGEDON,VHT_CHANGEDBYID) values ('proxy_scheme_nn.jsp',16,'d bgcolor="#ffffff"  class="tableBdalt" valign="top">B</td>
				<td bgcolor="#ffffff"  class="tableBdalt" valign="top">
					<table  border="0" cellspacing="0" cellpadding="0">
					<tr><td class="tableBdalt">B + S</td></tr>
					<tr><td class="tableBdalt">B + A</td></tr>
					</table>
				</td>
				<td bgcolor="#ffffff"  class="tableBdalt" valign="top">
					<table  border="0" cellspacing="0" cellpadding="0">
					<tr><td class="tableBdalt">B + S</td><td class="tableBdalt">&nbsp;</td></tr>
					<tr><td class="tableBdalt">B + A</td><td class="tableBdalt">&nbsp;</td></tr>
					</table>
				</td>
				<td bgcolor="#ffffff"  class="tableBdalt" valign="top">
					 <table border="0" cellspacing="0" cellpadding="0">
						<tr><td class="tableBdalt" >B max EUR 1 million</td><td class="tableBdalt">&nbsp;</td></tr>
					 </table>
				</td>
			</tr>
			<tr>
				<td bgcolor="#ffffff"  class="tableBdalt" valign="top">C1</td>
				<td bgcolor="#ffffff"  class="tableBdalt" valign="top">&nbsp;</td>
				<td bgcolor="#ffffff"  class="tableBdalt" valign="top">
					<table  border="0" cellspacing="0" cellpadding="0">
						<tr><td class="tableBdalt">C1 + S</td><td class="tableBdalt">&nbsp;</td></tr>
						<tr><td class="tableBdalt">C1 + A</td><td class="tableBdalt">&nbsp;</td></tr>
						<tr><td class="tableBdalt">C1 + C1</td><td class="tableBdalt">&nbsp;</td></tr>
						<tr><td class="tableBdalt">C1 + C2</td><td class="tableBdalt">&nbsp;max. EUR 5 million</td></tr>
						<tr><td class="tableBdalt">C1 + D</td><td class="tableBdalt">&nbsp;max. EUR 1 million</td></tr>
					</table>
				</td>
				<td bgcolor="#ffffff"  class="tableBdalt" valign="top">
					 <table border="0" cellspacing="0" cellpadding="0">
						<tr><td class="tableBdalt" >C1 max EUR ',sysdate(),1,sysdate(),1);
Insert into VAR_HTMLTEXT (VHT_PAGENAME,VHT_NUMBER,VHT_HTMLTEXT,VHT_CREATEDON,VHT_CREATEDBYID,VHT_CHANGEDON,VHT_CHANGEDBYID) values ('proxy_scheme_nn.jsp',17,'1 million</td><td class="tableBdalt">&nbsp;</td></tr>
					 </table>
				</td>
			</tr>
			<tr>
				<td bgcolor="#ffffff"  class="tableBdalt" valign="top">C2</td>
				<td bgcolor="#ffffff"  class="tableBdalt" valign="top">&nbsp;</td>
				<td bgcolor="#ffffff"  class="tableBdalt" valign="top">
					<table  border="0" cellspacing="0" cellpadding="0">
						<tr><td class="tableBdalt">C2 + S</td><td class="tableBdalt">&nbsp;max. EUR 5 million</td></tr>
						<tr><td class="tableBdalt">C2 + A</td><td class="tableBdalt">&nbsp;max. EUR 5 million</td></tr>
						<tr><td class="tableBdalt">C2 + C1</td><td class="tableBdalt">&nbsp;max. EUR 5 million</td></tr>
						<tr><td class="tableBdalt">C2 + C2</td><td class="tableBdalt">&nbsp;max. EUR 5 million</td></tr>
						<tr><td class="tableBdalt">C2 + D</td><td class="tableBdalt">&nbsp;max. EUR 1 million</td></tr>
					</table>
				</td>
				<td bgcolor="#ffffff"  class="tableBdalt" valign="top">
					 <table border="0" cellspacing="0" cellpadding="0">
						<tr><td class="tableBdalt" >C2 max EUR 1 million</td><td class="tableBdalt">&nbsp;</td></tr>
					 </table>
				</td>
			</tr>
			<tr>
				<td bgcolor="#ffffff"  class="tableBdalt" valign="top">D</td>
				<td bgcolor="#ffffff"  class="tableBdalt" valign="top">&nbsp;</td>
				<td bgcolor="#ffffff"  class="tableBdalt" valign="top">
					<table  border="0" cellspacing="0" cellpadding="0">
						<tr><td class="tableBdalt">D + S</td><td class="tableBdalt">&nbsp;max. EUR 1 million</td></tr>
						<tr><td class="tableBdalt">D + A</td><td class="tableBdalt">&nbsp;max. EUR 1 million</td></tr>
						<tr><td class="tableBdalt">D + C1</td><td class="tableBdalt">&nbsp;max. EUR 1 million</td></tr>
						<tr><td class="tableBdalt">D + C2</td><td class="tableBda',sysdate(),1,sysdate(),1);
Insert into VAR_HTMLTEXT (VHT_PAGENAME,VHT_NUMBER,VHT_HTMLTEXT,VHT_CREATEDON,VHT_CREATEDBYID,VHT_CHANGEDON,VHT_CHANGEDBYID) values ('proxy_scheme_nn.jsp',15,'td class="tableBdalt">S + C2</td><td class="tableBdalt">&nbsp;max. EUR 5 million</td></tr>
						<tr><td class="tableBdalt">S + D</td><td class="tableBdalt">&nbsp;max. EUR 1 million</td></tr>
					 </table>
				</td>
				<td bgcolor="#ffffff"  class="tableBdalt" valign="top">
					 <table border="0" cellspacing="0" cellpadding="0">
						<tr><td class="tableBdalt" >S max EUR 1 million</td><td class="tableBdalt">&nbsp;</td></tr>
					 </table>
				</td>
			</tr>
			<tr>
				<td bgcolor="#ffffff"  class="tableBdalt" valign="top">A</td>
				<td bgcolor="#ffffff"  class="tableBdalt" valign="top">
					<table  border="0" cellspacing="0" cellpadding="0">
					<tr><td class="tableBdalt">A + S</td></tr>
					<tr><td class="tableBdalt">A + A</td></tr>
					<tr><td class="tableBdalt">A + B</td></tr>
					</table>
				</td>
				<td bgcolor="#ffffff"  class="tableBdalt" valign="top">
					<table  border="0" cellspacing="0" cellpadding="0">
					<tr><td class="tableBdalt">A + S</td><td class="tableBdalt">&nbsp;</td></tr>
					<tr><td class="tableBdalt">A + A</td><td class="tableBdalt">&nbsp;</td></tr>
					<tr><td class="tableBdalt">A + B</td><td class="tableBdalt">&nbsp;</td></tr>
					<tr><td class="tableBdalt">A + C1</td><td class="tableBdalt">&nbsp;</td></tr>
					<tr><td class="tableBdalt">A + C2</td><td class="tableBdalt">&nbsp;max. EUR 5 million</td></tr>
					<tr><td class="tableBdalt">A + D</td><td class="tableBdalt">&nbsp;max. EUR 1 million</td></tr>
					</table>
				</td>
				<td bgcolor="#ffffff"  class="tableBdalt" valign="top">
					 <table border="0" cellspacing="0" cellpadding="0">
						<tr><td class="tableBdalt" >A max EUR 1 million</td><td class="tableBdalt">&nbsp;</td></tr>
					 </table>
				</td>
			</tr>
			<tr>
				<t',sysdate(),1,sysdate(),1);
Insert into VAR_HTMLTEXT (VHT_PAGENAME,VHT_NUMBER,VHT_HTMLTEXT,VHT_CREATEDON,VHT_CREATEDBYID,VHT_CHANGEDON,VHT_CHANGEDBYID) values ('proxy_scheme_nn.jsp',13,', provided that he acts jointly with a category
		S, A, C1 or C2 attorney, as applicable, the power to
		delegate the power of representation that he has with
		that attorney to a third party, provided that the
		substitution occurs in respect of a concrete transaction
		which is specifically and accurately described in writing
		in the authorisation concerning the substitution and is
		intended solely to enable the third party to appear on
		behalf of the company in order to execute an instrument
		before a civil-law notary in respect of the above-
		mentioned concrete transaction, to which third party
		independent authority may be granted for the company with
		respect to the aforementioned transaction.
	</td></tr>

	<tr><td  class="tableBdAlt" colspan="3"><br><A NAME="type_d"></A>
	<B>Category D attorney:</B> officer having special power to
	represent the company;
	</td></tr>
	<tr><td width="10" class="tableBdalt" valign="top">-</td>
	<td colspan="2" class="tableBdalt" valign="top">
		empowered to perform the legal transactions referred
	  to in block 2, provided that he acts jointly with a
	  category S, A, C1 or C2 attorney, provided the amount
	  involved does not exceed EUR 1,000,000;
	</td></tr>
	<tr><td width="10" class="tableBdalt" valign="top">-</td>
	<td colspan="2" class="tableBdalt" valign="top">
		empowered to perform the legal transactions referred
	  to in block 3, provided the amount involved does not
	  exceed EUR 1,000,000.
	</td></tr>
</table>

<!-- Pagebreak so close and reopen outer table  -->
</td></tr></table>
<PAGE_MARKER_SIGNATUREBOOK>
<PAGE_MARKER_SIGNATUREBOOK>
<div class="prspagebreakafter">&nbsp;</div>

<table width="620" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td width="20" class="ta',sysdate(),1,sysdate(),1);
Insert into VAR_HTMLTEXT (VHT_PAGENAME,VHT_NUMBER,VHT_HTMLTEXT,VHT_CREATEDON,VHT_CREATEDBYID,VHT_CHANGEDON,VHT_CHANGEDBYID) values ('proxy_scheme_nn.jsp',14,'bleBdalt" valign="top">&nbsp;</td>
		<td width="600" class="tableBdalt">
		<!-- end classification and start of the schema -->
		<table width="600" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td width="600" class="tableBdalt" align="middle">&nbsp;</div><br><br><div class="header">
				matrix for standardised power-of-attorney regulations at</div>
				</td>
			</tr>
			<tr>
				<td  width="600" class="tableBdalt" align="middle"><div class="header">
				ING Bank N.V.</div><br><br>
				</td>
			</tr>
		</table>
		<!-- begin matrix -->
		<table width="600" border="0" cellspacing="1" cellpadding="4" bgcolor="#000000">
			<tr>
				<td width="100" class="tableHd" bgcolor="#c2dbf8" >Category</td>
				<td width="100" class="tableHd" bgcolor="#c2dbf8">Block 1</td>
				<td width="200" class="tableHd" bgcolor="#c2dbf8">Block 2</td>
				<td width="200" class="tableHd" bgcolor="#c2dbf8">Block 3</td>
			</tr>
			<tr>
				<td bgcolor="#ffffff"  class="tableBdalt" valign="top">S</td>
				<td bgcolor="#ffffff"  class="tableBdalt" valign="top">
					<table  border="0" cellspacing="0" cellpadding="0">
		      			<tr><td class="tableBdalt">S + S</td></tr>
		      			<tr><td class="tableBdalt">S + A</td></tr>
		      			<tr><td class="tableBdalt">S + B</td></tr>
		  			</table>
				</td>
				<td bgcolor="#ffffff"  class="tableBdalt" valign="top">
					 <table border="0" cellspacing="0" cellpadding="0">
						<tr><td class="tableBdalt" >S + S</td><td class="tableBdalt">&nbsp;</td></tr>
						<tr><td class="tableBdalt">S + A</td><td class="tableBdalt">&nbsp;</td></tr>
						<tr><td class="tableBdalt">S + B</td><td class="tableBdalt">&nbsp;</td></tr>
						<tr><td class="tableBdalt">S + C1</td><td class="tableBdalt">&nbsp;</td></tr>
						<tr><',sysdate(),1,sysdate(),1);
Insert into VAR_HTMLTEXT (VHT_PAGENAME,VHT_NUMBER,VHT_HTMLTEXT,VHT_CREATEDON,VHT_CREATEDBYID,VHT_CHANGEDON,VHT_CHANGEDBYID) values ('proxy_scheme_nn.jsp',12,'rd party to appear on
		behalf of the company in order to execute an instrument
		before a civil-law notary in respect of the above-
		mentioned concrete transaction, to which third party
		independent authority may be granted for the company with
		respect to the aforementioned transaction.
	</td></tr>

</table>
</td></tr></table>

<table width="620" border="0" cellspacing="0" cellpadding="0">
<tr>  <td width="20" class="tableBdalt" valign="top">&nbsp;</td> <td width="600" class="tableBdalt">
<!-- end classification and start of the schema -->
<table width="600" border="0" cellspacing="0" cellpadding="0">

	<tr><td  class="tableBdAlt" colspan="3"><br><A NAME="type_c2"></A>
	<B>Category C2 attorney:</B> officer having special power to
	represent the company;
	</td></tr>
<tr><td width="10" class="tableBdalt" valign="top">-</td>
	<td colspan="2" class="tableBdalt" valign="top">
		empowered to perform the legal transactions referred
  		to in block 2:
	</td></tr>
	<tr><td width="10" class="tableBdalt" valign="top">-</td>
	<td colspan="2" class="tableBdalt" valign="top">
		jointly with a category S, A, C1 or C2 attorney,
	  provided the amount involved does not exceed EUR 5,000,000,
	  or
	</td></tr>
	<tr><td width="10" class="tableBdalt" valign="top">-</td>
	<td colspan="2" class="tableBdalt" valign="top">
		jointly with a category D attorney, provided the
  		amount involved does not exceed EUR 1,000,000;
	</td></tr>
	<tr><td width="10" class="tableBdalt" valign="top">-</td>
	<td colspan="2" class="tableBdalt" valign="top">
		empowered to perform the legal transactions referred to
	  in block 3, provided the amount involved does not
	  exceed EUR 1,000,000;
	</td></tr>

	<tr><td  class="tableBdAltIta" colspan="3">
		including',sysdate(),1,sysdate(),1);
Insert into VAR_HTMLTEXT (VHT_PAGENAME,VHT_NUMBER,VHT_HTMLTEXT,VHT_CREATEDON,VHT_CREATEDBYID,VHT_CHANGEDON,VHT_CHANGEDBYID) values ('applicationinnewform.jsp',0,'<table width="500" border="0" cellspacing="1" cellpadding="4" bgcolor="#ffffff">
<tr><td class="header">
Request for a new proxy
</td></tr>
<tr>
  <td class="tableText">
    Please enter the necessary data for a new proxy<br>
    and click on ok.
  </td>
  <td class="prserror">
       <span>
          <img src="images/prs_error_exclamation.gif" width="16" height="16" vspace="0" hspace="0" border="0" alt="" />
          <b>&nbsp;scroll down&nbsp;</b>
          <img src="images/prs_error_exclamation.gif" width="18" height="18" vspace="0" hspace="0" border="0" alt="" />
       </span>
  </td>
</tr>
</table>
<BR>',sysdate(),1,sysdate(),1);

commit;