data "aws_vpc" "selected" {
  tags {
    Owner = "aws-team"
  }
}

data "aws_subnet_ids" "trusted" {
  vpc_id = "${data.aws_vpc.selected.id}"

  tags {
    Zone = "trusted"
  }
}

data "aws_subnet_ids" "data" {
  vpc_id = "${data.aws_vpc.selected.id}"

  tags {
    Zone = "data"
  }
}

data "aws_security_group" "tf_mgmt_linux_master" {
  vpc_id = "${data.aws_vpc.selected.id}"

  tags {
    Name = "tf-mgmt-linux-master"
  }
}

data "aws_caller_identity" "current" {}

data "aws_security_group" "allow-ssh-from-bastion" {
  name = "allow-ssh-from-bastion"
}

data "aws_route53_zone" "current" {
  name         = "${var.env}.keshavkummari.com"
  private_zone = true
}

data "aws_acm_certificate" "certificate" {
  domain = "*.${var.env}.keshavkummari.com"
}
