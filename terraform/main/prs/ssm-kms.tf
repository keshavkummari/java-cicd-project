# SRL_R-13

resource "aws_kms_key" "ssm_kms_key" {
  description             = "${local.aws_kms_key_ssm_kms_key_description}"
  enable_key_rotation     = true
  deletion_window_in_days = 7

  lifecycle {
    prevent_destroy = false
  }

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Id": "${local.aws_kms_key_ssm_kms_key_description}-key-policy",
  "Statement": [{
    "Sid": "Allow access for Key Administrators",
    "Effect": "Allow",
    "Principal": {"AWS":
      ${jsonencode(concat(local.aws_kms_key_ssm_kms_key_allowed_roles))}
    },
    "Action": [
      "kms:*"
    ],
    "Resource": "*"
  }]
}
EOF
}
