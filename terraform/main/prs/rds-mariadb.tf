############################################
#          PARAMETER GROUP VARIABLES       #
############################################

resource "aws_db_parameter_group" "rds_parameter_group" {
  name_prefix = "${local.aws_db_instance_rds_mariadb_instance_name}"
  family      = "mariadb10.2"

  # The parameters below were provided by the RDS team

  parameter {
    name  = "character_set_server"
    value = "utf8mb4"
  }
  parameter {
    name  = "character_set_client"
    value = "utf8mb4"
  }
  parameter {
    name  = "character_set_database"
    value = "utf8mb4"
  }
  parameter {
    name  = "sql_mode"
    value = "strict_all_tables,no_auto_create_user"
  }
  parameter {
    name  = "local_infile"
    value = 0
  }

  # End of parameters provided by RDS team


  # The parameters below are necessary for the nnprs_schema.sql script to work

  parameter {
    name         = "lower_case_table_names"
    value        = 1
    apply_method = "pending-reboot"
  }
  parameter {
    name         = "log_bin_trust_function_creators"
    value        = 1
    apply_method = "pending-reboot"
  }
  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_db_option_group" "rds_optiongroup" {
  name_prefix          = "${local.aws_db_instance_rds_mariadb_instance_name}"
  engine_name          = "${var.engine_name}"
  major_engine_version = "${var.og_engine_version}"

  option {
    option_name = "MARIADB_AUDIT_PLUGIN"

    option_settings {
      name  = "SERVER_AUDIT_EVENTS"
      value = "CONNECT,QUERY"
    }

    option_settings {
      name  = "SERVER_AUDIT_FILE_SIZE"
      value = 1000000
    }

    option_settings {
      name  = "SERVER_AUDIT_FILE_ROTATION"
      value = 10
    }
  }

  timeouts {
    delete = "45m"
  }

  lifecycle {
    create_before_destroy = true
  }
}

############################################
#            RDS INSTANCE VARIABLES        #
############################################

resource "aws_db_instance" "rds_mariadb" {
  identifier           = "${local.aws_db_instance_rds_mariadb_instance_name}"
  allocated_storage    = "${var.mariadb_allocated_storage}"
  storage_type         = "gp2"
  engine               = "${var.engine_name}"
  engine_version       = "${var.mariadb_engine_version}"
  instance_class       = "${var.mariadb_instance_class}"
  name                 = "${local.aws_db_instance_rds_mariadb_database_name}"
  username             = "prsnn"
  password             = "prsnnpassword"                                      # To do: randomly generate password (SRL_1.28)
  db_subnet_group_name = "tf-platform-data-eu-west-1-master"
  parameter_group_name = "${aws_db_parameter_group.rds_parameter_group.name}"
  option_group_name    = "${aws_db_option_group.rds_optiongroup.name}"

  vpc_security_group_ids = ["${aws_security_group.prs_db.id}"]
  storage_encrypted      = true                                 # Encryption - Personal Identifying Data (SRL_R-12)
  kms_key_id             = "${aws_kms_key.rds_kms_key.arn}"
  publicly_accessible    = false
  skip_final_snapshot    = "${var.mariadb_skip_final_snapshot}"

  final_snapshot_identifier  = "finalsnapshot-${var.uuid}"
  auto_minor_version_upgrade = true
  backup_retention_period    = 14                          # Best practice
  backup_window              = "02:15-02:45"

  multi_az = "${var.production}" # MOT (SRL_R-40 SRL_R-42)

  depends_on = ["aws_db_option_group.rds_optiongroup", "aws_db_parameter_group.rds_parameter_group"]
}
