resource "aws_route53_record" "www" {
  zone_id = "${data.aws_route53_zone.current.zone_id}"
  name    = "${local.aws_route53_prs_dns_name}.${data.aws_route53_zone.current.name}"
  type    = "A"

  alias {
    name                   = "${aws_elb.hof_prs_elb.dns_name}"
    zone_id                = "${aws_elb.hof_prs_elb.zone_id}"
    evaluate_target_health = false
  }
}
