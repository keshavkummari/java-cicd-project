#!/bin/bash
#Checking out if the database actually exists using the SSL connection.
#This is done because if the database has been already created it enforces SSL transport security.
#SSL CA certificate has been downloaded by Ansible in the Packer provisioning step.

target_database_tables_count=$(mysql --host=${DATABASE_ENDPOINT} --user=${DATABASE_USER} --password='${DATABASE_PASSWORD}' --ssl-ca=/usr/share/tomcat/conf/rds-combined-ca-bundle.pem -s -N -e "SELECT COUNT(*) FROM information_schema.tables WHERE table_schema = '${DATABASE_NAME}';");

if [ "$target_database_tables_count" -eq "0" ];
    then
        echo "The database ${DATABASE_NAME} has not been provisioned yet. Creating initial schema.";
        mysql --host=${DATABASE_ENDPOINT} --user=${DATABASE_USER} --password='${DATABASE_PASSWORD}' --ssl-ca=/usr/share/tomcat/conf/rds-combined-ca-bundle.pem < /tmp/nnprs_schema.sql | tee -a /tmp/exec-log.log;

        echo "Populating initial data.";
        mysql --host=${DATABASE_ENDPOINT} --user=${DATABASE_USER} --password='${DATABASE_PASSWORD}' --ssl-ca=/usr/share/tomcat/conf/rds-combined-ca-bundle.pem < /tmp/nn_prs_initital_data.sql | tee -a /tmp/exec-log.log;

        echo "Applying security controls.";
        mysql --host=${DATABASE_ENDPOINT} --user=${DATABASE_USER} --password='${DATABASE_PASSWORD}' --ssl-ca=/usr/share/tomcat/conf/rds-combined-ca-bundle.pem < /tmp/mysql-security-controls-query.sql --ssl-ca=/usr/share/tomcat/conf/rds-combined-ca-bundle.pem| tee -a /tmp/exec-log.log;

        echo "Database ${DATABASE_NAME} provisioning completed." | tee -a /tmp/exec-log.log;
    else
        echo "Database ${DATABASE_NAME} has been previously provisioned, skipping initial provisioning.";
fi

# Hara-kiri (since instance_initiated_shutdown_behavior = "terminate")
sudo shutdown now
