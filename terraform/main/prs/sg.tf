resource "aws_security_group" "prs_elb" {
  name_prefix = "${local.aws_security_group_prs_elb_name}"

  vpc_id = "${data.aws_vpc.selected.id}"

  tags {
    Name  = "${local.aws_security_group_prs_elb_name}"
    Owner = "keshav.kummari@gmail.com"
  }
}

resource "aws_security_group" "prs_app" {
  name_prefix = "${local.aws_security_group_prs_app_name}"

  vpc_id = "${data.aws_vpc.selected.id}"

  tags {
    Name  = "${local.aws_security_group_prs_app_name}"
    Owner = "keshav.kummari@gmail.com"
  }
}

resource "aws_security_group" "prs_db" {
  name_prefix = "${local.aws_security_group_prs_db_name}"
  vpc_id      = "${data.aws_vpc.selected.id}"

  tags {
    Name  = "${local.aws_security_group_prs_db_name}"
    Owner = "keshav.kummari@gmail.com"
  }
}

resource "aws_security_group_rule" "elb_ingress_https" {
  security_group_id = "${aws_security_group.prs_elb.id}" # Encryption - Data transmission - Internal NN - Closed Network (SRL_R-3, SRL_1.13)

  type        = "ingress"
  from_port   = 443
  to_port     = 443
  protocol    = "tcp"
  cidr_blocks = ["0.0.0.0/0"]
}

resource "aws_security_group_rule" "elb_egress_app" {
  security_group_id = "${aws_security_group.prs_elb.id}" # Encryption - Data transmission - Internal NN - Closed Network (SRL_R-3, SRL_1.13)

  type                     = "egress"
  from_port                = 8443
  to_port                  = 8443
  protocol                 = "tcp"
  source_security_group_id = "${aws_security_group.prs_app.id}"
}

resource "aws_security_group_rule" "app_ingress_elb" {
  security_group_id = "${aws_security_group.prs_app.id}" # Encryption - Data transmission - Internal NN - Closed Network (SRL_R-3, SRL_1.13)

  type                     = "ingress"
  from_port                = 8443
  to_port                  = 8443
  protocol                 = "tcp"
  source_security_group_id = "${aws_security_group.prs_elb.id}"
}

resource "aws_security_group_rule" "app_egress_db" {
  security_group_id = "${aws_security_group.prs_app.id}" # Encryption - Data transmission - Internal NN - Closed Network (SRL_R-3, SRL_1.13)

  type                     = "egress"
  from_port                = 3306
  to_port                  = 3306
  protocol                 = "tcp"
  source_security_group_id = "${aws_security_group.prs_db.id}"
}

resource "aws_security_group_rule" "db_ingress_app" {
  security_group_id = "${aws_security_group.prs_db.id}" # Encryption - Data transmission - Internal NN - Closed Network (SRL_R-3, SRL_1.13)

  type                     = "ingress"
  from_port                = 3306
  to_port                  = 3306
  protocol                 = "tcp"
  source_security_group_id = "${aws_security_group.prs_app.id}"
}
