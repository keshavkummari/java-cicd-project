resource "aws_ssm_parameter" "database_username" {
  name       = "/applications/prs/${var.env}/${var.uuid}/database/username"
  type       = "SecureString"
  value      = "${aws_db_instance.rds_mariadb.username}"
  key_id     = "${aws_kms_key.ssm_kms_key.id}"
  depends_on = ["aws_kms_key.ssm_kms_key"]
  overwrite  = true
}

resource "aws_ssm_parameter" "database_password" {
  name   = "/applications/prs/${var.env}/${var.uuid}/database/password"
  type   = "SecureString"                                               # Any passwords stored in database tables, configuration files, automatic login scripts, batch files and software macros should be encrypted or hashed (one-way encrypted) (SRL_2.7)
  value  = "${aws_db_instance.rds_mariadb.password}"
  key_id = "${aws_kms_key.ssm_kms_key.id}"

  depends_on = ["aws_kms_key.ssm_kms_key"] # SRL_2.7
  overwrite  = true
}

resource "aws_ssm_parameter" "database_connectionstring" {
  name   = "/applications/prs/${var.env}/${var.uuid}/database/connectionstring"
  type   = "SecureString"
  value  = "jdbc:mariadb://${aws_db_instance.rds_mariadb.address}:3306/${aws_db_instance.rds_mariadb.name}?useSSL=true&serverSslCert=/usr/share/tomcat/conf/rds-combined-ca-bundle.pem"
  key_id = "${aws_kms_key.ssm_kms_key.id}"

  depends_on = ["aws_kms_key.ssm_kms_key"]
  overwrite  = true
}
