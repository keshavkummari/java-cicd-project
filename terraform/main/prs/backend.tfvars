#bucket = "tf-state-storage-hof-prs-app-${var.env}"
bucket = "tf-state-storage-hof-prs-app-tst"
region     = "ap-south-1"
encrypt    = true
kms_key_id = "alias/tf-state-hofprs-app-tst"
#kms_key_id = "alias/tf-state-hofprs-app-${var.env}"
