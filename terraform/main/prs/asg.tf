data "template_file" "prs-env" {
  template = "${file("prs.env.tpl")}"

  vars {
    ENVIRONMENT = "${var.env}"
    UUID        = "${var.uuid}"
  }
}

resource "aws_launch_configuration" "hof_prs_asg_conf" {
  name_prefix          = "${local.aws_launch_configuration_hof_prs_asg_conf_name_prefix}"
  iam_instance_profile = "arn:aws:iam::875428837402:instance-profile/"
  image_id             = "${var.pckr_ami_id}"                                                                                                                               # The infrastructure building blocks that are used by the application should comply to the NN policy (SRL_2.3)
  instance_type        = "t2.micro"
  security_groups      = ["${aws_security_group.prs_app.id}", "${data.aws_security_group.allow-ssh-from-bastion.id}", "${data.aws_security_group.tf_mgmt_linux_master.id}"]
  key_name             = "hof_dev"
  user_data            = "${data.template_file.prs-env.rendered}"

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_autoscaling_group" "hof_prs_asg" {
  name                 = "${local.aws_autoscaling_group_hof_prs_asg_name}"
  launch_configuration = "${aws_launch_configuration.hof_prs_asg_conf.name}"

  vpc_zone_identifier = ["${data.aws_subnet_ids.trusted.ids}"]

  load_balancers = ["${aws_elb.hof_prs_elb.name}"]
  min_size       = 1
  max_size       = 1

  tag {
    key                 = "Name"
    propagate_at_launch = true
    value               = "${local.aws_autoscaling_group_hof_prs_asg_name}"
  }

  lifecycle {
    create_before_destroy = true
  }

  depends_on = ["aws_db_instance.rds_mariadb"]
}
