############################################
# ENVIRONMENT (ACCOUNT) SPECIFIC VARIABLES #
############################################

variable "pckr_ami_id" {
  description = "packer ami id"
  type        = "string"
  default     = "ami-76d6f519"
}

variable "env" {
  description = "Name of the environment"
  type        = "string"
}

#variable "subnet_platform_trusted_az_a" {
#  description = "Subnet in Availability Zone A"
#  type        = "string"
#}

#variable "subnet_platform_trusted_az_b" {
#  description = "Subnet in Availability Zone B"
#  type        = "string"
#}

#variable "subnet_platform_trusted_az_c" {
#  description = "Subnet in Availability Zone C"
#  type        = "string"
#}
######################
# INSTANCE VARIABLES #
######################

variable "instance_type" {
  description = "Instance size"
  type        = "string"
  default     = "t2.small"
}

variable "instance_iam_profile" {
  description = "Instance profile arn"
  type        = "string"
  default     = "arn:aws:iam::875428837402:instance-profile/"
}

# This gets solved by the datasource. If you prefer to use the pipeline variables for this, comment this in, and set the ami id using TF_VAR_ami="somekindofvalue"
// variable "ami" {
//   description = "AMI id of the latest AMI build from Packer"
//   type        = "string"
// }

#variable "instance_max" {
#  description = "Max instances parameter for auto-scaling group"
#  default     = "0"
#}

#variable "instance_min" {
#  description = "Min instances parameter for auto-scaling group"
#  default     = "0"
#}

#variable "instance_wait_elb" {
#  description = "Number of instances must be in service on elb before completing"
#  default     = "0"
#}
# 0 = to turned off, 1 is turned on  # Terraform does not support boolean, but convert false to 0 and true to 1. Using count parameter at the resource, you can switch resource on and off.
# For more info refer to https://www.terraform.io/docs/configuration/variables.html
variable "schedule_nightly_shutdown" {
  description = "Turns on or off nightly shutdown to save costs"
  default     = false
}

####################
# ACCESS FILTERING #
####################

#variable "elb_src_cidrs" {
#  description = "IP ranges in CIDR format to define who can access the application"
#  type        = "list"
#}

####################
# KMS Variables    #
####################

variable kms-usage-arns {
  description = "list of role arns that will have access to s3 and kms"
  type        = "list"
  default     = ["arn:aws:iam::875428837402:role/gitlab-shared-runners-manager-4.gitlab.com", "arn:aws:iam::875428837402:role/Admin", "arn:aws:iam::875428837402:role/Engineer"]
}

variable "uuid" {
  type = "string"
}

###############  General variables ####################

variable "db_subnet_group" {
  description = "Provide the database subnet group"
  type        = "string"
  default     = "tf-platform-data-eu-west-1-master"
}

variable "region" {
  description = "Provide the region in which your instance must be deployed"
  type        = "string"
  default     = "eu-west-1"
}

variable "role_name" {
  description = "Provide the role that creates the instance"
  type        = "string"
  default     = "arn:aws:iam::875428837402:role/"
}

variable "account_id" {
  description = "Provide the account number in which you deploy"
  type        = "string"
  default     = "875428837402"
}

variable "engine_name" {
  description = "Provide your rds instance (e.g. mariadb)"
  type        = "string"
  default     = "mariadb"
}

###############  Security group variables ####################

#variable "sg_ingress_cidr" {
#  description = "Provide CIDR for incoming connections to RDS"
#  type        = "string"
#}

###############  Parameter group variables ####################

variable "pg_engine_family" {
  description = "Provide the current family"
  type        = "string"
  default     = "mariadb10.2"
}

###############  Option group variables ####################

variable "og_engine_version" {
  description = "Provide your RDS instance version number"
  type        = "string"
  default     = "10.2"
}

###############  Mariadb instance variables ####################

variable "mariadb_allocated_storage" {
  description = "Set the storaged needed (in GB)"
  type        = "string"
  default     = "20"
}

variable "mariadb_engine_version" {
  description = "Provide the mariadb engine versions"
  type        = "string"
  default     = "10.2.11"
}

variable "mariadb_instance_class" {
  description = "Provide the instance compute class"
  type        = "string"
  default     = "db.t2.small"
}

variable "mariadb_username" {
  description = "Provide a name for the root user (only alphanumeric)"
  type        = "string"
  default     = "prsnn"
}

variable "mariadb_user_pw" {
  description = "Provide a password for the root user, during terraform apply provide a pw"
  type        = "string"
  default     = "prsnnpassword"
}

variable "mariadb_skip_final_snapshot" {
  description = "Make final snapshot when RDS instance is terminated"
  type        = "string"
  default     = "true"
}

variable "mariadb_backup_window" {
  description = "Provide a backup window (optional). If you don't want a backup window: delete in mariadb.tf"
  type        = "string"
  default     = "02:15-02:45"
}

variable "shell_script" {
  description = "A shell script template that will be run from the ephemeral instanceRDS database name"
  type        = "string"
  default     = "bootstrap.sh.tpl"
}

variable "sql_script" {
  description = "A SQL script that will be run from the ephemeral instance against a MySQL/Aurora RDS DB"
  type        = "string"
  default     = "bootstrap.sql.tpl"
}

variable "production" {
  description = "Switch to indicate if the target environment is production environment."
  type        = "string"
  default     = false
}
