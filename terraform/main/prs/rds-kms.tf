# Encryption - Key Management (SRL_R-13)

resource "aws_kms_key" "rds_kms_key" {
  description             = "${local.aws_kms_key_rds_kms_key_description}"
  enable_key_rotation     = true
  deletion_window_in_days = 7

  lifecycle {
    prevent_destroy = false
  }

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Id": "${local.aws_kms_key_rds_kms_key_description}-key-policy",
  "Statement": [{
    "Sid": "Allow access for Key Administrators",
    "Effect": "Allow",
    "Principal": {"AWS":
      ${jsonencode(concat(local.aws_kms_key_rds_kms_key_allowed_roles))}
    },
    "Action": [
      "kms:Create*",
      "kms:Describe*",
      "kms:Enable*",
      "kms:List*",
      "kms:Put*",
      "kms:Update*",
      "kms:Revoke*",
      "kms:Disable*",
      "kms:Get*",
      "kms:Delete*",
      "kms:ScheduleKeyDeletion",
      "kms:CancelKeyDeletion"
    ],
    "Resource": "*"
  }]
}
EOF
}

resource "aws_kms_alias" "rds_kms_key_alias" {
  name_prefix   = "alias/${local.aws_kms_key_rds_kms_key_description}"
  target_key_id = "${aws_kms_key.rds_kms_key.key_id}"
}
