# Prepare MySQL script
data "template_file" "mysql_script" {
  template = "${file("${path.module}/${var.sql_script}")}"

  vars {
    DATABASE_NAME = "${aws_db_instance.rds_mariadb.name}"
    DATABASE_USER = "${aws_db_instance.rds_mariadb.username}"
  }

  depends_on = ["aws_db_instance.rds_mariadb"]
}

# Bootstrap script
data "template_file" "user_data" {
  template = "${file("${path.module}/bootstrap.sh.tpl")}"

  vars {
    DATABASE_ENDPOINT = "${aws_db_instance.rds_mariadb.address}"
    DATABASE_NAME     = "${aws_db_instance.rds_mariadb.name}"
    DATABASE_USER     = "${aws_db_instance.rds_mariadb.username}"
    DATABASE_PASSWORD = "${aws_db_instance.rds_mariadb.password}"
    MYSQL_SCRIPT      = "${data.template_file.mysql_script.rendered}"
  }

  depends_on = ["aws_db_instance.rds_mariadb"]
}

data "template_file" "nnprs_schema" {
  template = "${file("${path.module}/templates/sql/nnprs_schema.sql.tpl")}"

  vars {
    DATABASE_NAME = "${aws_db_instance.rds_mariadb.name}"
  }

  depends_on = ["aws_db_instance.rds_mariadb"]
}

data "template_file" "nnprs_initital_data" {
  template = "${file("${path.module}/templates/sql/nnprs_initital_data.sql.tpl")}"

  vars {
    DATABASE_NAME = "${aws_db_instance.rds_mariadb.name}"
  }

  depends_on = ["aws_db_instance.rds_mariadb"]
}

resource "aws_instance" "ephemeral_instance" {
  subnet_id              = "${data.aws_subnet_ids.trusted.ids[0]}"
  instance_type          = "${var.instance_type}"
  iam_instance_profile   = "tf-glrunner-7933ca0be-master"
  ami                    = "${var.pckr_ami_id}"
  vpc_security_group_ids = ["${aws_security_group.prs_app.id}", "${data.aws_security_group.tf_mgmt_linux_master.id}", "${aws_security_group.allow_ssh_from_runner.id}"]

  #user_data              = "${data.template_file.user_data.rendered}"
  key_name = "hof_dev"
  tags     = "${map("Name", format("%s-RDS_BOOSTRAP_EPHEMERAL_INSTANCE", var.uuid))}"

  # Terminate instance on shutdown
  instance_initiated_shutdown_behavior = "terminate"

  provisioner "file" {
    connection {
      user        = "nn-admin"
      private_key = "${file("${path.module}/hof_dev")}"
    }

    content     = "${data.template_file.user_data.rendered}"
    destination = "/tmp/bootstrap.sh"
  }

  provisioner "file" {
    connection {
      user        = "nn-admin"
      private_key = "${file("${path.module}/hof_dev")}"
    }

    content     = "${data.template_file.nnprs_schema.rendered}"
    destination = "/tmp/nnprs_schema.sql"
  }

  provisioner "file" {
    connection {
      user        = "nn-admin"
      private_key = "${file("${path.module}/hof_dev")}"
    }

    content     = "${data.template_file.nnprs_initital_data.rendered}"
    destination = "/tmp/nn_prs_initital_data.sql"
  }

  provisioner "file" {
    connection {
      user        = "nn-admin"
      private_key = "${file("${path.module}/hof_dev")}"
    }

    content     = "${data.template_file.mysql_script.rendered}"
    destination = "/tmp/mysql-security-controls-query.sql"
  }

  provisioner "remote-exec" {
    connection {
      user        = "nn-admin"
      private_key = "${file("${path.module}/hof_dev")}"
    }

    inline = [
      "chmod +x /tmp/bootstrap.sh",
      "/tmp/bootstrap.sh",
    ]
  }

  root_block_device {
    volume_type           = "gp2"
    volume_size           = "128"
    delete_on_termination = "true"
  }

  volume_tags = "${map("Name", format("%s-RDS_BOOSTRAP_EPHEMERAL_INSTANCE", var.uuid))}"
  depends_on  = ["aws_db_instance.rds_mariadb"]
}

resource "aws_security_group" "allow_ssh_from_runner" {
  name_prefix = "allow_ssh_from_runner_${var.uuid}"
  description = "Allow SSH from the target VPC"
  vpc_id      = "${data.aws_vpc.selected.id}"

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["${data.aws_vpc.selected.cidr_block}"]
  }
}
