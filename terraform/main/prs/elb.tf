resource "aws_elb" "hof_prs_elb" {
  name            = "${local.aws_elb_hof_prs_elb_name}"
  subnets         = ["${data.aws_subnet_ids.trusted.ids}"]
  security_groups = ["${aws_security_group.prs_elb.id}"]

  access_logs {
    bucket        = "${local.aws_elb_hof_prs_elb_bucket}"
    bucket_prefix = "${local.aws_elb_hof_prs_elb_bucket_prefix}"
    interval      = 60
  }

  internal = true

  health_check {
    healthy_threshold   = 2
    unhealthy_threshold = 2
    timeout             = 3
    target              = "HTTPS:8443/prs_nn/index.jsp"
    interval            = 30
  }

  # backing instance port, so outgoing
  # Encryption - Data transmission - Internal NN - Closed Network (SRL_R-3, SRL_1.13)
  listener {
    instance_port      = 8443
    instance_protocol  = "https"
    lb_port            = 443
    lb_protocol        = "https"
    ssl_certificate_id = "${data.aws_acm_certificate.certificate.arn}"
  }

  cross_zone_load_balancing   = true
  idle_timeout                = 400
  connection_draining         = true
  connection_draining_timeout = 400

  tags {
    Name  = "HOF-PRS-elb"
    Owner = "keshavkummari@gmail.com"
  }
}
