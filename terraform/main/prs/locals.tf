locals {
  aws_route53_prs_dns_name = "${var.uuid == "master" ? "prs" : "prs-${var.uuid}"}"

  aws_launch_configuration_hof_prs_asg_conf_name_prefix = "hof-prs-app-${var.env}"
  aws_autoscaling_group_hof_prs_asg_name                = "${local.aws_launch_configuration_hof_prs_asg_conf_name_prefix}-${var.env}-${var.uuid}"
  aws_elb_hof_prs_elb_name                              = "hof-prs-elb-${var.uuid}-${var.env}"
  aws_elb_hof_prs_elb_bucket                            = "hof-dev-bucket"
  aws_elb_hof_prs_elb_bucket_prefix                     = "elb-logs"
  aws_security_group_prs_elb_name                       = "prs-sg-elb-${var.uuid}-${var.env}"
  aws_security_group_prs_app_name                       = "prs-sg-app-${var.uuid}-${var.env}"
  aws_security_group_prs_db_name                        = "prs-sg-db-${var.uuid}-${var.env}"
  aws_kms_key_rds_kms_key_description                   = "hof-prs-rds-kms-${var.uuid}-${var.env}"
  aws_kms_key_ssm_kms_key_description                   = "hof-prs-ssm-kms-${var.uuid}-${var.env}"

  #Remove the hardcoded values below
  aws_kms_key_rds_kms_key_allowed_roles     = ["arn:aws:iam::${data.aws_caller_identity.current.account_id}:role/tf-glrunner-b6e0a732b-master", "arn:aws:iam::${data.aws_caller_identity.current.account_id}:role/Admin", "arn:aws:iam::${data.aws_caller_identity.current.account_id}:role/Engineer"]
  aws_kms_key_ssm_kms_key_allowed_roles     = ["arn:aws:iam::${data.aws_caller_identity.current.account_id}:role/tf-glrunner-b6e0a732b-master", "arn:aws:iam::${data.aws_caller_identity.current.account_id}:role/Admin", "arn:aws:iam::${data.aws_caller_identity.current.account_id}:role/Engineer"]
  aws_db_instance_rds_mariadb_instance_name = "hof-prs-rds-${var.uuid}-${var.env}"
  aws_db_instance_rds_mariadb_database_name = "prs${replace(var.uuid, "/[^0-9a-zA-Z]*/", "")}${var.env}"
}
