######################
# INSTANCE VARIABLES #
######################

# Specify the size of EC2 instances for this environment
instance_type = "t2.micro"

# Specify the default EC2 instance role  for this environment
instance_iam_profile = "arn:aws:iam::875428837402:instance-profile/secaec2instanceprofile"

instance_wait_elb = "1"

instance_min = "1"

instance_max = "1"

####################
# ACCESS FILTERING #
####################

# Specify IP ranges in CIDR format to define who can access the application
elb_src_cidrs = ["0.0.0.0/0"]
