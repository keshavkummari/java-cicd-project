resource "aws_kms_key" "prs-tf-state" {
  description             = "prs-tf-state-${var.env}"
  enable_key_rotation     = true
  deletion_window_in_days = 7

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Id": "key-state-policy",
  "Statement": [{
    "Sid": "Allow access for Key Administrators",
    "Effect": "Allow",
    "Principal": {"AWS":
        ${jsonencode(var.role-arns)}
    },
    "Action": [
      "kms:Create*",
      "kms:Describe*",
      "kms:Enable*",
      "kms:List*",
      "kms:Put*",
      "kms:Update*",
      "kms:Revoke*",
      "kms:Disable*",
      "kms:Get*",
      "kms:Delete*",
      "kms:ScheduleKeyDeletion",
      "kms:CancelKeyDeletion"
    ],
    "Resource": "*"
  },
  {
    "Sid": "Allow use of the key",
    "Effect": "Allow",
    "Principal": {"AWS":
        ${jsonencode(var.role-arns)}
    },
    "Action": [
      "kms:Encrypt",
      "kms:Decrypt",
      "kms:ReEncrypt*",
      "kms:GenerateDataKey*",
      "kms:DescribeKey"
    ],
    "Resource": "*"
  }]
}
EOF
}

resource "aws_kms_alias" "prs-tf-state" {
  name          = "alias/prs-tf-state-${var.env}"
  target_key_id = "${aws_kms_key.prs-tf-state.key_id}"
}
